<?php
$page = 'About Us';
include_once ('header.php');
include ('includes/allfunctions.php');

?>

    <style>
        .container{
            width:95%;
        }
    </style>
    <!-- basic-slider start -->
    <div class="slider-section">
        <div class="slider-active owl-carousel">
            <div class="single-slider slider-screen nrbop bg-black-alfa-40" style="background-image: url(assets/img/slides/sd1.jpg);">
                <div class="container">
                    <div class="slider-content text-white">
                        <h2 class="b_faddown1 cd-headline clip is-full-width" >About Us</h2>
                        <p class="b_faddown2">Myaidfund.com is the number 1 crowdfunding platform in Ghana and beyond.
<!--                            <br />Online Fundraising Has Never Been Easier. Get started and accept online donations instantly </p>-->
                        <div class="slider_button b_faddown3"><a href="#our_mission">Our Mission</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- basic-slider end -->

    <!-- Features -->


    <!-- Special Cuase Paralax -->



    <!-- Causes -->



    <!-- team -->
    <div class="team-wrapper" id="our_mission">
        <div class="container">
            <div class="section-name one">
                <h2>Our Mission</h2>

                <div class="tags">
                    <p style="font-size: 18px">Empowering individual, communities and organizations to support and nature the causes they care about
                        Bring smiles to people, communities and organizations through crowdfunding.</p>

                    <p style="font-size: 18px">Our platform provides unlimited possibilities achievable by anyone by making global giving instantaneous and impactful.
                        Myaidfund gives you the best in crowd fundraising. Online and offline fundraising has never been easier.</p>


                </div>
            </div>


        </div>
    </div>




    <!-- Modal -->




    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>