<?php
$page = 'Safety';
include_once ('header.php');
include ('includes/allfunctions.php');

?>

    <style>
        .container{
            width:95%;
        }
    </style>

    <div class="team-wrapper" style="padding-top: 0px" id="our_mission">
        <div class="container">
            <div class="section-name one">
                <h2>How we protect the our community</h2>
                <br/>
                <div class="tags" style="text-align: left;">

                    <p>
                        MyaidFund is dedicated to empowering people to help people, and an
                        overwhelming majority of campaigns on our platform are safe and legitimate.
                        Fraudulent campaigns make up less than one percent of all campaigns.
                    </p>
                    <p>
                        In the rare instances where people create campaigns with the intention to
                        take advantage of others’ generosity, MyaidFund takes swift action to
                        resolve the issue.
                    </p>
                    <p>
                        Our policy is simple and strictly enforced:
                        <strong>
                            It is not permitted to lie or intentionally deceive donors to a
                            MyaidFund campaign for financial or personal gain.
                        </strong>
                    </p>
                    <p>
                        The following are    <strong>examples of fraudulent activity we expressly forbids:</strong>
                    </p>
                    <p>
                        · Breaking the law
                    </p>
                    <p>
                        · Lying or being misleading about your identity as a Campaign Organizer or
                        your relationship to the beneficiary of the funds
                    </p>
                    <p>
                        · Posting misleading statements in the campaign description
                    </p>
                    <p>
                        · Not delivering funds to the stated beneficiary
                    </p>
                    <p>
                        Sometimes,
                        <strong>
                            there are situations that may raise questions but are not considered
                            fraudulent
                        </strong>
                        on our platform. These include:
                    </p>
                    <p>
                        · Rumors or speculation of fraud
                    </p>
                    <p>
                        · Requesting that your name or image be removed from a campaign description
                    </p>
                    <p>
                        · Disagreeing with the nature of the campaign or the character of the
                        Campaign Organizer
                    </p>
                    <p>
                        · Custody or familial disputes or disagreements
                    </p>
                    <p>
                        To report a campaign for fraud, visit <a href="contact-us.php">https://myaidfund.com/contact-us.php</a>
                    </p>
                    <p>
                        MyaidFund cooperates with law enforcement and provides them with any
                        information needed in an investigation. If you are a law enforcement
                        officer looking to contact our team, you can reach us on
                        <a href="contact-us.php">https://myaidfund.com/contact-us.php</a>
                    </p>

                </div>
            </div>


        </div>
    </div>





    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>