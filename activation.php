<?php
$page = 'Activation';
include_once ('header.php');
include ('includes/allfunctions.php');
$code = $_REQUEST['ec'];
$email = $_REQUEST['em'];

$account_type = get_account_activation_details($email);
$rst=update_activation_code($code);

$business_account='<div class="">
                <form id="fdoc" enctype="multipart/form-data" action="process/upload_documents.php" class="formoid-metro-cyan" style="background-color:#f8f6f6;font-size:14px;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Arial,Verdana,sans-serif;color:#666666;max-width:480px;min-width:150px" method="post">
                    <input type="hidden" name="opera" value="org">
                     <input type="hidden" name="email" value="'.$email.'">

                    <div class="element-separator"><hr><h3 style="text-align: center" class="section-break-title">Please Provide The Following Details</h3></div><br/>
                    <div class="element-file" title="Business Registration Details"><label class="title">Business Registration Details<span style="color:red;" class="required">*</span></label><label class="large" ><div class="button">Choose File</div><input type="file" class="file_input" name="docs" required="required"/><div class="file_text">No file selected</div></label></div>
                    <div class="submit"><input type="submit" value="Submit"/></div></form>
                <!-- Stop Formoid form-->
            </div>';

$personal_account='<div class="">
                <form id="fdoc" enctype="multipart/form-data" action="process/upload_documents.php" class="formoid-metro-cyan" style="background-color:#f8f6f6;font-size:14px;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Arial,Verdana,sans-serif;color:#666666;max-width:480px;min-width:150px" method="post">
                    <input type="hidden" name="opera" value="pers">
                    <input type="hidden" name="email" value="'.$email.'">
                    <div class="element-separator"><hr><h3 style="text-align: center" class="section-break-title">Please Provide The Following Details</h3></div><br/>
                    
                    <div class="element-input"><label class="title">Date of Birth<span style="color:red;" class="required">*</span></label><input class="large" type="date" name="dob" required="required"/></div>
                    
                    <div class="element-input"><label class="title">Nationality<span style="color:red;" class="required">*</span></label><input class="large" type="text" name="nationality" required="required"/></div>
                    
                    <div class="element-input"><label class="title">Residential Address<span style="color:red;" class="required">*</span></label><input class="large" type="text" name="resident" required="required"/></div>
                    
                    <div class="element-input"><label class="title">Phone Number<span style="color:red;" class="required">*</span></label><input class="large" type="text" name="phone" required="required"/></div>
                    
                    <div class="element-select"><label class="title">ID Type<span style="color:red;" class="required">*</span></label><div class="large"><span><select name="card_type" required="required">

        <option value="Voters ID">Voters ID</option>
		<option value="Passport">Passport</option>
		<option value="National ID Card">National ID Card</option>
		<option value="Drivers Licence">Drivers Licence</option>
	
		</select><i></i></span></div></div>
		
		<div class="element-input"><label class="title">ID Number<span style="color:red;" class="required">*</span></label><input class="large" type="text" name="id_number" required="required"/></div>
		
                    <div class="element-file" title="Valid Id Card"><label class="title">Valid ID Card<span style="color:red;" class="required">*</span></label><label class="large" ><div class="button">Choose File</div><input type="file" class="file_input" name="docs" required="required"/><div class="file_text">No file selected</div></label></div>
                    <div class="submit"><input type="submit" value="Submit"/></div></form>
                <!-- Stop Formoid form-->
            </div>';

if(!$rst){
    $rst = check_account_status($email);
}
if ($rst){
    $head = '<h2 style="color: green">Email Validation Successful</h2>';
    $body = 'Your Email has been Verified successfully. Please Proceed to Login. <a href="#" data-toggle="modal" data-target="#login-modal" class="btn btn-ms btn-solid"><span>Login</span></a>';
}else{
    $head = '<h2 style="color: red">Email Validation Unsuccessful</h2>';
    $body = 'Your Email Verification Failed. Please try again';
}
?>

    <style>
        .container{
            width:95%;
        }
    </style>




    <!-- team -->
    <div class="team-wrapper" id="our_mission" style="padding-top: 5px">
        <div class="container">
            <div style="padding-bottom:0" class="section-name one">
                <? echo $head ?>

                <div class="" style="text-align: left; padding: 10px 30px 30px 30px">
                <p style="text-align: center"><?php echo $body; ?></p>
                </div>
            </div>

            <div class="doc_container">
                <?php

                if(!empty($account_type)){
                    if($account_type=='Personal'){
                        echo $personal_account;
                    }else{
                        //echo $business_account;
                    }
                }
                ?>
            </div>


        </div>
    </div>







    <!-- Foter -->
<?php include_once ('footer.php');?>

<script>
    // File input
    jQuery("input[type=file]").change(function(){
        $(this).next().html( $(this).val() );
    });



    $('#fdoc').ajaxForm({
        beforeSend: function() {

            $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } });

        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            console.log(percentVal);
            $('.blockUI h1').html(percentVal);
        },
        success: function() {

        },
        complete: function(xhr) {
            $.unblockUI();
            var rst = xhr['responseText'];

            console.log(rst);
            if (rst === '0') {
                swal("Upload Status", "Your Documents Have Been Uploaded Successfully", "success");
                $('.doc_container').addClass('hide');
                setTimeout(
                    function () {
                        window.location.href = 'index.php';
                    }, 1500);
                //alert("Your Documents Have Been Uploaded Successfully");
            } else {
                swal("Upload Status", "Sorry an error occurred, Try Again", "error");

            }

        }
    });

</script>
