<?php

$invoice = array(
	'invoice'        => array(
		'items'         => array(
			'item_0'       => array(
				'name'        => 'Donation',
				'quantity'    => 1,
				'unit_price'  => '35.0',
				'total_price' => '35.0',
				'description' => '{Customer Name} order from MyAidFund',
			)
		),

		'total_amount' => 2.00,
		'description'  => 'Total cost of 2 shirts'

	),
	'store'        => array(
		'name'        => 'MyAidFund',
		'tagline'     => 'donation',
		'phone'       => '+233246144043',
		'website_url' => 'https://myaidfund.com/',
	),

	'actions'     => array(
		'cancel_url' => 'https://myaidfund.com/',
		'return_url' => 'https://hubtel.com',
	),
);

$clientId       = 'ysoethtd';//
$clientSecret   = 'rcwtazre';
$basic_auth_key = 'Basic '.base64_encode($clientId.':'.$clientSecret);
$request_url    = 'https://api.hubtel.com/v1/merchantaccount/onlinecheckout/invoice/create';
$create_invoice = json_encode($invoice, JSON_UNESCAPED_SLASHES);

$ch = curl_init($request_url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $create_invoice);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization: '.$basic_auth_key,
		'Cache-Control: no-cache',
		'Content-Type: application/json',
	));

$result = curl_exec($ch);
$error  = curl_error($ch);
curl_close($ch);

if ($error) {
	echo $error;
} else {
	// redirect customer to checkout
	$response_param = json_decode($result);
	$redirect_url   = $response_param->response_text;
	header('Location: '.$redirect_url);

}