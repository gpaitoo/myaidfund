<?php
$page = 'home';
include_once ('header.php');
include ('includes/allfunctions.php');
$catg = $_REQUEST['catg'];
$of   = 0;
if (isset($_REQUEST['of'])) {
	$of = $_REQUEST['of'];
}

?>
<style>
        .container{
            width:95%;
        }
    </style>

<?php
$sp                 = get_special_cause();
$sp_amount_achieved = get_donation_sum($sp->id);
$crst               = determine_donation_currency($sp->id);
$fcurrency          = '$';
if ($crst == 'USD') {
	$fcurrency = '$';
} else {
	$fcurrency = 'GHS';
}
?>
    <div class="special-cause">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 donet__area_img">
                    <a href="campaign_details.php?donate_id=<?php echo $sp->id?>"><img style="width:100%" src="<?php echo $sp->top_img;?>" alt="" /></a>
                </div>
                <div class="col-md-6 col-xs-12 donet__area">
                    <div class="section-name parallax one">
                        <h1>special cause Right Now</h1>
                        <h2><a href="campaign_details.php?donate_id=<?php echo $sp->id?>"><?php echo $sp->title;?></a></h2>
                        <h4><?php echo strlen($sp->story) > 200?substr($sp->story, 0, 200)."...":$sp->story;?>

                        </h4>
                    </div>
                    <div class="foundings">
                        <div class="progress-bar-wrapper big">
                            <div class="progress-bar-outer">
                                <div class="clearfix">
                                    <span class="value one">Rised: <?php echo $fcurrency.$sp_amount_achieved?></span>
                                    <span class="value two">- To go: <?php echo $fcurrency;echo $sp->amount_goal-$sp_amount_achieved?></span>
                                </div>
                                <div class="progress-bar-inner">
                                    <div class="progress-bar">
                                        <span data-percent="<?php echo (($sp_amount_achieved/$sp->amount_goal)*100)?>"> <span class="pretng"><?php echo (($sp_amount_achieved/$sp->amount_goal)*100)?>%</span> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btns-wrapper">
                        <a href="#" data-toggle="modal" data-target="#payoptions" data-donation-id="<?php echo $sp->id?>" class="btn btn-big btn-solid "><i class="fa fa-archive"></i><span>Make Donation</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Causes -->

    <div class="causes-wrapper">
        <div class="container">
            <div class="section-name one">
                <h2>Recent Cause</h2>
                <div class="short-text">
                    <h5>Your little support can bring a simile to someone</h5>
                </div>
            </div>
            <div class="causes">
                <div class="causes-list row">
<?php
$cs = get_cause_by_catg($catg, $of);

while ($row = $cs->fetch_object()) {
	$r_amount_achieved = get_donation_sum($row->id);
	$crst              = determine_donation_currency($row->id);
	$currency          = '$';
	if ($crst == 'USD') {
		$currency = '$';
	} else {
		$currency = 'GHS';
	}

	?>
	                        <div class="cause-wrapper col-md-4 col-xs-12 col-sm-6 legal health">
	                            <div class="cause content-box">
	                                <div class="img-wrapper">
	                                    <a href="campaign_details.php?donate_id=<?php echo $row->id?>">
	                                        <div class="overlay">
	                                        </div>
	                                        <img class="img-responsive" src="<?php echo $row->top_img;?>" alt="">
	                                    </a>
	                                </div>
	                                <div class="info-block">
	                                    <a href="campaign_details.php?donate_id=<?php echo $row->id?>"><h4><?php echo $row->title?></h4></a>
	                                    <p><?php echo strlen($row->brief) > 200?substr($row->brief, 0, 200)."...":$row->brief;?></p>
	                                    <div class="foundings">
	                                        <div class="progress-bar-wrapper min">
	                                            <div class="progress-bar-outer">
	                                                <p class="values"><span class="value one">Raised: <?php echo $currency.$r_amount_achieved;
	?></span>-<span class="value two">To go: <?php echo $currency;
	echo $row->amount_goal-$r_amount_achieved?></span></p>
	                                                <div class="progress-bar-inner">
	                                                    <div class="progress-bar">
	                                                        <span data-percent="<?php echo (($r_amount_achieved/$row->amount_goal)*100)?>"><span class="pretng"><?php echo (($r_amount_achieved/$row->amount_goal)*100)?>%</span> </span>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="donet_btn">
	                                        <a data-toggle="modal" data-target="#payoptions" data-donation-id="<?php echo $row->id?>" href="#" class="btn btn-min btn-solid"><i class="fa fa-archive"></i><span>Donate Now</span></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	<?php }?>
</div>
                <div>
                    <ul class="pagination pagination-lg">
<?php
$it = get_total_cause_by_catg($catg);

for ($i = 0; $i < $it; $i++) {
	if ($i == $of) {
		echo '<li class="active" ><a href="campaigns.php?catg='.$catg.'&of='.$i.'">'.($i+1).'</a></li>';
	} else {
		echo '<li><a href="campaigns.php?catg='.$catg.'&of='.$i.'">'.($i+1).'</a></li>';
	}

}
?>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <!-- team -->
    <div class="team-wrapper">
        <div class="container">
            <div class="section-name one">
                <h2>Categories</h2>

                <div class="tags">
                    <a href="campaigns.php?catg=community"><div class="btn btn-solid">Clubs &amp; Community</div></a>
                    <a href="campaigns.php?catg=arts"><div class="btn btn-solid">Creative Projects</div></a>
                    <a href="campaigns.php?catg=greek"><div class="btn btn-solid">Fraternities &amp; Sororities</div></a>
                    <a href="campaigns.php?catg=other"><div class="btn btn-solid">Fun &amp; Special Events</div></a>
                    <a href="campaigns.php?catg=children"><div class="btn btn-solid">Kids &amp; Family</div></a>
<!--                    <a href="campaigns.php?catg=lgbt"><div class="btn btn-solid">LGBT</div></a>-->
                    <a href="campaigns.php?catg=health"><div class="btn btn-solid">Medical &amp; Health</div></a>
                    <a href="campaigns.php?catg=memorial"><div class="btn btn-solid">Memorials &amp; Funerals</div></a>

                    <a href="campaigns.php?catg=military"><div class="btn btn-solid">Military</div></a>
                    <a href="campaigns.php?catg=non-profit"><div class="btn btn-solid">Non-Profit and Charity</div></a>
                    <a href="campaigns.php?catg=animal"><div class="btn btn-solid">Pets &amp; Animals </div></a>
                    <a href="campaigns.php?catg=political"><div class="btn btn-solid">Politics &amp; Public Office</div></a>
                    <a href="campaigns.php?catg=church"><div class="btn btn-solid">Religious Organizations</div></a>
                    <a href="campaigns.php?catg=run-walk-ride"><div class="btn btn-solid">Run/Walk/Rides </div></a>
                    <a href="campaigns.php?catg=education"><div class="btn btn-solid">Schools &amp; Education</div></a>
                    <a href="campaigns.php?catg=sports"><div class="btn btn-solid">Sports &amp; Teams</div></a>
                    <a href="campaigns.php?catg=trips"><div class="btn btn-solid">Trips &amp; Adventures</div></a>

                </div>
            </div>


        </div>
    </div>


    <div class="modal fade" id="payoptions" tabindex="-1" role="dialog" aria-labelledby="payoptions">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content">
                    <div class="row" style="margin-left:0; margin-right:0; padding-bottom:10px">
                        <form>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <h3 style="margin:10px 0 10px 0;" class="text-center">Payment Option</h3>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Amount</label>
                                    <input required="required" class="form-control cam" style="width:100%;" type="text" id="iamount" value="10" placeholder="Amount"/>
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Full Name</label>
                                    <input required="" class="form-control" style="width:100%;" type="text" id="fname" value="" placeholder="Full Name"/>
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Email Address</label>
                                    <input required="" class="form-control" style="width:100%;" type="email" id="iemail" value="" placeholder="Email Address"/>
                                </div>

                            </div>

                            <br/>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <button style="width:100%;" type="submit" class="btn btn-primary btn-lg paystripe" data-dismiss="modal"><i class="fa fa-credit-card fa-2"></i> Visa/Mastercard</button>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <button style="width:100%;" type="submit" class="btn btn-danger btn-lg paymm" data-toggle="modal" data-target="#myModal" data-dismiss="modal" aria-hidden="true"><i class="fa fa-money fa-2"></i> Mobile Money</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content">

                    <h2 class="text-center">Payment Options</h2>
                    <div class="modal-body">
                        <div class="">
                            <form class="form-goup" style="padding: 8px;" id="donate_form">
                                <input type="hidden" name="email" id="email"/>
                                <input type="hidden" name="campid" value="" class="campid"/>
                                <input type="hidden" name="isregistered" value="0"/>
                                <input type="hidden" name="donatorid" value="-1"/>

                                <div class="form-group">
                                    <label class="sr-only" for="donate_amount">Amount (in Ghana Cedis)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">GH₵</div>
                                        <input type="number" name="amount" class="form-control" id="donate_amount" placeholder="Amount">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>


                                <div class="form-group">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input checked style="margin-top: 18px;" type="radio" id="card_mtn" value="mtn" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/mtn-mobile-money.png"></label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_airtel" value="airtel" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/airtel-money.png"/></label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_tigo" value="tigo" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/tigo-cash.png"/></label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_voda" value="voda" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/vodafone-cash-ghana.png"/></label>
                                        </div>
                                    </div>
                                    <!--                    <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_visa" value="visa" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="http://www.catnets.com.au/images/visa_mastercard_logo.png"/></label>-->




                                </div>

                                <!--OTHER MOBILE MONEY-->

                                <div id="mm_form" class="sw_form">
                                    <div class="form-group">
                                        <label class="sr-only" for="fullname">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input name="fullname" type="text" class="form-control" id="fullname" placeholder="Name"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="sr-only" for="mphone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone</div>
                                            <input name="phone" type="tel" class="form-control" id="mphone" placeholder="Phone Number"/>
                                        </div>
                                    </div>
                                </div>
                                <!--VODAFONE CASH-->

                                <div id="voda_form" class="sw_form hide">
                                    <div class="form-group">
                                        <label class="sr-only" for="fullname">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input name="vfullname" type="text" class="form-control" id="vfullname" placeholder="Name"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="sr-only" for="mphone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone</div>
                                            <input name="vphone" type="text" class="form-control" id="vphone" placeholder="Phone Number"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only" for="token">Token</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Token</div>
                                            <input name="token" type="text" class="form-control" id="token" placeholder="Token"/>
                                        </div>
                                    </div>

                                </div>
                                <div style="display: block;text-align: right;">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Donate</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>



    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>