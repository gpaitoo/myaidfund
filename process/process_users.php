<?php
/**
 * Created by PhpStorm.
 * User: Mtech
 * Date: 01/10/2017
 * Time: 8:31 PM
 */
include('../includes/allfunctions.php');

if($is_production){
    if(isset($_REQUEST['g-recaptcha-response'])){
        $ccode = $_REQUEST['g-recaptcha-response'];
        $d = check_captcha($ccode);
        $responseData = json_decode($d, true);
        $cresp = $responseData['success'];
        if($cresp==true){

        }else{
            echo 'captcha';
            die();
        }
    }
}


if(isset($_REQUEST['opera'])){
    $opera = $_REQUEST['opera'];
    if($opera=='login'){
        $username = $mysqli->real_escape_string($_REQUEST['xxxname']);
        $password = md5($mysqli->real_escape_string($_REQUEST['xxxpass']));
        echo letin($username,$password);
    }else if($opera=='update'){
        $pass1 = $_REQUEST['xxpass1'];
        $pass2 = $_REQUEST['xxpass2'];
        if($pass1 != $pass2){
            echo 'Passwords does not match';
            die();
        }
        if(strlen($pass1)<6){
            echo 'Password must be more than six (6) characters';
            die();
        }
        $password = strtoupper(md5($mysqli->real_escape_string($pass1)));
        $code = $mysqli->real_escape_string($_REQUEST['code']);
        echo update_letin($code,$password);
    }else if($opera=='d_register'){
        insert_donor_user();
    }else if($opera == 'd_login'){
        $username = $_REQUEST['xxname'];
        $password = md5(trim($_REQUEST['xxpass']));
        d_letin($username,$password);
    }else if($opera == 'fb_login'){
        fb_login();
    }else if($opera == 'gl_login'){
        gl_login();
    }
}





function insert_donor_user(){
    global $mysqli;
    $name = $_REQUEST['fname'];
    $email = $_REQUEST['xxname'];
    $password = md5(trim($_REQUEST['xxpass']));

    if(check_donor_exist($email)){
        echo "Account with the same email exist!";
        die();
    }
    $sql = "INSERT INTO `registered_donors`(`name`, `xxname`, `xxpass`, `email`, `source`, `isactive`) 
              VALUES('$name','$email','$password','$email','web',1);
              ";
    $mysqli->query($sql);
    d_letin($email,$password);
}


function fb_login(){
    global $mysqli;
    $name = $_REQUEST['name'];
    $userid = $_REQUEST['fb_Id'];
    $email = $_REQUEST['email'];
    $profilePictureUrl = $_REQUEST['profilePictureUrl'];
    if(!check_donor_exist($userid)){
        $sql = "INSERT INTO `registered_donors`(`name`, `xxname`, `xxpass`, `email`, `source`, `isactive`) 
              VALUES('$name','$userid','','$email','fb',1);
              ";
        $mysqli->query($sql);
        social_letin($userid);
    }else{
        social_letin($userid);
    }
}


function gl_login(){
    global $mysqli;
    $name = $_REQUEST['name'];
    $userid = $_REQUEST['gl_Id'];
    $email = $_REQUEST['email'];
    $profilePictureUrl = $_REQUEST['profilePictureUrl'];
    if(!check_donor_exist($userid)){
        $sql = "INSERT INTO `registered_donors`(`name`, `xxname`, `xxpass`, `email`, `source`, `isactive`) 
              VALUES('$name','$userid','','$email','gl',1);
              ";
        $mysqli->query($sql);
        social_letin($userid);
    }else{
        social_letin($userid);
    }
}


function d_letin($username,$password) {
    global $mysqli;

    $sql     = "SELECT id,email,name FROM registered_donors WHERE xxname = '$username' AND xxpass = '$password' AND isactive = 1 LIMIT 1";
    $results = $mysqli->query($sql);
    $r       = $results->fetch_object();
    if ($results->num_rows > 0) {
        begin_donor_session($r->id, $r->email, $r->name);
        echo 'true';
    } else {
        echo 'false';
    }
}

function social_letin($userid) {
    global $mysqli;
    $sql     = "SELECT id,email,name FROM registered_donors WHERE xxname = '$userid' AND isactive = 1 LIMIT 1";
    $results = $mysqli->query($sql);
    $r       = $results->fetch_object();
    if ($results->num_rows > 0) {
        begin_donor_session($r->id, $r->email, $r->name);
        echo 'true';
    } else {
        echo 'false';
    }
}

function check_donor_exist($value){
    global $mysqli;
    $sql = "SELECT id FROM registered_donors WHERE xxname = '$value'
              ";
    $results = $mysqli->query($sql);
    if ($results->num_rows > 0) {
        return true;
    }else{
        return false;
    }
}

function begin_donor_session($id, $email, $name) {
    session_start();
    $_SESSION['timeout'] = time();
    $_SESSION['d_id']      = $id;
    $_SESSION['d_email']   = $email;
    $_SESSION['d_name']   = $name;
    $_SESSION['login_type'] = 'donor';
}