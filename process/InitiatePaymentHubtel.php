<?php
include ('../includes/allfunctions.php');
/*header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');*/

$sw           = $_REQUEST['optradio'];
$trans_source = $_REQUEST['trans_source'];
//print_r(json_encode($_REQUEST));

debug("Initiate_payment",json_encode($_REQUEST));
switch ($sw) {
	case 'visa':
		# code...
		$fname  = $_REQUEST['fname'];
		$lname  = $_REQUEST['lname'];
		$name   = "$fname $lname";
		$email  = $_REQUEST['email'];
		$phone  = $_REQUEST['vmphone'];
		$amount = $_REQUEST['amount'];

		$transid = date('YmdHis').rand(12345, 9999999);
		//insert_donation($name, $phone, $amount, 'visa', $transid);
		$pay = initiate_call_visa($fname, $lname, $email, $phone, $amount, $transid);
		$val = $pay->ResponseCode;
		if ($val == '0') {
			$url = $pay->PaymentUrl;
			echo $url;
		} else {
			echo $val;
		}

		break;

	case 'mtn':
		# code...
		$name         = $_REQUEST['fullname'];
		$phone        = $_REQUEST['phone'];
		$amount       = $_REQUEST['amount'];
		$campid       = $_REQUEST['campid'];
		$email        = $_REQUEST['email'];
		$isregistered = $_REQUEST['isregistered'];
		$donatorid    = $_REQUEST['donatorid'];

		$transmode = 'mtn';

		$transid = 'Maid'.date('YmdHis').rand(12345, 9999999);

		if (insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, $transmode, $transid, $trans_source)) {
		    $pay = json_decode(initiate_call($phone, $amount, 'mtn-gh', $transid,$email,$name));
			$val = $pay->ResponseCode;
			echo $val;
		}

		break;

	case 'tigo':
		# code...
		$name         = $_REQUEST['fullname'];
		$phone        = $_REQUEST['phone'];
		$amount       = $_REQUEST['amount'];
		$campid       = $_REQUEST['campid'];
		$email        = $_REQUEST['email'];
		$isregistered = $_REQUEST['isregistered'];
		$donatorid    = $_REQUEST['donatorid'];

		$transmode = 'tigo';

		$transid = 'Maid'.date('YmdHis').rand(12345, 9999999);
		if (insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, $transmode, $transid, $trans_source)) {
			$pay = json_decode(initiate_call($phone, $amount, 'tigo-gh', $transid,$email,$name));
			$val = $pay->ResponseCode;
			echo $val;
		}
		break;

	case 'voda':
		# code...
        $name         = $_REQUEST['fullname'];
        $phone        = $_REQUEST['vphone'];
        $amount       = $_REQUEST['amount'];
        $campid       = $_REQUEST['campid'];
        $email        = $_REQUEST['email'];
        $isregistered = $_REQUEST['isregistered'];
        $donatorid    = $_REQUEST['donatorid'];
        $token = $_REQUEST['token'];

        $transmode = 'vodafone';

        $transid = 'Maid'.date('YmdHis').rand(12345, 9999999);
        if (insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, $transmode, $transid, $trans_source)) {
            $pay = json_decode(initiate_call($phone, $amount, 'vodafone-gh', $transid,$email,$name,$token));
            $val = $pay->ResponseCode;
            echo $val;
        }
		break;

	case 'airtel':
		# code...
		$name         = $_REQUEST['fullname'];
		$phone        = $_REQUEST['phone'];
		$amount       = $_REQUEST['amount'];
		$campid       = $_REQUEST['campid'];
		$email        = $_REQUEST['email'];
		$isregistered = $_REQUEST['isregistered'];
		$donatorid    = $_REQUEST['donatorid'];

		$transmode = 'airtel';

		$transid = 'Maid'.date('YmdHis').rand(12345, 9999999);
		if (insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, $transmode, $transid, $trans_source)) {
			$pay = json_decode(initiate_call($phone, $amount, 'airtel-gh', $transid,$email,$name));
			$val = $pay->ResponseCode;
			echo $val;
		}
		break;

	default:
		# code...
		break;
}

function initiate_call($phone, $amount, $transmode, $transid,$email,$fname,$token='') {

    if(!is_email_valid($email)){
        $email = "$phone@myaidfund.com";
    }
    debug("Email of Donator",$email);
	//$token    = ($transmode == 'VODAFONE')?Serial::random(5):'';
    $clientId       = 'ysoethtd';//
    $clientSecret   = 'rcwtazre';
    $basic_auth_key = 'Basic '.base64_encode($clientId.':'.$clientSecret);

	$post_arr = array(
	    'Channel' => $transmode,
		'CustomerMsisdn'              => $phone,
		'CustomerName'                 => $fname,
		'Amount'                   => $amount,
		'ClientReference'            => $transid,
		'Description'                     => 'MyAidFund Donation',
		'PrimaryCallbackUrl'              => 'https://myaidfund.com/process/hubtel_callback.php',
		//'SecondaryCallbackUrl'      => '',
        'CustomerEmail'             => $email,
        'Token' => $token
	);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.hubtel.com/v1/merchantaccount/merchants/HM2301180022/receive/mobilemoney",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($post_arr),
        CURLOPT_HTTPHEADER => array(
            'Authorization: '.$basic_auth_key,
            "cache-control: no-cache",
            "content-type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    debug('Hubtel Payment Call Results',$response);
	return $response;
}

/*function initiate_call_visa($fname, $lname, $email, $phone, $amount, $transid) {

	//$token    = ($transmode == 'VODAFONE')?Serial::random(5):'';
	$post_arr = array('PayType' => 'VISA/MASTERCARD',
		'PhoneNumber'              => $phone,
		'FirstName'                => $fname,
		'LastName'                 => $lname,
		'Email'                    => $email,
		'Amount'                   => $amount,
		'TransactionId'            => $transid,
		'Item'                     => 'ODG Donation',
		'CallbackUrl'              => 'http://139.162.221.182/fuse/callback.php',

	);

	$post_data = json_encode(array($post_arr));
	$curl      = curl_init();
	curl_setopt_array($curl, array(
			CURLOPT_URL            => "http://139.162.221.182/initiate.php",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			CURLOPT_POSTFIELDS     => $post_data,
			CURLOPT_HTTPHEADER     => array(
				"api: AE2B1FCA515949E5D54FB22B8ED95575",
				"authorization: Basic: 8fa8d201e89d1d5fe6002a4af8db882a",
				"cache-control: no-cache",
				"content-type: application/json",
			),
		));

	return json_decode(curl_exec($curl));
}*/
