<?php
include_once('../includes/allfunctions.php');
session_start();
if(isset($_REQUEST['opera'])){
    $email = $_REQUEST['email'];
    $id = get_account_id_by_email($email);
    $id.=rand(100,9999999);
    $opera =$_REQUEST['opera'];
    if($opera=="pers"){

        $id_type = $_REQUEST['card_type'];
        $id_number = $_REQUEST['id_number'];

        upload_personal_file($id,$email,$id_type,$id_number);
    }else{
        upload_business_file($id,$email);
    }
}

function upload_business_file($id,$email){
    global $mysqli;
    //get posted data
    $business_name = $mysqli->real_escape_string($_REQUEST['business_name']);
    $business_type = $mysqli->real_escape_string($_REQUEST['business_type']);
    $business_location = $mysqli->real_escape_string($_REQUEST['business_location']);
    $d_full_name = $mysqli->real_escape_string($_REQUEST['d_full_name']);
    $d_dob = $_REQUEST['d_dob'];
    $d_nationality = $mysqli->real_escape_string($_REQUEST['d_nationality']);
    $d_address = $mysqli->real_escape_string($_REQUEST['d_address']);
    $d_card_type = $mysqli->real_escape_string($_REQUEST['d_card_type']);
    $d_id_number = $_REQUEST['d_id_number'];
    $ownerid = $_SESSION['owner'];


    $target_dir = "../uploaded_documents/";;
    $nm = date("YmdHis").rand(999,999999);
    $nm2 = date("YmdHis").rand(1000,999999);
    $fname=$_FILES['docs']['name'];
    $d_name = $_FILES['d_id']['name'];
    $target_file = $target_dir .$id.'_'.$nm .'.'. pathinfo($fname, PATHINFO_EXTENSION);
    $d_target_file = $target_dir .$id.'_'.$nm2 .'.'. pathinfo($d_name, PATHINFO_EXTENSION);


    $bus_path = $mysqli->real_escape_string(str_replace('../','',$target_file));
    $d_path= $mysqli->real_escape_string(str_replace('../','',$d_target_file));
    if (move_uploaded_file($_FILES["docs"]["tmp_name"], $target_file) && move_uploaded_file($_FILES["d_id"]["tmp_name"], $d_target_file)){

        if(update_account_document_upload($email) && update_account_document_path($email,$bus_path,'Business Document','nothing')
            && insert_into_business_doc($ownerid,$business_name,$business_type,$business_location,$bus_path,$d_full_name,$d_dob,$d_nationality,$d_address,$d_card_type,$d_id_number,$d_path)) {
            echo 0;
        }else{
            debug("upload_documents","Database insertion failed");
        }
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

function upload_personal_file($id,$email,$id_type,$id_number){
    global $mysqli;
    $target_dir = "../uploaded_documents/";
    $nm = date("YmdHis").rand(999,999999);
    $fname=$_FILES['docs']['name'];

    $dob = $_REQUEST['dob'];
    $nationality = $_REQUEST['nationality'];
    $residential = $_REQUEST['resident'];
    $phone = $_REQUEST['phone'];

    $target_file = $target_dir .$id.'_'.$nm .'.'. pathinfo($fname, PATHINFO_EXTENSION);

    $path = $mysqli->real_escape_string(str_replace('../','',$target_file));
    debug("Uload_document",$target_file);
    if (move_uploaded_file($_FILES["docs"]["tmp_name"], $target_file) && update_account_document_upload($email) && update_account_document_path($email,$path,$id_type,$id_number)
        && insert_into_personal_doc($id,$dob,$nationality,$residential,$id_type,$id_number,$path,$phone)) {
        echo 0;
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
