<?php
error_reporting(0);
require '../../vendor/autoload.php';
include_once '../../includes/allfunctions.php';

// Create the JudoPay client, populate the api token and api secret with the
// details from the application you created on our

$amount= $_REQUEST['amount'];
$title = $_REQUEST['title'];
$campid = $_REQUEST['campid'];
$email = $_REQUEST['email'];
$isregistered = '0';
$donatorid = '-1';
$name = $_REQUEST['name'];
$phone = '';
$transid      = 'Maid'.date('YmdHis').rand(12345, 9999999);
$pay_ref      = 'Maid'.date('YmdHis').rand(123456789, 9999999999);
$rh = get_client_ip();
$useragent = $_SERVER['HTTP_USER_AGENT'];

$judopay = new \Judopay(
    array(
        'apiToken' => '479eojeYrBvuau4Q',
        'apiSecret' => 'b36c8c8faf5f9a01f4c419b6e29b4d2a8155c3a623bca3a9b143f1cdbcd423b3',
        'judoId' => '100896-844',
        'useProduction' => false
    )
);

// create an instance of the WebPayment Payment model (or you can use the Preauth model)
// if you only want to process a pre-authorisation which you can collect later.
$payment = $judopay->getModel('WebPayments\Payment');

// populate the required data fields.
$payment->setAttributeValues(
    array(
        'judoId' => '100896-844',
        'yourConsumerReference' => $transid,
        'yourPaymentReference' => $pay_ref,
        'amount' => $amount,
        'currency' => 'USD',
        'clientIpAddress' => $rh,
        'clientUserAgent' => $useragent
    )
);

// Send the model to the JudoPay API, this provisions your webpayment and returns a unique reference along with the
// URL of the page you'll need to dispatch your users to.

$webpaymentDetails = $payment->create();


$theWebPaymentReference = $webpaymentDetails["reference"];
$formPostUrl = $webpaymentDetails["postUrl"];

debug("JUDOPAY REQUEST RESPONSE",$webpaymentDetails);
insert_judopay_logs($theWebPaymentReference,$transid,$pay_ref);
insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, 'judopay', $transid, "WEB");

echo $formPostUrl.'?Reference='.$theWebPaymentReference;




function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}