<?php
/**
 * Created by PhpStorm.
 * User: Gideon Paitoo
 * Date: 29/03/2018
 * Time: 2:13 PM
 */
include ('../includes/allfunctions.php');
include ('../includes/reset_password_mail.php');

if(isset($_REQUEST['g-recaptcha-response'])) {
    $ccode = $_REQUEST['g-recaptcha-response'];
    $d = check_captcha($ccode);
    $responseData = json_decode($d, true);
    $cresp = $responseData['success'];
    if ($cresp == true) {

    } else {
        echo 'Captcha Failed';
        die();
    }
}

if(isset($_REQUEST['xxxemail'])){
    $email = $_REQUEST['xxxemail'];
    if(check_email($email)){
        $code = rand(99999,9999999999);
        insert_reset_code($code,$email);
        $msg = get_reset_password_message($code,$email);
        send_mail($email,"Myaidfund Password Reset",$msg);
        echo 'true';
    }else{
        echo 'Sorry Your Account was not identified on Myaidfund.com';
    }
}else{
    echo 'This is an invalid request';
}
