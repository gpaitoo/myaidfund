<?php
/**
 * Created by PhpStorm.
 * User: Mtech
 * Date: 05/07/2018
 * Time: 11:33 AM
 */

include ('../includes/allfunctions.php');
require '../vendor/autoload.php';

echo 'started'.PHP_EOL;
get_tokens();
echo 'ended'.PHP_EOL;

function get_tokens(){
    global $mysqli;
    $sql = "SELECT reference,transid FROM judopay_logs WHERE transstate=0 AND logwhen > timestampadd(day, -1, now())";
    $result = $mysqli->query($sql);

    while($row = $result->fetch_object()){
        $reference = $row->reference;
        $transid = $row->transid;
        //$campid = $row->campid;
        //echo "working on $reference and $transid".PHP_EOL;
        query_invoice_status($reference,$transid);
    }
}

function update_judopay_logs($reference,$status){
    global $mysqli;
    $sql = "UPDATE judopaylogs SET transstate=$status WHERE reference = '$reference'";
    $mysqli->query($sql);
}

function query_invoice_status($reference,$transid){


    $judopay = new \Judopay(
        array(
            'apiToken' => '479eojeYrBvuau4Q',
            'apiSecret' => 'b36c8c8faf5f9a01f4c419b6e29b4d2a8155c3a623bca3a9b143f1cdbcd423b3',
            'judoId' => '100896-844',
            'useProduction' => false
        )
    );

    // Create an instance of the WebPayment Transaction model (as web payments can either be payments or preauths we have a superclass called transaction).

    $existingTransactionRequest = $judopay->getModel('WebPayments\Transaction');


// invoke the find method passing in the reference you obtained above.
    $transactionDetails = $existingTransactionRequest->find($reference);

// check the value of the "status" array key to confirm the payment was successful
    $webpaymentStatus = $transactionDetails["status"];

// webpaymentStatus should be "Success"

// you can also access a copy of our receipt object using the "receipt" entry.

    //$receipt = $transactionDetails["receipt"];

    debug('Judopay Reference Check',$transactionDetails);

    print_r($webpaymentStatus);
    if($webpaymentStatus=='Success'){
        //echo '--its successfull---';
        update_donation(1, $transid);
        update_transaction_report(1,$transid);
        update_judopay_logs($reference,1);
        //echo '--after successfull---';
        $email = get_donator_email_by_transid($transid);
        $campid = get_donator_campid_by_transid($transid);
        if(contains('@',$email)){
            send_automatic_thank_you($campid, $email);
            debug("Email Sent For Successful Donation",$email);
        }else{
            debug("SMS Sent For Failed Donation",$email);
        }
    }elseif ($webpaymentStatus=='Expired'){
        update_donation(5, $transid);
        update_transaction_report(5,$transid);
        update_judopay_logs($reference,5);

        $email = get_donator_email_by_transid($transid);
        $campid = get_donator_campid_by_transid($transid);

        if(contains('@',$email)){
            send_donation_failed_thank_you($campid, $email);
            debug("Email Sent For Failed Donation",$email);
        }else{
            debug("SMS Sent For Failed Donation",$email);
        }
    }

}