<?php
/**
 * Created by PhpStorm.
 * User: Mtech
 * Date: 08/06/2018
 * Time: 9:23 PM
 */

include ('../includes/allfunctions.php');

echo 'started'.PHP_EOL;
get_tokens();
echo 'ended'.PHP_EOL;

function get_tokens(){
    global $mysqli;
    $sql = "SELECT token,transactionid FROM hubtel_tokens WHERE trans_state=0 AND logwhen > timestampadd(day, -1, now())";
    $result = $mysqli->query($sql);

    while($row = $result->fetch_object()){
        $token = $row->token;
        $transid = $row->transactionid;
        //$campid = $row->campid;
        echo "working on $token and $transid".PHP_EOL;
        query_invoice_status($token,$transid);
    }
}

function update_token($token,$status){
    global $mysqli;
    $sql = "UPDATE hubtel_tokens SET trans_state=$status WHERE token = '$token'";
    $mysqli->query($sql);
}

function query_invoice_status($token,$transid){
    $clientId       = 'ysoethtd';//
    $clientSecret   = 'rcwtazre';
    $basic_auth_key = 'Basic '.base64_encode($clientId.':'.$clientSecret);
    $request_url    = 'https://api.hubtel.com/v1/merchantaccount/onlinecheckout/invoice/status/'.$token;

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $request_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            'Authorization: '.$basic_auth_key,
            'Cache-Control: no-cache',
            'Content-Type: application/json'
        ),
    ));

    $result = curl_exec($curl);
    $error  = curl_error($curl);
    curl_close($curl);

    $response_param = json_decode($result);
    $response_code   = $response_param->response_code;
    //$redirect_url   = $response_param->response_text;

    debug('Hubtel Visa Invoice Check',$result);
    if ($error) {
        return 'false';
    } else {
        if($response_code=='00'){
            $status = $response_param->status;
            if($status=='completed'){
                update_donation(1, $transid);
                update_transaction_report(1,$transid);
                update_token($token,1);

                $email = get_donator_email_by_transid($transid);
                $campid = get_donator_campid_by_transid($transid);
                if(contains('@',$email)){
                    send_automatic_thank_you($campid, $email);
                    debug("Email Sent For Successful Donation",$email);
                }else{
                    debug("SMS Sent For Failed Donation",$email);
                }
            }elseif($status == 'pending'){
                update_donation(2, $transid);
                update_transaction_report(2,$transid);

            }elseif ($status == 'cancelled'){
                update_donation(5, $transid);
                update_transaction_report(5,$transid);
                update_token($token,5);

                $email = get_donator_email_by_transid($transid);
                $campid = get_donator_campid_by_transid($transid);

                if(contains('@',$email)){
                    send_donation_failed_thank_you($campid, $email);
                    debug("Email Sent For Failed Donation",$email);
                }else{
                    debug("SMS Sent For Failed Donation",$email);
                }
            }
        }else{
            return 'false';
        }
    }
}