<?php
/**
 * Created by PhpStorm.
 * User: Mtech
 * Date: 21/09/2017
 * Time: 8:39 PM
 */

include ('../includes/allfunctions.php');

$transaction_id   = $_REQUEST['transactionid'];
$response_code    = $_REQUEST['responsecode'];
$response_message = $_REQUEST['responsemessage'];
debug("Callback",$_REQUEST);
if ($response_code == '200') {
    update_donation(1,$transaction_id);
    update_transaction_report(1,$transaction_id);
} else {
    update_donation(5,$transaction_id);
    update_transaction_report(5,$transaction_id);
}

echo json_encode("success");
