<?php
/**
 * Created by PhpStorm.
 * User: Mtech
 * Date: 04/09/2017
 * Time: 10:10 PM
 */

require_once ('stripe-php-5.2.0/init.php');
include ('../includes/allfunctions.php');

/*\Stripe\Stripe::setApiKey('sk_test_BQokikJOvBiI2HlWgH4olfQ2');
$charge = \Stripe\Charge::create(array('amount' => 2000, 'currency' => 'usd', 'source' => 'tok_189fqt2eZvKYlo2CTGBeg6Uq' ));
echo $charge;*/

/*Stripe::setApiKey('d8e8fca2dc0f896fd7cb4cb0031ba249');
$charge = Stripe_Charge::create(array('source' => 'tok_XXXXXXXX', 'amount' => 2000, 'currency' => 'usd'));
echo $charge;*/

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey("sk_live_8ZEDqMHYvWYAaDuGi8ghcglq");

// Token is created using Stripe.js or Checkout!
// Get the payment token ID submitted by the form:
$token        = $_POST['stripeToken'];
$amount       = $_POST['amount'];
$campid       = $_POST['campid'];
$description  = $_POST['description'];
$email        = $_POST['stripeEmail'];
$isregistered = $_POST['isregistered'];
$donatorid    = $_POST['donatorid'];
$name         = $_POST['name'];
$phone        = '';
$transid      = 'Maid'.date('YmdHis').rand(12345, 9999999);
//print_r($key);
//echo $token;

// Charge the user's card:
$charge = \Stripe\Charge::create(array(
		"amount"      => $amount,
		"currency"    => "usd",
		"description" => 'Service Payment',//$description,
		"source"      => $token,
	));

$vbr          = $charge->__toJSON();
$dj           = json_decode($vbr);
$json_results = json_encode($dj);
$ref          = $dj->id;
$status       = $dj->status;
$amount       = $amount/100;

insert_stripe_response($transid, $ref, $status, $json_results);
insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, 'stripe', $transid, "WEB");

if ($status == 'succeeded') {
	update_donation(1, $transid);
    update_transaction_report(1,$transid);
	send_automatic_thank_you($campid, $email);
} else {
	update_donation(5, $transid);
    update_transaction_report(5,$transid);
}