<?php
session_start();
include ('../includes/allfunctions.php');
if (isset($_REQUEST)) {
	$opera = $_REQUEST['opera'];
	switch ($opera) {
		case 'create':
			save_campaign();
			break;
		case 'update':
			update_campaign();
			break;
		case 'news_update':
			save_campaign_news_update();
			break;
		default:
			# code...
			break;
	}
}

function save_campaign() {
	global $mysqli;
	if (isset($_SESSION['owner'])) {
		$ownerid  = $_SESSION['owner'];
		$title    = $mysqli->real_escape_string($_REQUEST['title']);
		$country  = $mysqli->real_escape_string($_REQUEST['country']);
		$city     = $mysqli->real_escape_string($_REQUEST['city']);
		$target   = $mysqli->real_escape_string($_REQUEST['target']);
		$category = $mysqli->real_escape_string($_REQUEST['category']);
		$enddate  = $mysqli->real_escape_string($_REQUEST['camp_enddate']);

		$thumb   = json_decode($_REQUEST['thumb_values'])->data;
		$story   = $mysqli->real_escape_string($_REQUEST['story']);
		$brief   = $mysqli->real_escape_string($_REQUEST['brief']);
        $video_title   = $mysqli->real_escape_string($_REQUEST['video_title']);
        $video_url   = str_replace("watch?v=","embed/",$mysqli->real_escape_string($_REQUEST['youtube_link']));
		$top_img = save_base64_image($thumb);
		$sql     = "
			INSERT INTO campaign(ownerid,title,story,date_end,amount_goal,top_img,country,city,category,brief,publish_status,video_title,video_url) VALUES($ownerid,'$title','$story','$enddate',$target,'$top_img','$country','$city','$category','$brief','unapproved','$video_title','$video_url');
";

		$results = $mysqli->query($sql);
		if ($results) {
			echo 'true';
		}
	}

}

function update_campaign() {
	global $mysqli;
	if (isset($_SESSION['owner'])) {
		$ownerid  = $_SESSION['owner'];
		$campid   = $_REQUEST['campid'];
		$title    = $mysqli->real_escape_string($_REQUEST['title']);
		$country  = $mysqli->real_escape_string($_REQUEST['country']);
		$city     = $mysqli->real_escape_string($_REQUEST['city']);
		$target   = $mysqli->real_escape_string($_REQUEST['target']);
		$category = $mysqli->real_escape_string($_REQUEST['category']);
		$enddate  = $mysqli->real_escape_string($_REQUEST['camp_enddate']);

		$story = $mysqli->real_escape_string($_REQUEST['story']);
		$brief = $mysqli->real_escape_string($_REQUEST['brief']);
        $video_title   = $mysqli->real_escape_string($_REQUEST['video_title']);
        $video_url   = str_replace("watch?v=","embed/",$mysqli->real_escape_string($_REQUEST['youtube_link']));

		if (isset($_REQUEST['thumb_values'])) {
			$old_topimg = $_REQUEST['oldimg'];
			$thumb      = json_decode($_REQUEST['thumb_values'])->data;
			$top_img    = save_base64_image($thumb);

			$sql = "UPDATE campaign SET title='$title',
                                        story='$story',
                                        date_end='$enddate',
                                        amount_goal='$target',
                                        top_img='$top_img',
                                        country='$country',
                                        city='$city',
                                        category='$category',
                                        brief='$brief',
                                        video_title='$video_title',
                                        video_url='$video_url'
                                         WHERE id = $campid AND ownerid = $ownerid
";
		} else {
			$sql = "UPDATE campaign SET title='$title',
                                        story='$story',
                                        date_end='$enddate',
                                        amount_goal='$target',
                                        country='$country',
                                        city='$city',
                                        category='$category',
                                        brief='$brief',
                                        video_title='$video_title',
                                        video_url='$video_url'
                                         WHERE id = $campid AND ownerid = $ownerid
";
		}

		/*        $sql      = "
		INSERT INTO campaign(ownerid,title,story,date_end,amount_goal,top_img,country,city,category,brief)
		VALUES($ownerid,'$title','$story','$enddate',$target,'$top_img','$country','$city','$category','$brief');
		";*/
//echo $sql;
		$results = $mysqli->query($sql);
		if ($results) {
			if (isset($_REQUEST['thumb_values'])) {
				unlink("../".$old_topimg);
			}
			echo 'true';
		}
	}

}

function save_campaign_news_update() {
	global $mysqli;
	if (isset($_SESSION['owner'])) {
		$story  = $mysqli->real_escape_string($_REQUEST['mstory']);
		$campid = $mysqli->real_escape_string($_REQUEST['id']);

		$sql = "
			INSERT INTO campaign_updates(campid,story)
			VALUES($campid,'$story');
";

		$results = $mysqli->query($sql);
		if ($results) {
			echo 'true';
		}
	} else {
		echo 'false';
	}

}