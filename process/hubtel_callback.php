<?php
/**
 * Created by PhpStorm.
 * User: Mtech
 * Date: 21/09/2017
 * Time: 8:39 PM
 */

include ('../includes/allfunctions.php');
$callback_obj = file_get_contents("php://input");

/*$transaction_id   = $_REQUEST['transactionid'];
$response_code    = $_REQUEST['responsecode'];
$response_message = $_REQUEST['responsemessage'];*/
debug("Callback",$callback_obj);

$r = json_decode($callback_obj);

$d = $r->Data;
print_r($r);

$response_code = $r->ResponseCode;
$transaction_id = $d->ClientReference;

debug("Callback Response_code",$response_code);
if ($response_code == '0000') {
    update_donation(1,$transaction_id);
    update_transaction_report(1,$transaction_id);

    $email = get_donator_email_by_transid($transaction_id);
    $campid = get_donator_campid_by_transid($transaction_id);
    if(contains('@',$email)){
        send_automatic_thank_you($campid, $email);
        debug("Email Sent For Successful Donation",$email);
    }else{
        debug("SMS Sent For Failed Donation",$email);
    }

} else {
    update_donation(5,$transaction_id);
    update_transaction_report(5,$transaction_id);

    $email = get_donator_email_by_transid($transaction_id);
    $campid = get_donator_campid_by_transid($transaction_id);

    if(contains('@',$email)){
        send_donation_failed_thank_you($campid, $email);
        debug("Email Sent For Failed Donation",$email);
    }else{
        debug("SMS Sent For Failed Donation",$email);
    }

}

echo json_encode("success");
