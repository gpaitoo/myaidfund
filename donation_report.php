<?php
include ('includes/allfunctions.php');
$ownerid = -1;
$page    = 'donation_report';
include_once ('header.php');
$ownerid = $_SESSION['owner'];
if (isset($_REQUEST['crec'])) {
	$crec = $_REQUEST['crec'];
} else {
	$crec = get_first_camp($ownerid);
}
if (isset($_REQUEST['start_date']) && isset($_REQUEST['end_date'])) {
	$start_date = $_REQUEST['start_date'];
	$end_date   = $_REQUEST['end_date'];
}
$donation_report = get_donation_report($crec, $start_date, $end_date);
?>
<div class="container" style="padding-top: 10px;width: 95%;">

<?php include_once ('dashboard_header.php');?>
<div class="row">
        <section style="padding-bottom: 30px;">


            <div class="well span9" style="max-width:700px;min-width:150px; margin: 0px auto 0 auto;">
                <h2 style="padding: 10px; text-align: center; margin-bottom: 15px;">Donation Reports</h2>

    <div id="reportrange" class="" style="cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                    <br/>

<div id="donation_div">

<?php echo $donation_report;?>
</div>
            </div>


        </section>
    </div>
</div>




<?php include_once ('footer.php');?>
