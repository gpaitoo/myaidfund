<?php
include ('includes/allfunctions.php');
$ownerid = -1;
session_start();
$page = 'campaign_home';

if(!isset($_SESSION['owner'])){
    header("location: index.php");
}
$crec    = '';
$ownerid = $_SESSION['owner'];
if (isset($_REQUEST['crec'])) {
	$crec = $_REQUEST['crec'];
} else {
	$crec = get_first_camp($ownerid);
}

/*if (!isset($crec)) {
if(){

}else{
$crec = 0;
}

}*/

$title           = '';
$story           = '';
$date_created    = '';
$date_end        = '';
$amount_goal     = 0;
$amount_achieved = 0;
$top_img         = '';
$beneficiary     = '';
$message         = '';
$brief           = '';

if ($crec != -1) {
	$results = get_campaign_by_id($ownerid, $crec);
	//echo $results->num_rows;
	while ($row = $results->fetch_object()) {
		$title        = $row->title;
		$story        = $row->story;
		$date_created = $row->date_created;
		$date_end     = $row->date_end;
		$amount_goal  = $row->amount_goal;
		$top_img      = $row->top_img;
		$beneficiary  = $row->beneficiary;
		$message      = $row->message;
		$brief        = $row->brief;
	}
	$amount_achieved = get_donation_sum($crec);
}

$meta = '
<meta property="og:url"           content="https://myaidfund.com/campaign_details.php?donate_id='.$crec.'"/>
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="MyAidFund" />
  <meta property="og:image"         content="https://myaidfund.com/'.$top_img.'" />
  <meta property="og:description"   content="'.$brief.'" />

<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@MyaidFund"/>
<meta name="twitter:creator" content="@MyaidFund"/>
<meta name="twitter:title" content="'.$title.'"/>
<meta name="twitter:description" content="'.$brief.'"/>
<meta name="twitter:image" content="https://myaidfund.com/'.$top_img.'"/>
';
include_once ('header.php');
?>

<?
$currency = '$';
$crst     = 0;
if ($crec != -1) {
	$crst = determine_donation_currency($crec);

	if ($crst == 'USD') {
		$currency = '$';
	} else {
		$currency = 'GHS';
	}
}

$account_type = get_account_activation_details_by_owner($ownerid);
$doc_uploaded = get_is_doc_uploaded_by_owner($ownerid)
?>
<div class="container" style="margin-top: 10px;background-color: white; width:95%;">

<?php if ($crec == -1) {?>
	<div class="alert alert-info" role="alert">
	                You Have no campaign created. Use the "Create Page" to start a new campaign
	            </div>
	<?php }?>
        <?php include_once ('dashboard_header.php');?>
<br/>

    <?php
    $more_info = '<div class="alert alert-danger" role="alert">
        Kindly confirm your identity by providing us with some few information. Kindly click <a href="activation2.php" class="alert-link">here</a>.
    </div>';
    $waiting_approval = '<div class="alert alert-danger" role="alert">
        Your Document has been uploaded and it`s awaiting approval. Please allow 2 to 3 working days for approval.</div>';
    if(!empty($account_type)){
        echo $more_info;
    }
    if(!empty($doc_uploaded)){
        echo $waiting_approval;
    }


    ?>

    <div class="alert alert-info hide" role="alert">
        Kindly wait up to 24 hours while we confirm your identity. You can start creating your campaigns
    </div>

    <div class="alert alert-success hide" role="alert">
        This is a success alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
    </div>

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8 col-md-8 col-sm-8">

                <!-- Blog Post -->
                <br/>
                <div class="well">
                    <div style="max-height: 84px;min-height:84px;background-color: #333; padding: 10px 10px 0 10px;">
                        <div class="row">
                            <div class="col-md-7">
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label style="color: white">Select Report</label>
                                    <select class="form-control rpt_change" name="category" required="required">
                                        <option value="donation">DONATIONS</option>
                                        <option value="page">PAGE VIEWS</option>
<!--                                        <option>SUPPORTERS</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <canvas id="myChart" style="width:100%;min-height:400px;"></canvas>
                </div>

                <div class="well span9" style="max-width:700px;min-width:150px; margin: 50px auto 0 auto;">
                    <h2 style="padding: 10px; text-align: center; margin-bottom: 15px;">Automatic Thank You Message</h2>
                    <form id="thank_you" method="get" action="process/save_auto_message.php">
                        <input type="hidden" value="<?php echo $crec;?>" name="id"/>
                        <div class="form-group">
                            <label style="color: blue;font-size: medium;">Name of Beneficiary</label>

                            <input type="text" value="<?php echo $beneficiary;?>" class="form-control" name="beneficiary" placeholder="Beneficiary"/>
                        </div>

                        <div class="form-group">
                            <label style="color: blue;font-size: medium;">Message</label>

                            <textarea rows="4" class="form-control" name="message" placeholder="Message"><?php echo $message;?></textarea>
                        </div>


                        <button type="submit" class="btn btn-primary right">Save</button>

                    </form>
                </div>

                <div class="well span9" style="max-width:700px;min-width:150px; margin: 50px auto 0 auto;">
                    <h2 style="padding: 10px; text-align: center; margin-bottom: 15px;">Post An Update</h2>
                    <form id="campaign_news" method="get" action="process/save_campaign.php">
                        <input type="hidden" value="news_update" name="opera"/>
                        <input type="hidden" value="<?php echo $crec;?>" name="id"/>
                        <div class="form-group">
                            <label style="color: blue;font-size: medium;">Latest Update</label>
                            <p>Every update is a chance to inform and involve potential donors. Posts will appear on
                                your campaign page.</p>
                            <textarea class="form-control story" name="" placeholder="Your Story Here"></textarea>
                            <textarea class="hide mstory" name="mstory"></textarea>
                        </div>

                        <div class="checkbox">


                            <label><input class="" type="checkbox" name="agreement" value="Agree" required="required"/>I Agree to terms and conditions of MyAidfund<span style="color:red;" > *</span></label>
                        </div>

                        <button type="submit" class="btn btn-primary right">Post</button>

                    </form>
                </div>


            </div> <!-- /.col-md-5 -->
            <!-- Blog Sidebar Widgets Column -->
            <div class="col-lg-4 col-md-4 col-sm-4">

                <!-- Blog Search Well -->
                <br/>
                <div class="well">
                    <h4>Search Campaigns</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <br/>

                <div class="well">
                    <div class="alert alert-error" role="alert">
                        To receive fund raised, sends us a letter of confirmation from your bankers on their letterhead confirming your account details with them at our chosen address;

                        <p>Apptechhub Global limited</p>

                        <p>114 Nkwame Nkrumah Avenue,</p>

                        <p>Adabraka, Accra.</p>

                        <p>Or</p>

                        <p>P. O. Box GP 22485,</p>

                        <p>Accra Central.</p>
                    </div>
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Support Us</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well text-center">


                                <div class="f-p-t-medium text-left">
                                    <h3 class="f-lh-xsmall" style="font-size: 25px; margin:0px;">
<?php
echo get_number_of_donations($crec);
?>
</h3>
                                    <a class="scroll f-font-condensed f-caps f-fw-bold " href="#">
                                        Donors
                                    </a>
                                </div>
                                <!-- AMOUNT DONATED -->
                                <br/>
                                <div class="f-m-t-medium text-left">
                                    <h3 class="f-m-t-small f-lh-xsmall js-raise" style="font-size: 25px; margin:0px;">
<?echo $currency."<strong>".number_format($amount_achieved)."</strong>";?>
                                    </h3>
                                    <a class="scroll f-font-condensed f-ilb f-caps f-fw-bold">
                                        Raised
                                        <span>
(<?php echo $crst?>)
</span>
                                    </a>
                                </div>
                                <br/>


                                <div class="f-m-t-medium">
                                    <div class="f-thermometer f-m-b-xsmall">
                                        <div class="f-mercury f-bg-primary"
                                             style="width: <?echo (($amount_achieved/$amount_goal)*100);?>%"></div>
                                    </div>


                                    <div class="f-font-condensed text-left">
<span class="f-fw-bold">
Goal:
</span>
                                        <span class="js-campaign-goal-target">
<?echo $currency.number_format($amount_goal);
?>
</span>
                                    </div>

                                    <div class="f-fr f-font-condensed text-left">
<span class="f-fw-bold">
Days Left:
</span>
                                        <span class="js-days-left-target">0</span>
                                    </div>

                                </div>

                                <br/>

                                <!--                                NET PAYABLE-->
                                <div class="f-p-t-medium text-left">
                                    <h3 class="f-lh-xsmall" style="font-size: 25px; margin:0px;">
                                        <?php
                                        $payable = get_net_payable($crec);
                                        if($payable > 0){
                                            echo $currency."<strong>$payable</strong>";
                                        }else{
                                            echo $currency."<strong>0</strong>";
                                        }

                                        ?>
                                    </h3>
                                    <a class="scroll f-font-condensed f-caps f-fw-bold " href="#">
                                        Net Payable
                                    </a>
                                </div>

                                <!-- <button style="width:100%; margin-top:10px;" type="button" class="btn btn-hot text-uppercase btn-lg" data-toggle="modal" data-target="#myModal">Donate <span class="glyphicon glyphicon-heart" aria-hidden="true"></span></button> -->

                                <button style="width:100%; margin-top:10px;" type="button"
                                        class="btn btn-facebook btn-lg" campid="<?echo $crec;?>"><i class="fa fa-facebook fa-2"></i> Share
                                </button>
                                <a target="_blank" href="https://twitter.com/share?url=https://myaidfund.com/campaign_details.php?donate_id=<?echo $crec;?>&via=MyAidFund&related=Help Raise Fund&hashtags=MyAidFund&text=<?echo substr($title, 0, 140);?>">
                                <button style="width:100%; margin-top:10px;" type="button"
                                        class="btn btn-twitter btn-lg"><i class="fa fa-twitter fa-2"></i> Share
                                </button>
                                </a>

                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>


            </div>
        </div> <!-- /.row -->

    </div> <!-- /.container -->


<?php include_once ('footer.php');?>