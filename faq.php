<?php
$page = 'FAQ';
include_once ('header.php');
include ('includes/allfunctions.php');

?>

    <style>
        .container{
            width:95%;
        }
    </style>

    <div class="team-wrapper" style="padding-top: 0px" id="our_mission">
        <div class="container">
            <div class="section-name one">
                <h2>Frequently Asked Questions</h2>
                <br/>
                <div class="tags" style="text-align: left;">

                    <p>
                        <strong>What are the guidelines for creating campaign?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        1. Campaigns must fall under one of the following categories listed on our
                        website: health, education, politics &amp; Public office, special events,
                        sports &amp; teams, clubs &amp; community, creative projects and other
                    </p>
                    <p>
                        2. Campaigns can’t be fraudulent, polemic, sectarian, or violent in nature.
                    </p>
                    <p>
                        3. We do not run any campaigns where the campaign organizer is anonymous to
                        the public. If a campaign is related to supporting an individual or family
                        through emergency assistance funds, the campaign creator name will be
                        displayed on the campaign and publicly vouch on behalf of the individuals
                        he/she is crowdfunding for. If possible, we prefer that the campaign
                        creator partner up with a 501c3 charity or an NGO or reputable institution
                        where we can deposit the funds.
                    </p>
                    <p>
                        This is the established standard for transparency and accountability
                        required for us to protect our supporters, safeguarding against any
                        potential cases of fraud.
                    </p>
                    <p>
                        To read more about our Terms of Use, click    <a href="https://myaidfund.com/terms_of_use.php">Terms of Use</a>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong>How do I get funds raised?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong>Everything You Need to Receive Funds on MyAidFund</strong>
                    </p>
                    <p>
                        You'll need to verify two things in order to receive your funds
                        successfully on MyAidFund.
                    </p>
                    <p>
                        <strong>1. Your identity.</strong>
                    </p>
                    <p>
                        To do this, you will need to fill out your personal details such as name,
                        address, phone number, TIN and date of birth (must be at least 18 years of
                        age). If this information can't be automatically verified, then there's a
                        chance you'll need to provide a copy of your government issued photo ID.
                        All of the following are acceptable forms of ID:
                    </p>
                    <ul>
                        <li>
                            Valid Passports
                        </li>
                        <li>
                            Driver's Licenses
                        </li>
                        <li>
                            Voters ID
                        </li>
                        <li>
                            National ID Card
                        </li>
                    </ul>
                    <p>
                        · For organizations you need to provide your business registration
                        documents in addition with ID of a director of the company
                    </p>
                    <p>
                        <strong>2. Your banking information.</strong>
                        You will need to use a bank account in your name that is located in the
                        country you signed up in.
                    </p>
                    <p>
                        You’ll be able to fill out your bank information from within your account
                        and provide a letter from your bankers addressing to us confirming your
                        account details with them. This will help protect you against missing
                        withdrawals and other related issues.
                    </p>
                    <p>
                        Final tip: If you do have to upload any of the above documentation, it will
                        take our payment partner up to 2 business day to review and approve the
                        information you sent over. This can extend the time it takes for you to
                        receive your funds, so it’s important to plan around that.
                    </p>
                    <p>
                        Note that bank deposits also take 2-5 business days to arrive from the time
                        they are sent.
                    </p>
                    <p>
                        <strong>To get this process started:</strong>
                    </p>
                    <ul>
                        <li>
                            First, sign in to your account.
                        </li>
                    </ul>
                    <p>
                        · For organizers or beneficiaries on a desktop or laptop computer, you will
                        click the 'Request Payment' button at the top of your Dashboard.
                    </p>
                    <p>
                        · Follow the prompts from there to enter the necessary information.
                    </p>
                    <p>
                        · If you have trouble with any step, just contact our team.
                    </p>
                    <p>
                        To receive funds raised, send us a letter of confirmation from your bankers
                        on their letterhead confirming your account details with them at our chosen
                        address <strong>(This apply to campaigns created in Ghana)</strong>;
                    </p>
                    <p>
                        Apptechhub Global limited
                    </p>
                    <p>
                        114 Nkwame Nkrumah Avenue,
                    </p>
                    <p>
                        Adabraka, Accra. Or
                    </p>
                    <p>
                        P. O. Box GP 22485,
                    </p>
                    <p>
                        Accra Central.
                    </p>
                    <p>
                        <strong>How does it work?</strong>
                    </p>
                    <p>
                        <strong> </strong>
                    </p>
                    <p>
                        <strong>Step 1:</strong>
                    </p>
                    <p>
                        <strong>Create an account and log in </strong>
                    </p>
                    <p>
                        Click on <strong>“create campaign”</strong> on the web to create an account
                        by completing the registration form. After submitting the completed
                        registration form an email is sent to the provided email address during the
                        registering for validation, then you can log in to your account.
                    </p>
                    <p>
                        <strong>Step 2:</strong>
                    </p>
                    <p>
                        <strong>
                            Upload your personal Identification and/or Business registration
                            document for verification
                        </strong>
                    </p>
                    <p>
                        In accordance with national laws and central bank’s guidelines on KYC (Know
                        Your Customer), we are mandated to verify every person or organization who
                        have created an account on our website, in order to allow your campaign to
                        go live on our website and start accepting donation. We verify personal ID
                        (Passport, Voters ID, Drivers Licence etc) for personal accounts and
                        business documents, business owner or Director ID for business or
                        organization accounts.
                    </p>
                    <p>
                        <strong>Step 3:</strong>
                    </p>
                    <p>
                        <strong>
                            Create your fundraising Campaign, Share with Friends, Family and love
                            ones on social media platforms
                        </strong>
                    </p>
                    <p>
                        There is no easier way to share your story and attract support from
                        friends, family and love one cross the world. Our built in connections to
                        Facebook, twitter, whatsapp etc. make sharing your campaign much easier.
                        After creating campaign our team verifies the campaign before approving it
                        to show live on our website.
                    </p>
                    <p>
                        <strong>Step 4:</strong>
                    </p>
                    <p>
                        <strong>Start accepting Donations</strong>
                    </p>
                    <p>
                        You can receive your money by requesting a bank transfer or cheque after
                        filling your bank account details and providing us with a confirmation
                        letter from your banker’s confirming your accounts details with the bank on
                        the banks letterhead.
                    </p>
                    <p>
                        <strong>Step 5:</strong>
                    </p>
                    <p>
                        <strong>Monitor your Campaign results</strong>
                    </p>
                    <p>
                        With our real time updates on donations you can monitor your campaign
                        performance anytime, anywhere. You can also make changes, post updates and
                        send thank you notes from your dashboard.
                    </p>
                    <p>
                        You can watch a video overview about how to create an account and campaign
                        by using the link below;
                    </p>
                    <p>
                        <a href="https://www.youtube.com/watch?v=vEwJGRoQO54&amp;t">
                            https://www.youtube.com/watch?v=vEwJGRoQO54&amp;t
                        </a>
                    </p>
                    <p>
                        <strong>How much will it cost me to use myaidfund platform?</strong>
                    </p>
                    <p>
                        While it's free to create and share your online fundraising campaign,
                        Myaidfund will deduct a 5% fee from each donation that you receive. A small
                        payment processing fee will also be deducted from each donation depending
                        on the payment method the donor uses.
                    </p>
                    <p>
                        We also provide automatic discounts for larger campaigns!
                    </p>
                    <p>
                        For information about Myaidfund.com’s pricing, please see
                        <a href="https://myaidfund.com/pricing.php">
                            Myaidfund.com’s pricing page
                        </a>
                        . Myaidfund.com reserves the right to change its pricing at any time, with
                        or without notice. Myaidfund.com will post any changes to the Pricing on
                        the Pricing page, so please check often. All fees are exclusive of all
                        taxes, levies, or duties imposed by taxing authorities, and User shall be
                        responsible for payment of all taxes, levies, or duties associated with
                        User’s purchases hereunder.
                    </p>
                    <p>
                        <strong>
                            Does myaidfund verifies campaign and campaign organizer or charity?
                        </strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        Myaidfund team reviews every single campaign on site to make sure they
                        follow our general guidelines, and they are transparent and clear. Also at
                        times using screening services such Stripe and other third party’s services
                        to further verify campaign creators/organizations.
                    </p>
                    <p>
                        Although Myaidfund team reviews campaigns, we cannot verify all the
                        information that Campaign Organizers supply, nor do we guarantee that the
                        Donations will be used in accordance with any fundraising purpose
                        prescribed by a Campaign Organizer or Charity. We assume no responsibility
                        to verify whether the Donations are used in accordance with any applicable
                        laws; such responsibility rests solely with the Campaign Organizer or
                        Charity, as applicable.
                    </p>
                    <p>
                        You, as a Donor, must make the final determination as to the value and
                        appropriateness of contributing to any Campaign, Campaign Organizer, or
                        Charity. We encourage donors to use their discretion when supporting
                        campaigns.
                        <br/>
                        <br/>
                        We do take possible fraudulent activity and the misuse of funds raised very
                        seriously. If you have reason to believe that a Campaign Organizer or
                        Charity is not raising or using the funds for their stated purpose, please
                        do alert our team of this potential issue and we will investigate by
                        clicking on the <strong>Report button </strong>of the campaign in question
                        or send us a message to    <a href="mailto:support@myaidfund.com">support@myaidfund.com</a>
                    </p>
                    <p>
                        <strong>Why does my campaign need to be reviewed?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        At Myaidfund we want to make sure that our campaign creators are successful
                        on their crowdfunding campaign and are assigned an expert coach who will be
                        best suitable for their campaign.
                    </p>
                    <p>
                        We review the campaigns for verification purposes as well, to avoid fraud.
                    </p>
                    <p>
                        <strong>
                            Can I edit my campaign after it has been submitted for review or live?
                        </strong>
                    </p>
                    <p>
                        Yes, you can edit your campaign at any time, including after it's been
                        submitted for review and during the course of your campaign.
                    </p>
                    <p>
                        <strong>What picture size should I use?</strong>
                    </p>
                    <p>
                        Use 600 by 338 - pixel pictures or you can upload the image into the image
                        box and use the edit tool to crop it and position it the way you want it to
                        show on the campaign page.
                    </p>
                    <p>
                        <strong>How can I add pictures to my campaign story?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        To add pictures to your campaign pitch <strong></strong>
                    </p>
                    <p>
                        <strong> </strong>
                    </p>
                    <p>
                        1. Go on the Tell Your Story section<strong></strong>
                    </p>
                    <p>
                        <strong> </strong>
                    </p>
                    <p>
                        2. You will see the text editor tool at the top of the page you are writing
                        your story<strong></strong>
                    </p>
                    <p>
                        3. Click on the button first from the right lebelled “<strong>IMAGE” </strong>to add the preferred picture to your story    <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong> </strong>
                    </p>
                    <p>
                        <strong>How can I add a video?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        To add a video
                    </p>
                    <p>
                        1. Click on the Edit tab after you login to your dashboard
                    </p>
                    <p>
                        2. Scroll down to the end of the page a space is provided to paste your
                        YouTube link
                    </p>
                    <p>
                        3. Insert your Youtube link in the format provided.
                    </p>
                    <p>
                        <strong>
                            NOTE: Make sure your Youtube link in only the format provided or it
                            will not show on your campaign page
                        </strong>
                    </p>
                    <p>
                        <strong> </strong>
                    </p>
                    <p>
                        <strong>How can I support a campaign as a donor?</strong>
                    </p>
                    <p>
                        You can support a campaign by clicking on the donate button of the campaign
                        to donate and/or share it on your social media timelines or WhatsApp groups
                        for your friends to also support.
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong>What happens if campaign creators do not fulfill perks?</strong>
                    </p>
                    <p>
                        We periodically ask campaign creators to make sure they fulfill promises
                        made in their campaigns. Ultimately, it boils down to a relationship of
                        trust between campaign creators and their supporters.
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong>What happens if campaign creators do not fulfill perks?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        To get an official tax receipt
                    </p>
                    <p>
                        1. Go to the campaign page you donated to
                    </p>
                    <p>
                        2. Click Send Email
                    </p>
                    <p>
                        3. Reach out to the Campaign creator for a tax receipt
                    </p>
                    <p>
                        <strong>Why didn’t my donation go through?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        Donations may fail for a few reasons, including:
                    </p>
                    <p>
                        · typos in your card number, CVV code or postal/zip code
                    </p>
                    <p>
                        · donation name doesn't match your card's billing name
                    </p>
                    <p>
                        · insufficient funds or card declines by credit card issuers (can happen if
                        your donation attempt seems outside your normal spending habits)
                    </p>
                    <p>
                        · browser issues or issues with your internet connection (sending encrypted
                        card information requires a strong internet connection)
                    </p>
                    <p>
                        Here are a few ways to troubleshoot this:
                    </p>
                    <p>
                        1. Enter the registered name exactly as seen on your billing statements.
                    </p>
                    <p>
                        2. Try your donation again using a different web browser and/or device.
                    </p>
                    <p>
                        3. If all else fails, call your bank and ask them to make sure they aren't
                        blocking the payment
                    </p>
                    <p>
                        If that didn’t work, please contact    <a href="mailto:support@myaidfun.com">support@myaidfun.com</a> and we'll
                        help you troubleshoot your donation. Make sure you let us know if you've
                        already tried some of our troubleshooting tips!
                    </p>
                    <p>
                        If you see extra charges let us know and we will troubleshoot it for you.
                    </p>
                    <p>
                        <strong>
                            For mobile money users in Ghana, it could be insufficient balance on
                            your wallet, wrong pin, exceeded your daily spend limit or network
                            issues. In any case contact your network operator to help you resolve
                            it.
                        </strong>
                    </p>
                    <p>
                        <strong>Is it safe to donate on myaidfund?</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        Yes, Myaidfund uses the same secure &amp; encrypted payments technology as
                        your bank to ensure your donation is processed safely. We use Stripe to
                        ensure your donations are made safely. No credit card information can be
                        accessed by us or the campaign creators.
                        <br/>
                        <br/>
                        If you have questions about a campaign, you should first try to contact the
                        Campaign Creator directly by visiting their campaign and clicking on the
                        Send Email button beside their name. If you still have concerns that a page
                        is misleading or fraudulent, let our team know by clicking on the<strong>Report</strong> button on the campaign to alert us or by emailing<a href="mailto:support@myaidfun.com">support@myaidfun.com</a>    <strong></strong>
                    </p>
                    <p>
                        <strong>Is myaidfund a Non-profit or 501c3?</strong>
                    </p>
                    <p>
                        No, MyaidFund is not a Non-Profit.
                    </p>

                </div>
            </div>


        </div>
    </div>





    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>