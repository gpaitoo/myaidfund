<?php
//echo get_auto_message("Yaw","Thank you");
function get_activation_message($code,$email)
{
    $msg = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Activation</title>
</head>
<body>
<div bgcolor="#F6F5F2" style="background:#f6f5f2;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0">



    <table align="center" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F6F5F2" style="background:#f6f5f2;margin-top:0;text-align:center;width:100%">
        <tbody>
        <tr>
            <td style="min-width:8px"></td>
            <td height="18px">&nbsp;</td>
            <td style="min-width:8px"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                    <tr>
                        <td align="left" style="text-align:left">
                            <a href="">
                                <img src="https://myaidfund.com/assets/img/logo.png" width="111" height="" border="0" style="border:none">
                            </a>
                        </td>
                        <td align="right" style="text-align:right">
                            <a href="https://mydaidfund.com" style="color:#5f7d19!important;color:#5f7d19;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;text-decoration:none!important;text-decoration:none" target="_blank" ><font style="color:#5f7d19!important;color:#5f7d19">Create a Campaign</font></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="23px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="center" width="500" style="text-align:center">


                <table align="center" cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#FFFFFF" style="background:#ffffff;border-width:1px;border-style:solid;border-color:#e6e6e6;border-radius:5px 5px 5px 5px;max-width:500px;min-width:320px;text-align:left;width:100%">
                    <tbody>
                    <tr>
                        <td>

                            <table align="center" style="width:100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>

                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td height="31">&nbsp;</td>
                                    <td width="14">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td>

                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%">
                                            <tbody>

                                            <tr>
                                                <td style="color:#666666;font-family:Arial,Helvetica,sans-serif;font-size:16px;line-height:24px;text-align:left">
                                                    From: MyAidFund
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="8" style="font-size:8px;line-height:8px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="1" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d4d4d4;font-size:16px;line-height:16px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center">

                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td width="320">

                                                                <table align="center" cellspacing="0" cellpadding="0" border="0" style="width:100%;max-width:320px">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:24px">
                                                                            Your Account has been set up successfully. To activate it, Kindly click the link below or copy and paste the url in your internet browser.
                                                                            <br/><a href="https://myaidfund.com/activation.php?em='.$email.'&ec='. $code .'" >https://myaidfund.com/activation.php?em='.$email.'&ec='.$code.'</a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr><tr>
                                                <td height="13" style="font-size:13px;line-height:13px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="1" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d4d4d4;font-size:16px;line-height:16px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="30">&nbsp;</td>
                                            </tr>
                                            

                                            </tbody>
                                        </table>

                                    </td>
                                    <td width="14">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>


                        </td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="26"></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="color:#999999;font-family:Helvetica,Arial,sans-serif;line-height:20px;font-size:12px">
                Sent from MyAidfund\'s Headquarters:
                <br>
                <a href="#" style="color:#999999!important;text-decoration:none!important;color:#999999;text-decoration:none">114 kwame nkrumah Ave. Adabraka - Accra, Ghana</a>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="50"></td>
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>

    <div style="display:none;white-space:nowrap;font:15px courier;color:#f6f5f2">
        - - - - - - - - - - - - - - - - - - -
    </div>


    <img src="" alt="" width="2" height="2" class="CToWUd" style=""></div>
</body>
</html>';
    return $msg;
}
