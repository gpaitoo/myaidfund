<?php
include ('config.php');
include ('thankyouemail.php');

//ALL CONFIG
$pagn_size = 20;
function get_special_cause() {
	global $mysqli;
	$results = $mysqli->query("SELECT * FROM campaign where id = 5 AND publish_status='approved'");
	//echo $results->num_rows;
	return $results->fetch_object();

}

function get_three_cause() {
	global $mysqli;
	$results = $mysqli->query("SELECT * FROM campaign WHERE publish_status='approved' ORDER BY id DESC limit 9 ");
	//echo $results->num_rows;
	return $results;

}

function get_cause_by_catg($catg, $of) {
	global $mysqli, $pagn_size;
	$nof     = $of*$pagn_size;
	$results = $mysqli->query("SELECT * FROM campaign WHERE category='$catg' AND publish_status='approved' ORDER BY id DESC limit $nof, $pagn_size");
	//echo $results->num_rows;
	return $results;

}

function get_total_cause_by_catg($catg) {
	global $mysqli, $pagn_size;
	$results = $mysqli->query("SELECT COUNT(id) as t FROM campaign WHERE category='$catg' AND publish_status='approved'");
	$total   = 0;
	while ($r = $results->fetch_object()) {
		$total = $r->t;
	}
	$rt = ceil($total/$pagn_size);
	return $rt;

}

function get_owner_campaign($ownerid) {
	global $mysqli;
	$results = $mysqli->query("SELECT id,title FROM campaign WHERE ownerid = $ownerid ORDER BY id DESC");
	//echo $results->num_rows;
	return $results;
}

function get_owner_name($ownerid) {
	global $mysqli;
	$results  = $mysqli->query("SELECT fullname FROM account WHERE id = $ownerid");
	$fullname = '';
	while ($r = $results->fetch_object()) {
		$fullname = $r->fullname;
	}
	return $fullname;
}

function get_campaign_by_id($owner, $id) {
	global $mysqli;
	$results = $mysqli->query("SELECT * FROM campaign where ownerid = $owner AND id = $id");
	//echo $results->num_rows;
	return $results;
}

function get_campaign_update_by_id($id) {
    global $mysqli;
    $results = $mysqli->query("SELECT story FROM campaign_updates where campid = $id ORDER BY id DESC LIMIT 1");
    $rst = '';
    if($results->num_rows > 0){
        while ($r = $results->fetch_object()) {
            $rst = $r->story;
        }
    }

    return $rst;
}

function get_first_camp($ownerid) {
	global $mysqli;
	$results = $mysqli->query("SELECT id FROM campaign WHERE ownerid = $ownerid ORDER BY id DESC LIMIT 1");
	$r       = $results->fetch_object();
	if (isset($r->id)) {
		return $r->id;
	} else {
		return 0;
	}

}

function begin_session($id, $owner) {
	session_start();
	$_SESSION['timeout'] = time();
	$_SESSION['id']      = $id;
	$_SESSION['owner']   = $owner;
}

function letin($username, $password) {
	global $mysqli;
	$sql     = "SELECT id,email FROM account WHERE email = '$username' AND xxxpass = '$password' AND isactive = 1 LIMIT 1";
	$results = $mysqli->query($sql);
	$r       = $results->fetch_object();
	if ($results->num_rows > 0) {
		begin_session($r->email, $r->id);
		return 'true';
	} else {
		return 'false';
	}
}

function insert_reset_code($code,$email){
    global $mysqli;
    $sql = "UPDATE account SET reset_key = '$code' WHERE email = '$email'";
    $rst = $mysqli->query($sql);
    $r = $mysqli->affected_rows;
    if ($r > 0) {
        return true;
    } else {
        return false;
    }
}

function check_email($email) {
    global $mysqli;
    $sql = "
		SELECT id FROM account WHERE email = '$email'
";
    $results = $mysqli->query($sql);
    $num_row = $results->num_rows;
    return $num_row;
}

function update_letin($code, $password) {
    global $mysqli;
    $password=md5($password);
    $sql     = "UPDATE account SET xxxpass = '$password',reset_key = '' WHERE reset_key='$code'";
    $results = $mysqli->query($sql);
    $r       = $mysqli->affected_rows;
    if ($r > 0) {
        return 'true';
    } else {
        return 'Sorry you are not allowed to change this password';
    }
}

function get_owner_id($email) {
	global $mysqli;
	$results = $mysqli->query("SELECT id FROM account WHERE email = '$email' LIMIT 1");
	$r       = $results->fetch_object();
	return $r->id;
}

function get_number_of_donations($campid) {
    global $mysqli;
    $results = $mysqli->query("SELECT count(id) as totaldonation FROM donations where campid = $campid AND transstate=1");
    $rs      = 0;

    while ($r = $results->fetch_object()) {
        $rs = $r->totaldonation;
    }
    return $rs;
}

function get_net_payable($campid) {
    global $mysqli;
    $results = $mysqli->query("SELECT SUM(net_payable_campaign_currency) as netpayable FROM transaction_reports where campid = $campid AND transstate=1");
    $rs      = 0;

    while ($r = $results->fetch_object()) {
        $rs = $r->netpayable;
    }
    return $rs;
}

function get_graph_donations($campid) {
	global $mysqli;
	$sql    = "SELECT DATE_FORMAT(donation_when, '%d/%m/%Y') as ddate, count(id) as donations FROM donations WHERE campid=$campid AND transstate=1 GROUP BY ddate ORDER BY ddate DESC";
	$result = $mysqli->query($sql);
	$items  = array();

	while ($r = $result->fetch_array()) {
		$items[] = array(
			"date"      => trim($r['ddate']),
			"donations" => $r['donations'],
		);
	}
	$items = array_reverse($items);
	return json_encode($items);
}

function get_graph_page_view($campid) {
	global $mysqli;
	$sql = "SELECT DATE_FORMAT(logwhen, '%d/%m/%Y') as ddate, sum(hit) as hits FROM page_hits WHERE campid=$campid GROUP BY ddate ORDER BY ddate DESC";
	//echo $sql;
	$result = $mysqli->query($sql);
	$items  = array();

	while ($r = $result->fetch_array()) {
		$items[] = array(
			"date" => trim($r['ddate']),
			"hits" => $r['hits'],
		);
	}
	$items = array_reverse($items);
	return json_encode($items);
}

function get_donation_sum($campid) {
	global $mysqli;
	$d_rate = get_dollar_rate();
	$currency = determine_donation_currency($campid);
	$sql      = "SELECT transmode,amount FROM donations WHERE campid = $campid AND transstate = 1";
	$results  = $mysqli->query($sql);
	$rs       = 0;
	$tstripe  = 0;
	$tmm      = 0;

	while ($r = $results->fetch_object()) {
		$mode = $r->transmode;
		if ($mode == 'stripe') {
			$tstripe += $r->amount;
		} else {
			$tmm += $r->amount;
		}
	}

	if ($currency == 'GHS') {
		$tstripe = $tstripe * $d_rate;
		$rs = $tstripe + $tmm;
	} else {
	    $mtodollars = $tmm/$d_rate;
	    $rs = $tstripe + $mtodollars;
		//$tmm = $tmm * $d_rate;
	}
	debug("all function- get_donation_sum",$rs);
	return round($rs,2);
}

function determine_donation_currency($campid) {
	global $mysqli;
	$sql     = "SELECT country FROM campaign WHERE id = $campid";
	$results = $mysqli->query($sql);

	$r = $results->fetch_object();
	if (isset($r->country)) {
		$c = $r->country;
	} else {
		$c = '';
	}

	if ($c == 'Ghana') {
		return "GHS";
	} else {
		return "USD";
	}
}

function update_activation_code($code) {
    global $mysqli;
    $sql     = "UPDATE account SET isactive = 1,activate_key='validated' WHERE activate_key='$code'";
    $results = $mysqli->query($sql);
    if($mysqli->affected_rows > 0){
        return true;
    }else{
        return false;
    }
}

function update_account_document_upload($email) {
    global $mysqli;
    $sql     = "UPDATE account SET document_uploaded = 1 WHERE email='$email'";
    debug("All Functions",$sql);
    $results = $mysqli->query($sql);
    if($mysqli->affected_rows > 0){
        return true;
    }else{
        return false;
    }
}

function update_account_document_path($email,$path,$id_type,$id_number) {
    global $mysqli;
    $sql     = "UPDATE account SET document_path = '$path',id_type='$id_type',id_number='$id_number' WHERE email='$email'";

    debug("All Functions",$sql);
    $results = $mysqli->query($sql);
    if($mysqli->affected_rows > 0){
        return true;
    }else{
        return false;
    }
}

function insert_into_personal_doc($ownerid,$dob,$nationality,$address,$id_type,$id_number,$id_path,$phone) {
    global $mysqli;
    $sql     = "INSERT INTO `personal_doc`(`ownerid`, `dob`, `nationality`, `resident`, `phone`, `id_type`, `id_number`, `document_path`) 
                VALUES ($ownerid,'$dob','$nationality','$address','$phone','$id_type','$id_number','$id_path')";
    debug("All Functions",$sql);
    $results = $mysqli->query($sql);
    if($mysqli->affected_rows > 0){
        return true;
    }else{
        return false;
    }
}


function insert_into_business_doc($ownerid,$business_name,$business_type,$business_location,$business_registration_doc_path,$dfullname,$dob,$nationality,$address,$id_type,$id_number,$id_path) {
    global $mysqli;
    $sql     = "INSERT INTO `business_doc`(`ownerid`, `business_name`, `business_type`, `business_location`, `business_registration_doc_path`, `d_fullname`, `d_dob`, `d_nationality`, `d_address`, `d_id_type`, `d_id_number`, `d_id_path`) 
                VALUES ($ownerid,'$business_name','$business_type','$business_location','$business_registration_doc_path','$dfullname','$dob','$nationality','$address','$id_type','$id_number','$id_path')";
    debug("All Functions",$sql);
    $results = $mysqli->query($sql);
    if($mysqli->affected_rows > 0){
        return true;
    }else{
        return false;
    }
}

function check_account_status($email) {
    global $mysqli;
    $sql     = "SELECT id FROM account WHERE isactive=1 AND email = '$email'";
    $results = $mysqli->query($sql);
    if($results->num_rows > 0){
        return true;
    }else{
        return false;
    }
}

function check_can_change_password($email,$code) {
    global $mysqli;
    $sql     = "SELECT id FROM account WHERE email = '$email' AND reset_key = '$code'";
    $results = $mysqli->query($sql);
    if($results->num_rows > 0){
        return true;
    }else{
        return false;
    }
}

function get_account_activation_details($email) {
    global $mysqli;
    $sql     = "SELECT account_type FROM account WHERE document_uploaded = 0 AND email = '$email'";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->account_type;
    }
    return $rst;
}

function get_account_activation_details_by_owner($owner) {
    global $mysqli;
    $sql     = "SELECT account_type FROM account WHERE document_uploaded = 0 AND id = $owner";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->account_type;
    }
    return $rst;
}

function get_is_doc_uploaded_by_owner($owner) {
    global $mysqli;
    $sql     = "SELECT account_type FROM account WHERE document_uploaded = 1 AND documents_verified = 0 AND id = $owner";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->account_type;
    }
    return $rst;
}

function get_account_email_by_owner_id($ownerid) {
    global $mysqli;
    $sql     = "SELECT email FROM account WHERE id = '$ownerid'";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->email;
    }
    return $rst;
}

function get_account_id_by_email($email) {
    global $mysqli;
    $sql     = "SELECT id FROM account WHERE email = '$email'";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->id;
    }
    return $rst;
}

function save_base64_image($data) {

	list($type, $data) = explode(';', $data);
	list(, $data)      = explode(',', $data);
	$data              = base64_decode($data);
	$nm                = "myaid".date("YmdHis").rand(500, 99999999).".jpg";
	file_put_contents("../camp_images/$nm", $data);
	return 'camp_images/'.$nm;
}

function insert_donation($campid, $email, $isregistered, $donatorid, $fullname, $phone, $amount, $transmode, $transid, $trans_source) {
	global $mysqli;
	$ownerid = "(SELECT ownerid FROM campaign WHERE id = $campid LIMIT 1)";
	$sql     = "INSERT INTO donations(campid, ownerid, amount, phone, email, isregistered, donatorid, donation_when, donator,transmode,transid,transstate,trans_source)
    VALUES ($campid,$ownerid,$amount,'$phone','$email',$isregistered,$donatorid,now(),'$fullname','$transmode','$transid',0,'$trans_source')
";
    debug("INSERT DONATION SQL",$sql);

	insert_and_process_transaction_report($campid,$transmode,$amount,$transid);
	$results = $mysqli->query($sql);
	return $results;
}


function insert_and_process_transaction_report($campid,$gateway,$amount,$transid){
    global $mysqli;
    $ownerid = "(SELECT ownerid FROM campaign WHERE id = $campid LIMIT 1)";

    $gross_amount = $amount;
    $currency = determine_currency($gateway);
    $transaction_processing_fee_visa = get_transaction_processing_fee_visa($gateway,$amount);
    $transaction_processing_fee_mm = get_transaction_processing_fee_mm($gateway,$amount);
    $net_receivable_from_gateway= $amount - ($transaction_processing_fee_mm + $transaction_processing_fee_visa);
    $platform_fee= get_platform_fee($campid,$amount);
    $other_revenue= get_other_revenue($gateway,$amount);
    $total_revenue= $platform_fee + $other_revenue;
    $total_deductable= $transaction_processing_fee_visa + $transaction_processing_fee_mm + $platform_fee+ $other_revenue;
    $net_payable= $amount-$total_deductable;
    $net_payable_ghc = get_total_revenue_or_payable_ghc($gateway,$net_payable);
    $total_revenue_ghc = get_total_revenue_or_payable_ghc($gateway,$total_revenue);
    $payment_mode = get_payment_mode($gateway);
    $net_payable_camp_currency = get_net_payable_campaign_currency($campid,$net_payable,$gateway);
    $campaign_currency=determine_donation_currency($campid);

    $sql="
    INSERT INTO `transaction_reports`(`campid`,`transid`, `ownerid`, `payment_mode`, `gateway`, `currency_type`, `gross_amount`, `transaction_fee_visa` , `transaction_fee_mm` , `gateway_net`, `platform_fee`, `other_revenue`, `total_revenue`, `total_deduction_per_transaction`, `net_payable`,`total_revenue_ghc`, `net_payable_ghc`,`campaign_currency`,`net_payable_campaign_currency`) 
                              VALUES ($campid,'$transid',$ownerid,'$payment_mode','$gateway','$currency',$gross_amount,$transaction_processing_fee_visa,$transaction_processing_fee_mm,$net_receivable_from_gateway,$platform_fee,$other_revenue,$total_revenue,$total_deductable,$net_payable,$total_revenue_ghc,$net_payable_ghc,'$campaign_currency',$net_payable_camp_currency);
    ";
    debug('Transaction Report',$sql);
    $results = $mysqli->query($sql);
}

function get_net_payable_campaign_currency($campid,$value,$gateway){
    $camp_currency = determine_donation_currency($campid);
    switch ($camp_currency){
        case 'GHS':
            if($gateway!='stripe' && $gateway!='paypal'){
                $out = $value;
            }else{
                $out = get_dollar_rate() * $value;
            }

            break;
        case 'USD':
            if($gateway=='stripe' || $gateway=='paypal' || $gateway =='judopay'){
                $out = $value;
            }else{
                $out = $value/(get_dollar_rate()+0.25);
            }

            break;
        default:
            $out=0;
            break;

    }
    return $out;
}

function get_total_revenue_or_payable_ghc($gateway,$value){
    switch ($gateway){
        case 'mtn':
        case 'airtel':
        case 'tigo':
        case 'vodafone':
        case 'hubtel_visa':
            $out = $value;
            break;
        case 'stripe':
        case 'paypal':
        case 'judopay':
            $out = get_dollar_rate() * $value;
            break;
        default:
            $out='cant determine total revenue or payable';
    }
    return $out;
}


function get_payment_mode($value){
    switch ($value){
        case 'mtn':
        case 'airtel':
        case 'tigo':
        case 'vodafone':
            $out = "mobile money";
            break;
        case 'stripe':
        case 'paypal':
        case 'hubtel_visa':
        case 'judopay':
            $out = "visa/mastercard";
            break;
        default:
            $out='cant determine';
    }
    return $out;
}

function update_donation($rst, $transaction_id) {
    global $mysqli;

    $sql     = "UPDATE donations SET transstate = $rst WHERE transid = '$transaction_id'";
    debug("All functions",$sql);
    $results = $mysqli->query($sql);
}

function update_transaction_report($rst, $transaction_id) {
    global $mysqli;

    $sql     = "UPDATE transaction_reports SET transstate = $rst WHERE transid = '$transaction_id'";
    debug("All functions",$sql);
    $results = $mysqli->query($sql);
}


function determine_currency($gateway){
    $out='';
    switch ($gateway){
        case 'mtn':
        case 'airtel':
        case 'tigo':
        case 'vodafone':
        case 'hubtel_visa':
            $out = "GHS";
            break;
        case 'stripe':
        case 'paypal':
        case 'judopay':
            $out = "USD";
            break;
        default:
            $out='cant determine';
    }
    return $out;
}

function get_other_revenue($key,$value){
    $out=0;
    switch ($key){
        case 'mtn':
        case 'airtel':
        case 'tigo':
            $out = ($value * 0.0005);
            break;
        case 'vodafone':
            $out = ($value * 0.0005);
            break;
        default:
            $out = 0;
    }
    return $out;
}

function get_platform_fee($campid,$amount){
    $out=0;
    $rate = 0.05;
    $out = $amount * $rate;
    return $out;
}

function get_transaction_processing_fee_mm($key,$value){
    $out=0;
    switch ($key){
        case 'mtn':
        case 'airtel':
        case 'tigo':
           $out = ($value * 0.0195);
           break;
        case 'vodafone':
            $out = ($value * 0.0195);
            break;
        default:
            $out = 0;
    }
    return $out;
}

function get_transaction_processing_fee_visa($key,$value){
    $out=0;
    switch ($key){
        case 'stripe':
            $out = ($value * 0.029) + 0.30;
            break;
        case 'paypal':
            $out = ($value * 0.029) + 0.30;
            break;
        case 'hubtel_visa':
            $out = ($value * 0.020);
            break;
        case 'judopay':
            $out = ($value * 0.029 + 0.29);
            break;
        default:
            $out = 0;
    }
    return $out;
}





function insert_page_visit($campid) {
	global $mysqli;
	$identity = $_SESSION['identity'];
	$ownerid  = "(SELECT ownerid FROM campaign WHERE id = $campid LIMIT 1)";
	$sql      = "INSERT INTO page_hits(campid,ownerid,hit,identity) VALUES($campid,$ownerid,1,'$identity')";
	$result   = $mysqli->query($sql);
	return $result;
}

function insert_payment_request($sql) {
    global $mysqli;
    $mysqli->query($sql);
}

function insert_sql($sql) {
    global $mysqli;
    $mysqli->query($sql);
}


function insert_stripe_response($transid, $referenceid, $status, $json_response) {
	global $mysqli;
	$sql = sprintf(
		"INSERT INTO stripe_results(transid,referenceid,status,json_response)
VALUES('%s','%s','%s','%s')",
		$mysqli->real_escape_string($transid),
		$mysqli->real_escape_string($referenceid),
		$mysqli->real_escape_string($status),
		$mysqli->real_escape_string($json_response)
	);
	$mysqli->query($sql);

}

function get_donation_report($campid, $start_date, $end_date) {
	global $mysqli;
	$sql    = "SELECT id,amount,phone,email,donator,donation_when FROM donations WHERE transstate=1 AND campid=$campid AND donation_when BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' ORDER BY id DESC";
	$result = $mysqli->query($sql);
	$output = '
<table id="donation_report" class="table table-striped table-bordered">
                      <thead>
                                    <tr>
                                        <th> Donor</th>
                                        <th> Phone</th>
                                        <th> Email </th>
                                        <th> Amount</th>
                                        <th> Date</th>
                                   </tr>
                                </thead>
                     <tbody>
    ';
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_object()) {

			$output .= "
                <tr>
                    <td>$row->donator</td>
                    <td>$row->phone</td>
                    <td>$row->email</td>
                    <td>$row->amount</td>
                    <td>$row->donation_when</td>
                </tr>
";
		}
	} else {
		$output .= '
                <tr>
                     <td colspan="5">No Data Found</td>
                </tr>
           ';
	}

	$output .= '</tbody></table>';
	return $output;

}

function process_msisdn($msisdn) {
    if (strlen($msisdn) > 10) {
        if (startsWith($msisdn, '233')) {
            return $msisdn;
        } elseif (startsWith($msisdn, '+233')) {
            return str_replace('+', '', $msisdn);
        } else {
            return null;
        }
    } else if (strlen($msisdn) == 10) {
        if (startsWith($msisdn, '0')) {
            return '233'.substr($msisdn, 1);
        } else {
            return null;
        }
    } else {
        return null;
    }
}

function set_dollar_rate($rate){
    global $mysqli;
    $sql = "INSERT INTO myaid_settings(`key`,`value`) VALUES('dollar_rate','$rate')";
    $mysqli->query($sql);
}

function update_dollar_rate($rate){
    global $mysqli;
    $sql = "UPDATE myaid_settings SET `key` = 'dollar_rate',`value`='$rate' WHERE `key`='dollar_rate'";
    $mysqli->query($sql);
}

function get_dollar_rate(){
    global $mysqli;
    $sql = "SELECT `value` FROM myaid_settings WHERE `key`='dollar_rate'";
    $result = $mysqli->query($sql);
    $v=0;
    while($row = $result->fetch_object()){
        $v = $row->value;
    }
    return $v;
}

function get_days_left($futureDate){
    $d = new DateTime($futureDate);

    $date_now = new DateTime();
    if($date_now >= $d){
        $dl = 0;
    }else{
        $dl = $d->diff(new DateTime())->format('%a');
    }
    return $dl;
}


function get_hubtel_visa($amount,$title,$current_page,$campid,$email,$isregistered,$donatorid,$name,$phone){
    $transid      = 'Maid'.date('YmdHis').rand(12345, 9999999);
    $invoice = array(
        'invoice'        => array(
            'items'         => array(
                'item_0'       => array(
                    'name'        => 'Donation',
                    'quantity'    => 1,
                    'unit_price'  => $amount,
                    'total_price' => $amount,
                    'description' => 'MyAidFund Donation To '.$title
                )
            ),

            'total_amount' => $amount,
            'description'  => 'MyAidFund Donation To '.$title

        ),
        'store'        => array(
            'name'        => 'MyAidFund',
            'tagline'     => 'donation',
            'phone'       => '+233246144043',
            'website_url' => 'https://myaidfund.com/',
            'logo_url'    => 'https://myaidfund.com/assets/img/logo.png'
        ),

        'actions'     => array(
            'cancel_url' => $current_page,
            'return_url' => $current_page
        ),
    );



    insert_donation($campid, $email, $isregistered, $donatorid, $name, $phone, $amount, 'hubtel_visa', $transid, "WEB");

    $status = send_hutel_request($invoice,$transid,$campid);


    if ($status != 'false') {
        update_donation(2, $transid);
        update_transaction_report(2,$transid);
        //send_automatic_thank_you($campid, $email);
    } else {
        update_donation(5, $transid);
        update_transaction_report(5,$transid);
    }

    return $status;
}

function send_hutel_request($invoice,$transid,$campid){
    $clientId       = 'ysoethtd';//
    $clientSecret   = 'rcwtazre';
    $basic_auth_key = 'Basic '.base64_encode($clientId.':'.$clientSecret);
    $request_url    = 'https://api.hubtel.com/v1/merchantaccount/onlinecheckout/invoice/create';
    $create_invoice = json_encode($invoice, JSON_UNESCAPED_SLASHES);

    $ch = curl_init($request_url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $create_invoice);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: '.$basic_auth_key,
        'Cache-Control: no-cache',
        'Content-Type: application/json',
    ));

    $result = curl_exec($ch);
    $error  = curl_error($ch);
    curl_close($ch);

    debug('Hubtel Visa',$result);
    if ($error) {
        return 'false';
    } else {
        // redirect customer to checkout
        $response_param = json_decode($result);
        $response_code   = $response_param->response_code;
        $redirect_url   = $response_param->response_text;
        $token   = $response_param->token;
        //header('Location: '.$redirect_url);


        if($response_code=='00'){
            insert_hubtel_visa_response($token,$transid);
            return $redirect_url;

        }else{
            return 'false';
        }
    }
}

function insert_hubtel_visa_response($token,$trans_id){
    global $mysqli;
    $sql = "INSERT INTO hubtel_tokens(token, transactionid) 
            VALUES ('$token','$trans_id')";
    debug("INSERT HUBTEL VISA RESPONSE",$sql);
    $mysqli->query($sql);
}

function insert_judopay_logs($reference,$trans_id,$pay_ref){
    global $mysqli;
    $sql = "INSERT INTO judopay_logs(reference, transid, payref) 
            VALUES ('$reference','$trans_id','$pay_ref')";
    debug("INSERT JUDOPAY VISA RESPONSE",$sql);
    $mysqli->query($sql);
}

function get_donator_email_by_transid($transid) {
    global $mysqli;
    $sql     = "SELECT email FROM donations WHERE transid = '$transid'";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->email;
    }
    return $rst;
}

function get_donator_campid_by_transid($transid) {
    global $mysqli;
    $sql     = "SELECT campid FROM donations WHERE transid = '$transid'";
    $results = $mysqli->query($sql);
    $rst = '';
    while ($row=$results->fetch_object()){
        $rst = $row->campid;
    }
    return $rst;
}

function send_automatic_thank_you($id, $to) {
	global $mysqli;
	$results = $mysqli->query("SELECT * FROM campaign where id = $id");

	$beneficiary = '';
	$message     = '';
	$title       = '';
	$has_message = 0;

	while ($row = $results->fetch_object()) {
		$beneficiary = $row->beneficiary;
		$message     = $row->message;
		$title       = $row->title;
		$has_message = $row->has_message;
	}

	if ($has_message == 1) {
		$msg     = get_auto_message($beneficiary, $message, $id);
		$subject = "A Thank You Note From ".$beneficiary;
	} else {
		$beneficiary = "MyAidFund";
		$message     = "Thank you very much for your recent donation on $title,
        which we received today. Your generosity will make an immediate difference in the lives of others. The funds raised will go toward $title. You are making a difference!
        \n\nThanks again for your kindness,";
		$msg     = get_default_message($beneficiary, $message, $id);
		$subject = "A Thank You Note From MyAidFund";
	}

	send_mail($to, $subject, $msg);

}

function send_donation_failed_thank_you($id, $to) {
    global $mysqli;
    $results = $mysqli->query("SELECT * FROM campaign where id = $id");

    $beneficiary = '';
    $message     = '';
    $title       = '';
    $has_message = 0;

    while ($row = $results->fetch_object()) {
        $beneficiary = $row->beneficiary;

        $message     = "Thank you very much for your intention to donate $title,
        which did not go through successfully today. We would be much grateful if you can complete your donation by trying again. Your generosity will make an immediate difference in the lives of others. The funds raised will go toward $title. You are making a difference! Please Share this campaign on social media by clicking on the social media button below
        \n\nThanks again for your kindness.";

        $title       = $row->title;
        $has_message = $row->has_message;
    }

    if ($has_message == 1) {
        $msg     = get_default_donation_failed_message($beneficiary, $message, $id);
        $subject = "A Thank You Note From ".$beneficiary;
    } else {
        $beneficiary = "MyAidFund";
        $message     = "Thank you very much for your intention to donate $title,
        which did not go through successfully today. We would be much grateful if you can complete your donation by trying again. Your generosity will make an immediate difference in the lives of others. The funds raised will go toward $title. You are making a difference! Please Share this campaign on social media by clicking on the social media button below
        \n\nThanks again for your kindness.";
        $msg     = get_default_donation_failed_message($beneficiary, $message, $id);
        $subject = "A Thank You Note From MyAidFund";
    }

    send_mail($to, $subject, $msg);

}

function send_mail($to, $subject, $message) {

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0"."\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8"."\r\n";

	// More headers
	$headers .= 'From: info@myaidfund.com'."\r\n";
	//$headers .= 'Cc: myboss@example.com' . "\r\n";

	mail($to, $subject, $message, $headers);

}

function write_logs($log) {
	$File   = "payment.log";
	$Handle = fopen($File, 'a');
	$Data   = "$log\n";
	fwrite($Handle, $Data);
	fclose($Handle);
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack)-strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function contains($needle, $haystack) {
    return strpos($haystack, $needle) !== false;
}

function is_positive_integer($str) {
    return (is_numeric($str) && $str > 0 && $str == round($str));
}

function check_captcha($code) {
    $curl = curl_init();
    $data = array("secret" => "6Ld5qUsUAAAAAHRx0dHpUocPfifOYIxOOAi-eSm3",
        "response"            => "$code");
    curl_setopt_array($curl, array(
        CURLOPT_URL            => "https://www.google.com/recaptcha/api/siteverify",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_POSTFIELDS     => $data,
        CURLOPT_CUSTOMREQUEST  => "POST",
        CURLOPT_HTTPHEADER     => array(
            "cache-control: no-cache",
            "postman-token: c03ad5d9-6346-ddb1-9eff-ff14ced1e26b",
        ),
    ));

    $response = curl_exec($curl);
    return $response;

}

function debug($page, $data) {

    $item = array(
        "Log"  => $data,
        "Page" => $page,
        "time" => date('Y-m-d H:i:s')
    );
    $myFile = "../debug.log";
    $stringData = json_encode($item);
    file_put_contents(
        $myFile,
        $stringData.","."\n",
        FILE_APPEND
    );
}
function is_email_valid($address){
    if (!filter_var($address, FILTER_VALIDATE_EMAIL)){
        return false;
    }else{
        return true;
    }
}


function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}
