<?php
$page = 'Privacy Policy';
include_once ('header.php');
include ('includes/allfunctions.php');

?>

    <style>
        .container{
            width:95%;
        }
    </style>
    <!-- basic-slider start -->
    <!--<div class="slider-section">
        <div class="slider-active owl-carousel">
            <div class="single-slider slider-screen nrbop bg-black-alfa-40" style="background-image: url(assets/img/slides/team.jpg);">
                <div class="container">
                    <div class="slider-content text-white">
                        <h2 class="b_faddown1 cd-headline clip is-full-width" >Empowering Social Funding</h2>
                        <p class="b_faddown2">Myaidfund.com is the number 1 crowdfunding platform in Ghana and beyond.
                        <div class="slider_button b_faddown3"><a href="#our_mission">Our Mission</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>-->
    <!-- basic-slider end -->

    <!-- Features -->


    <!-- Special Cuase Paralax -->


    <!-- Causes -->



    <!-- team -->
    <div class="team-wrapper" style="padding-top: 0px" id="our_mission">
        <div class="container">
            <div class="section-name one">
                <h2>Privacy Policy</h2>

                <div class="" style="text-align: left; padding: 10px 30px 30px 30px">
<!--                    <p><strong>PRIVACY POLICY</strong></p>-->

                    <p>Last Updated: <strong>2<sup>nd</sup> March, 2018</strong></p>
                    <p>Apptechhub Global Limited (referred to throughout as &ldquo;<strong>Apptechhub</strong>,&rdquo; &ldquo;<strong>us</strong>,&rdquo; &ldquo;<strong>we</strong>,&rdquo; &ldquo;<strong>our</strong>&rdquo;), is the owner and operator of the www.Myaidfund.com website, a fundraising platform. This Privacy Policy applies to the&nbsp;<a href="https://myaidfund.com/">www.Myaidfund.com</a>&nbsp;website, any subdomains thereof, any API integrations or widgets we offer, any other websites or webpages we own or operate that include a link to this policy, and any of our mobile applications (all of which together are referred to as the &ldquo;<strong>Sites</strong>&rdquo;). Please keep in mind that this Privacy Policy does not apply to other websites, which may be accessible from the Sites. External websites may have data collection, storage, and use practices and policies that differ materially from those contained here.</p>
                    <p>We may update this Privacy Policy from time to time. If we do so, we will notify you by posting the date of the last change or amendment at the top of this page. You agree that this method of notice is sufficient and that you will regularly check this Privacy Policy to see if updates or changes have been made. By continuing to use the Sites, you consent to the revised Privacy Policy.</p>
                    <p><strong>What Types of Information Do We Collect and How?</strong></p>
                    <p>In order for you to make donations through the Sites, register with the Sites, or take certain other actions on the Sites, we may require you to provide us with certain information that identify you (&ldquo;<strong>Personal Information</strong>&rdquo;). Personal Information includes but is not limited to the following:</p>
                    <ul>
                        <li>Contact Data such as name, mailing address, e-mail address, and Myaidfund user-name, account number and password</li>
                        <li>Financial Data such as your account or credit card number</li>
                        <li>Demographic Data such as your zip code, age and gender</li>
                        <li>Company Data such as your business name, size and business type</li>
                        <li>Activity Data such as your donation history, fundraising history, and events attended.</li>
                        <li>Social Media information such as friend lists, likes and photos.</li>
                        <li>Third-Party Data such as numbers of your contacts</li>
                    </ul>
                    <p><strong>How do we collect Personal Information?</strong></p>
                    <p>We collect Personal Information you provide to us when you create an account, make a donation, create a campaign or take other actions on the Sites.</p>
                    <p>We also collect Personal Information from social media sites and services when you use your social media account to log into the Sites or use the Sites to take other actions on social media, such as sharing or liking a fundraising campaign.</p>
                    <p>If you access the Sites through a mobile device, you agree that we may communicate with you regarding Myaidfund by SMS, MMS, text message, or other electronic means to your mobile device. In the event you change or deactivate your mobile telephone number, you agree to promptly update your Myaidfund account information to ensure that your messages are not misdirected. Please note that your wireless service carrier&rsquo;s standard charges, data rates, and other fees may apply where you access the Sites through a mobile device. In addition, downloading, installing, or using certain Sites on a mobile device may be prohibited or restricted by your carrier, and not all Sites may work with all carriers or devices.</p>
                    <p><strong>Traffic Data</strong>.</p>
                    <p>As is true of most websites, we automatically collect certain information when you visit our Sites. This information includes: (i) IP addresses, (ii) domain servers, (iii) types of computers accessing the Sites, (iv) types of web browsers used to access the Sites, (v) referring/exit pages, (vi) data about usage patterns throughout the Sites (e.g. click rates on different links etc.). Information of this type (&ldquo;<strong>Traffic Data</strong>&rdquo;) is anonymous information that does not personally identify you but is helpful for us to improve the business performance and user experience of the Sites.</p>
                    <p><strong>How do we collect Traffic Data?</strong></p>
                    <p>As is true of most websites, we use cookies (a small text file placed on your computer or device to identify your computer and web browser) to collect Traffic Data related to the Sites. We use Service Providers (defined below) to place cookies on your computer or device to compile this non-personally identifiable information so we can aggregate statistical information about usage of the Sites. Most web browsers are initially set up to accept cookies. You can reset your web browser to refuse all cookies or to indicate when a cookie is being sent, however, certain features of the Site or Services may not work if you delete or disable cookies.</p>
                    <p>The Sites also contain web beacons, which are electronic images that are used along with cookies to compile statistics so we can analyze how the Sites are being used. Our e-mails may also contain web beacons so we can track how many people open the message or click on links within the message. This information helps us improve our communication and marketing efforts. We use a Service Provider to gather information on how you and others are using the Sites. By using this service we are able (for example) to see how many people visited a given page or clicked on a given link. This information helps us optimize the performance of the Sites.</p>
                    <p><strong>Is my Personal Information kept confidential?</strong></p>
                    <p>Except as otherwise provided in this Privacy Policy or agreed by User, we will keep your Personal Information private and will not share it with other third parties unless under the following circumstances:</p>
                    <p>(i) to comply with a court order or other legal process, (ii) to protect our rights or property, (iii) in connection with our sale, merger, consolidation, change of control, or transfer of substantial assets, (iv) to enforce our Terms of Service (v)with our vendors, consultants and other such persons to perform certain business-related functions on Our behalf.</p>
                    <p>Please keep in mind that while we take reasonable precautions to safeguard your Personal Information no amount of protection can guarantee its security.</p>
                    <p><strong>Who has access to my Personal Information?</strong></p>
                    <p>We share your Personal Information with other business partners who assist us in performing core services related to the operation of the Sites (&ldquo;<strong>Service Providers</strong>&rdquo;). These Service Providers only use your Personal Information to perform these core services, which are necessary for the orderly operation of the Sites, such as hosting and maintenance, data storage and management, security, payment processing, and marketing and promotions. Each Service Provider must agree to use reasonable security procedures and practices, appropriate to the nature of the information involved, in order to protect your Personal Information from unauthorized access, use, or disclosure.</p>
                    <p>Unless you donate anonymously, any organization you make a contribution to, or purchase from, through the Sites will have access to your Personal Information (except for your credit card number or other payment account information). If you make a donation or purchase through an individual fundraising page, then the person who created the fundraising page will also have access to your Personal Information (except for your credit card number or other payment account information). If you make a donation or purchase through an individual fundraising page associated with a fundraising team, or directly to a team fundraising page, then the person who created the team page will also have access to your Personal Information (again, except for your credit card number or other payment account information). You consent to the foregoing and agree that we are not responsible for how these organizations or persons handle your Personal Information. You should contact them directly for their privacy policies and data usage practices. From time to time, businesses or other entities will run campaigns to benefit charities through the Sites. In such a situation, your Personal Information (except for credit card number or other payment account information) may be accessible to the business or other entity running the campaign. If that is the case, we will provide additional notice to you during checkout to let you know if the business or other entity will have access to your data.</p>
                    <p>Please remember that after we disclose Personal Information to third parties, we can no longer control the use of such Personal Information. Accordingly, Myaidfund.com will not be liable for the acts or omissions of any third party.</p>
                    <p><strong>How does Myaidfund.com use my Personal Information?</strong></p>
                    <p>We use your information in a variety of ways to help us run the Sites, provide services to you, and communicate with you. Here are some examples of the ways we use your information:</p>
                    <ul>
                        <li>To facilitate the processing of a transaction between you and a third party</li>
                        <li>To send you receipts</li>
                        <li>To send you updates about the Sites</li>
                        <li>To send you updates about activity on the Sites related to you</li>
                        <li>To administer your account</li>
                        <li>To respond to customer service inquiries</li>
                        <li>To send marketing materials</li>
                        <li>To improve our Sites and marketing efforts</li>
                    </ul>
                    <p><strong>What kinds of advertising and online tracking do we use?</strong></p>
                    <p>We may allow third-party companies to serve advertisements and collect certain anonymous information when you visit the Sites. These companies may use non-personally identifiable information (e.g., click stream information, web browser type, time and date, subject of advertisements clicked or scrolled over) during your visits to the Sites and other websites in order to provide advertisements about goods and services likely to be of interest to you. These companies typically use a cookie or third-party web beacon to collect this information. Our systems do not recognize browser &ldquo;Do Not Track&rdquo; signals, but several of our Service Providers who utilize these cookies or web beacons on our Sites enable you to opt out of targeted advertising practices. To learn more about these advertising practices or to opt out of this type of advertising, you can visit&nbsp;<a href="http://www.networkadvertising.org/">www.networkadvertising.org</a>&nbsp;or&nbsp;<a href="http://www.aboutads.info/choices/">www.aboutads.info/choices/</a>. We also provide you with additional tools to opt out of marketing from us or certain transfers of your information. You can learn about this in the &ldquo;What choices do I have?&rdquo; section of this Privacy Policy.</p>
                    <p><strong>Who has access to my payment information?</strong></p>
                    <p>Myaidfund.com contracts with Service Providers to process all donations made through the Sites. Only the third party processors have access to your credit card number or other payment account information. We do not have access to your credit card number or other payment account information, nor do the organizations to whom you donate, or the people who create fundraising pages. Data handled by the third party processors is subject to their terms and privacy policies. Please contact us at the contact information below for any additional questions about your payment information and how payments are processed.</p>
                    <p><strong>What other information is public?</strong></p>
                    <p>Information you post on the Sites, event pages, campaign pages, personal fundraising pages, your profile (unless hidden), a charity profile, blog posts, or other public pages on the Sites can be accessed by other people. You should exercise caution when deciding to share information on public pages. We cannot control who accesses shared information or how other parties will use that information.</p>
                    <p>If you use Myaidfund.com support to create an event or campaign on the Sites that is &ldquo;private&rdquo;, this means that the event or campaign link will not be displayed through the activity feeds on the Sites, the event or campaign will not be available through the internal search function on the Sites, and it will not be indexed by external search engines like Google. It does not mean, however, that people cannot still wind up on your page (e.g. if the link is shared with them, by you or a person with whom you have shared the link). Please keep in mind that, even if you make a campaign or event private, any personal fundraising pages created by your supporters as part of that campaign or event will remain publicly accessible.</p>
                    <p><strong>What are API&rsquo;s and how can my information be shared?</strong></p>
                    <p>We make an application programming interface (an &ldquo;<strong>API</strong>&rdquo;) available to Clients (defined in the&nbsp;<a href="https://myaidfund.com/terms_of_use.php">Terms of Use</a>). An API (in non-technical terms) allows a Client to automatically retrieve information from our Sites, for use/display elsewhere (e.g. on the fundraising nonprofit&rsquo;s website). To give an example, a nonprofit might use our API to retrieve the names and photos of the top 5 fundraisers for a campaign it is running; the nonprofit could then display that information on a leader board on its own website. If you are fundraising for a nonprofit, your name, picture, amount raised, and goal, can be retrieved by the nonprofit through our API. If you create a team fundraising page, then your name, the team goal, and the amount raised by the team can also be retrieved by the nonprofit through our API. If you host an event for a charity, then your name, contact information, and the location of your event can be retrieved by the benefitting nonprofit through our API. The foregoing examples are for illustrative purposes only. The nonprofit you engage with has access to other information through our API such as tickets sold, number of attendees, etc. That being said, the information that we make available through our API is (generally) information that is otherwise available on the Sites (e.g. already posted on an event page, campaign page, available through search results, etc.).</p>
                    <p><strong>What choices do I have?</strong></p>
                    <p>It&rsquo;s up to you whether or not you want to provide us with Personal Information. You can still visit the Sites without providing us with Personal information, but you will be unable to take certain actions without doing so.</p>
                    <p>To opt out of receiving marketing emails from us, you can do so by clicking on &ldquo;Account Settings&rdquo; which is accessible at the top of your screen once you&rsquo;ve logged in or by following the unsubscribe instructions at the bottom of any marketing email we send to you.</p>
                    <p>How can I update or correct my Personal Information?</p>
                    <p>If you have registered (and therefore have an individual profile) you can update your information by clicking on &ldquo;Account Settings&rdquo; which is accessible at the top of your screen once you&rsquo;ve logged in.</p>
                    <p><strong>Security</strong></p>
                    <p>We maintain physical, electronic, and procedural safeguards to protect the confidentiality and security of information transmitted to us. However, no data transmission over the Internet or other network can be guaranteed to be 100% secure or error free. As a result, while we strive to protect information transmitted on or through the Sites, we cannot and do not guarantee the security of any information you transmit on or through the Sites, and you do so at your own risk.</p>
                    <p><strong>Links To Other Websites</strong></p>
                    <p>This Privacy Policy applies only to our Sites. Our Sites may contain links to or from other websites not operated or controlled by us or allow others to send you such links. A link to or framing of a third party&rsquo;s website does not mean that we endorse it or that we are affiliated with it. We do not exercise control over third-party websites. You access such third-party websites or content at your own risk. You should always read the privacy policy of a third-party website before providing any information to the website.</p>
                    <p><strong>Unsolicited Information</strong></p>
                    <p>Please be advised that some information you provide may be publicly accessible, such as information posted in forums or comment sections. We may also collect information through customer support communications, your communication to us of ideas for new products or modifications to existing products, and other unsolicited submissions (collectively, with publicly-accessible information, "Unsolicited Information"). By sending us Unsolicited Information, you further (a) agree that we are under no obligation of confidentiality, express or implied, with respect to the Unsolicited Information, (b) acknowledge that we may have something similar to the Unsolicited Information already under consideration or in development, (c) grant us an irrevocable, non-exclusive, royalty-free, perpetual, worldwide license to use, modify, prepare derivative works, publish, distribute and sublicense the Unsolicited Information, and (d) irrevocably waive, and cause to be waived, against Myaidfund and its users any claims and assertions of any moral rights contained in such Unsolicited Information. This Unsolicited Information section shall survive any termination of your account or the Services. This Privacy Policy otherwise does not apply to any information collected by Myfundaid other than information collected through the Sites.</p>
                    <p><strong>Children&rsquo;s Privacy</strong></p>
                    <p>We understand the importance of protecting privacy especially where children are involved, and we are committed to doing so. Our Sites are for a general audience and are not targeted towards children. In order to protect the privacy of children, our Sites do not knowingly collect personal information from children under the age of 13 without prior parental consent. If your child has used the Sites and submitted Personal Information to us, please contact us and we will endeavor to remove that information from the Sites and our database.</p>
                    <p><strong>Processing in the Ghana</strong></p>
                    <p>Please be aware that your Personal Information, communications, and other information may be transferred to and maintained on servers or databases located outside your state, province, or country. If you are located outside of Ghana, please be advised that we process and store all information in Ghana. By using the Sites, you agree that the collection, use, transfer, and disclosure of your Personal Information and communications will be governed by the applicable laws in the Ghana.</p>
                    <p><strong>Contact Information</strong></p>
                    <p><a href="mailto:support@myaidfund.com">support@Myaidfund.com</a></p>

                </div>
            </div>


        </div>
    </div>





    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>