<?php
$page = 'Contact Us';
include_once ('header.php');
?>
<!-- Banner -->
<!-- 	<div class="page-banner">
		<div class="container">
			<div class="parallax-mask"></div>
			<div class="section-name">
				<h2>Contact Us</h2>
				<div class="short-text">
					<h5>Home<i class="fa fa-angle-double-right"></i>Contact Us</h5>
				</div>
			</div>
		</div>
	</div> -->

<div class="contact-map-area">

		<div class="map-area" >

			<div id="map2"></div>

		</div>
	</div>
<div style="min-height:10px; background-color:black;"></div>
	<div class="contact-map-area">

		<div class="map-area" >

			<div id="map"></div>

		</div>
	</div>

	<!-- contact wrapper -->
	<div class="contact-page-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-sm-4 widget">
							<h4>Adress</h4>
							<i class="fa fa-map-marker"></i>
							<p>114 kwame nkrumah Ave. Adabraka - Accra, Ghana</p><br/>
							<p>67 South Muwn Ave apt 89, East Orange , NJ 07018,USA</p>
						</div>
						<div class="col-sm-4 widget">
							<h4>Phone</h4>
							<i class="fa fa-phone"></i>
							<p>+1(201)673-6337</p>
							<p>+233 24 653 1362</p>
						</div>
						<div class="col-sm-4 widget">
							<h4>E-mail</h4>
							<i class="fa fa-envelope"></i>
							<p>info@myaidfund.com</p>
							<p>support@myaidfund.com</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="comment-form-wrapper contact-from clearfix">
						<div class="widget-title ">
							<h4>Say Hello to Us</h4>
							<p>Leave a message and we will contact you as soon as possible</p>
						</div>
						<form class="comment-form row altered" id="contact_us" method="post" action="process/save_contact_us.php">
							<div class="field col-sm-4">
								<h4>Name</h4>
								<input type="text" name="fname">
							</div>
							<div class="field col-sm-4">
								<h4>E-mail</h4>
								<input type="email" name="email">
							</div>
							<div class="field col-sm-4">
								<h4>Subject</h4>
								<input type="text" name="subject">
							</div>
							<div class="field col-sm-12">
								<h4>Message</h4>
								<textarea name="message"></textarea>
							</div>
							<div class="field col-sm-4">
								<button class="btn btn-big btn-solid"><i class="fa fa-paper-plane"></i><span>Send Message</span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Foter -->
<?php
include ('footer.php');
?>