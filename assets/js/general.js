var dlogin_html='';
var c_url = 'index.php';
var fbl = false;
var gll = false;

function login_form($url){
    c_url = $url;
    var d = $('#d_login_form').html();
    if(!d){

    }else{
        dlogin_html = d;
        $('#d_login_form').html('');
    }
    // console.log(dlogin_html);
    swal({
        title: null,
        type: null,
        html:dlogin_html,
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false,
        focusConfirm: false,
        confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> Great!',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:
            '<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'Thumbs down'
    })
}

$(document).ready(function(){
    //login_form();
   // HandleGoogleApiLibrary();

});



function fb_login(){
    fbl = true;
    FB.login(statusChangeCallback, {scope: 'email,public_profile', return_scopes: true});
}


var getInfo;
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        isLogin();
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        // document.getElementById('status').innerHTML = 'Please log ' +
        //     'into Facebook.';
    }
}
// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}
window.fbAsyncInit = function() {
    FB.init({
        appId      : '1997854547143864',
        cookie     : true,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.5' // use graph api version 2.5
    });
    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
};
// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function isLogin() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me','GET', {fields: 'name,email,id,picture.width(150).height(150)'}, function(response) {
        var loginData = "opera=fb_login&name="+response.name+"&email="+response.email+"&fb_Id="+response.id+"&profilePictureUrl="+response.picture.data.url;
        console.log('Successful login for: ' + loginData);

        //ajax reqest to server..
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "process/process_users.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                var rst = xmlhttp.responseText;
                console.log(rst);
                if(rst === 'true'){
                    swal(
                        'Login Status',
                        'Login Successfull, You will be redirected soon',
                        'success'
                    );
                    if(fbl){
                        window.location.href = c_url;
                    }else{
                        location.reload();
                    }

                }
                fbLogoutUser();
                //document.getElementById('response').innerHTML = xmlhttp.responseText;
            }

        }
        xmlhttp.send(loginData);
        //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name +"<br> Email : "+ response.email +"<br> Profile Id :  "+ response.id +"<br> Profile Url : "+ response.picture.data.url +'!';
    });
}

function fbLogoutUser() {
    FB.getLoginStatus(function(response) {
        if (response && response.status === 'connected') {
            FB.logout(function(response) {
                document.location.reload();
            });
        }
    });
}

function submitlogin() {
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            'z-index': '999999'
        } });

    var uname = $('#xxxname').val();
    var pass = $('#xxxpass').val();
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'process/process_users.php?opera=d_login&xxname=' + uname + '&xxpass=' + pass,
        contentType: false,
        processData: false
    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.unblockUI();
        console.log(xhr);

        if (xhr === 'true') {
            location.href = c_url;
        } else {
            document.getElementById('status').innerHTML = 'Invalid login credentials';
        }
    });
    ajaxRequest.error(function () {
        $.unblockUI();
    });
}


function register_user() {
    $.blockUI({
        message:'<div class="loader" style="background-color:#ffffff00"></div>',
        css:{backgroundColor:'#ffffff00',border: 'none'},
        baseZ: '999999'
    });

    var fname = $('#dfullname').val();
    var xxname = $('#rxxxname').val();
    var xxpass = $('#rxxxpass').val();
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'process/process_users.php?opera=d_register&xxname=' + xxname + '&xxpass=' + xxpass + '&fname='+fname,
        contentType: false,
        processData: false
    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.unblockUI();
        console.log(xhr);

        if (xhr === 'true') {
            location.href = c_url;
        } else {
            document.getElementById('status').innerHTML = 'Invalid Registration Details';
        }

    });
    ajaxRequest.error(function () {
        $.unblockUI();
    });
}




function HandleGoogleApiLibrary() {
    // Load "client" & "auth2" libraries
    console.log('Google has been initialized');
    gapi.load('client:auth2',  {
        callback: function() {
            // Initialize client & auth libraries
            gapi.client.init({
                apiKey: 'viK_6DJO-0_ZDeT2B42e-AME',
                clientId: '854778400919-0fb9rj06t4o1r8q08nbjpghs7un67igc.apps.googleusercontent.com',
                scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
            }).then(
                function(success) {
                    // Libraries are initialized successfully
                    // You can now make API calls
                },
                function(error) {
                    // Error occurred
                    console.log(error)
                }
            );
        },
        onerror: function() {
            // Failed to load libraries
        }
    });
}

function gl_login() {
    // API call for Google login
/*    gapi.auth2.getAuthInstance().signIn().then(
        function(success) {
            // API call is successful

            var user_info = success.body;

            // user profile information
            console.log(user_info);
        },
        function(error) {
            // Error occurred
             console.log(error);
        }
    );*/
    $('.abcRioButtonContentWrapper').click();
}

function gl_logout() {
    gapi.auth.signOut();
}

function onSignIn(googleUser) {
    gll = true;
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

    var loginData = "opera=gl_login&name="+profile.getName()+"&email="+profile.getEmail()+"&gl_Id="+profile.getId()+"&profilePictureUrl="+profile.getImageUrl();
    console.log('Successful login for: ' + loginData);

    //ajax reqest to server..
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "process/process_users.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            var rst = xmlhttp.responseText;
            glSignOut();
            console.log(rst);
            if(rst === 'true'){
                swal(
                    'Login Status',
                    'Login Successfull, You will be redirected soon',
                    'success'
                );
                if(fbl){
                    window.location.href = c_url;
                }else{
                    location.reload();
                }

            }

            //document.getElementById('response').innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.send(loginData);
}

function glSignOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}