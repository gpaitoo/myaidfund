$(document).ready(function(){
    $(".orgdiv").addClass('hide');
    $("#org").removeAttr("required");

	$('#regform').submit(function(e){
		e.preventDefault();
		$('#err').html('');

		var pass = $('#xxxpass').val();
		var repass = $('#confirmxxxpass').val();
		if(pass != repass){
			alert('password missmatch');
			return;
		}

		$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 

		var url = "process/registration.php"; // the script where you handle the form input.

    	$.ajax({
           type: "POST",
           url: url,
           data: $("#regform").serialize(), // serializes the form's elements.
           success: function(data)
           {
           	$.unblockUI();
           		if (data.indexOf("err") >= 0){
           			$('#err').html(data.replace('err',''));
           		}else if(data == 'true'){
           			var email = $('#email').val();
           			window.location.href = 'reg_next.php?em='+email;
           		}
               
           }
         });

	});
});


$('#account_type').on("change",function(){
    var val = $('#account_type').val();
    console.log(val);
    if(val=='Personal'){
        $(".orgdiv").addClass('hide');
        $("#org").removeAttr("required");

	}else{
        $(".orgdiv").removeClass('hide');
        $("#org").attr("required","");
	}

});