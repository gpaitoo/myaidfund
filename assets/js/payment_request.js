var bankwire = '<div class="form-group">\n' +
    '                        <label style="color: blue;font-size: medium;">Account Holder Name</label>\n' +
    '                        <input class="form-control"  type="text" size="14" name="accholder" required="required"/>\n' +
    '                    </div>\n' +
    '\n' +
    '                    <div class="form-group">\n' +
    '                        <label id="mcurrency" style="color: blue;font-size: medium;" class="title">Swift Code<span style="color:red;" >*</span></label>\n' +
    '                        <input class="form-control" style="width:120px" type="text" name="swift" required="required"/>\n' +
    '                    </div>\n' +
    '\n' +
    '\n' +
    '                    <div class="form-group">\n' +
    '                        <label style="color: blue;font-size: medium;" >Bank Name<span style="color:red;" >*</span></label>\n' +
    '                        <input class="form-control" name="bankname" required="required" />\n' +
    '\n' +
    '                    </div>\n' +
    '\n' +
    '                    <div class="form-group">\n' +
    '                        <label style="color: blue;font-size: medium;" >IBAN<span style="color:red;" >*</span></label>\n' +
    '                        <input class="form-control" name="iban" required="required" />\n' +
    '                    </div>';

var gbank = '                    <div class="form-group">\n' +
    '                        <label style="color: blue;font-size: medium;">Bank Name</label>\n' +
    '                        <input class="form-control"  type="text" size="14" name="bankname" required="required"/>\n' +
    '                    </div>\n' +
    '\n' +
    '                    <div class="form-group">\n' +
    '                        <label id="mcurrency" style="color: blue;font-size: medium;" class="title">Account Holder Name<span style="color:red;" >*</span></label>\n' +
    '                        <input class="form-control" style="width:120px" type="text" name="accholder" required="required"/>\n' +
    '                    </div>\n' +
    '\n' +
    '\n' +
    '                    <div class="form-group">\n' +
    '                        <label style="color: blue;font-size: medium;" >Account Number<span style="color:red;" >*</span></label>\n' +
    '                        <input class="form-control" name="accnumber" required="required" />\n' +
    '\n' +
    '                    </div>\n' +
    '\n' +
    '                    <div class="form-group">\n' +
    '                        <label style="color: blue;font-size: medium;" >Branch<span style="color:red;" >*</span></label>\n' +
    '                        <input class="form-control" name="branch" required="required" />\n' +
    '                    </div>';

$(document).ready(function () {
    $('#bank_d').html(bankwire);
});

$('#mcountry').on("change",function(){
    var val = $('#mcountry').val();
    console.log(val);

    if(val=="Ghana"){
        $('#bank_d').html(gbank);
    }else{
        $('#bank_d').html(bankwire);
    }
});

$('#pay_req').ajaxForm({
    beforeSend: function() {
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });

    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        console.log(percentVal);
        $('.blockUI h1').html(percentVal);
    },
    success: function() {

    },
    complete: function(xhr) {
        $.unblockUI();
        var rst = xhr['responseText'];

        console.log(rst);
        if(rst=='true') {

            //$('#sms_content').val('');
            //$('#sms_title').val('');
            $('#campaign_news')[0].reset();
            toastr.success("Request Has Been Submitted successfully", "Status");

        }else {
            toastr.error("An error occured. Please try again", "Status");
        }

    }
});