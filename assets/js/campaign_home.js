var meditor;
tinymce.init({
    selector:'.story',
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | imageupload",
    height : 300,
    setup: function(editor) {
        var inp = $('<input id="tinymce-uploader" type="file" name="pic" accept="image/!*" style="display:none">');
        $(editor.getElement()).parent().append(inp);

        inp.on("change",function(){
            var input = inp.get(0);
            var file = input.files[0];
            var fr = new FileReader();
            fr.onload = function() {
                var img = new Image();
                img.src = fr.result;
                editor.insertContent('<img src="'+img.src+'" style="width:100%"/>');
                inp.val('');
            }
            fr.readAsDataURL(file);
        });

        editor.addButton( 'imageupload', {
            text:"IMAGE",
            icon: false,
            onclick: function(e) {
                inp.trigger('click');
            }
        });

                editor.on('change', function (e) {
                    $('.mstory').val(editor.getContent());
                });


        meditor = editor;
    }
});

$(document).ready(function () {
    var campid = $('#campid').val();

    $('.camp_change').on("change",function(){
        var val = $('.camp_change').val();
        console.log(val);
        //Document.localStorage.setItem("campid",val);
        location.href = 'campaign_home.php?crec='+val;
    });

    $('.rpt_change').on("change",function(){
        var val = $('.rpt_change').val();
        console.log(val);
        $('#myChart').html('');
        if(val=='page'){
            getPageGraph(campid);
        }else if(val=='donation'){
            getDonationGraph(campid);
        }

    });


    getDonationGraph(campid);


    $('.btn-facebook').on('click',function(){
        var cid = $(this).attr('campid');
        FB.ui({
            method: 'share',
            href: 'http://myaidfund.com/campaign_details.php?donate_id='+cid
        }, function(response){});
    });


});


function getDonationGraph(campid){
    $.ajax({ url: 'process/get_data.php',
        data: {campid: campid,switch:'donation'},
        type: 'post',
        success: function(output) {
            var v = JSON.parse(output);

            console.log(v);

            var labels = [], data=[];
            v.forEach(function(packet) {
                labels.push(packet.date);
                data.push(packet.donations);
            });

            setDonationChart(labels,data);
        }
    });
}

function getPageGraph(campid){
    $.ajax({ url: 'process/get_data.php',
        data: {campid: campid,switch:'page'},
        type: 'post',
        success: function(output) {
            var v = JSON.parse(output);

            console.log(v);

            var labels = [], data=[];
            v.forEach(function(packet) {
                labels.push(packet.date);
                data.push(packet.hits);
            });

            setPageChart(labels,data);
        }
    });
}

function setDonationChart(day,download){
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: day,
            datasets: [{
                label: 'Donations',
                data: download,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}


function setPageChart(day,hits){
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: day,
            datasets: [{
                label: 'Page View',
                data: hits,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}



$('#campaign_news').ajaxForm({
    beforeSend: function() {
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });

    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        console.log(percentVal);
        $('.blockUI h1').html(percentVal);
    },
    success: function() {

    },
    complete: function(xhr) {
        $.unblockUI();
        var rst = xhr['responseText'];

        console.log(rst);
        if(rst=='true') {

            //$('#sms_content').val('');
            //$('#sms_title').val('');
            $('#campaign_news')[0].reset();
            toastr.success("Campaign update has been saved successfully", "Status");

        }else {
            toastr.error("An error occured. Please try again", "Status");
        }

    }
});


$('#thank_you').ajaxForm({
    beforeSend: function() {
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } });

    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        console.log(percentVal);
        $('.blockUI h1').html(percentVal);
    },
    success: function() {

    },
    complete: function(xhr) {
        $.unblockUI();
        var rst = xhr['responseText'];

        console.log(rst);
        if(rst=='true') {

            //$('#sms_content').val('');
            //$('#sms_title').val('');
            $('#thank_you')[0].reset();
            toastr.success("Campaign update has been saved successfully", "Status");

        }else {
            toastr.error("An error occured. Please try again", "Status");
        }

    }
});