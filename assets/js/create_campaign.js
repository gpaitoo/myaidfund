var meditor;
tinymce.init({
    selector:'.story',
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | imageupload",
    height : 300,
    setup: function(editor) {
        var inp = $('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
        $(editor.getElement()).parent().append(inp);

        inp.on("change",function(){
            var input = inp.get(0);
            var file = input.files[0];
            var fr = new FileReader();
            fr.onload = function() {
                var img = new Image();
                img.src = fr.result;
                editor.insertContent('<img src="'+img.src+'" style="width:100%"/>');
                inp.val('');
            }
            fr.readAsDataURL(file);
        });

        editor.addButton( 'imageupload', {
            text:"IMAGE",
            icon: false,
            onclick: function(e) {
                inp.trigger('click');
            }
        });

        editor.on('change', function (e) {
            $('.mstory').val(editor.getContent());
        });
        meditor = editor;
    }
});

$('#add_campaign').ajaxForm({
    beforeSend: function() {
    $.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 
        
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        console.log(percentVal);
        $('.blockUI h1').html(percentVal);
    },
    success: function() {
        
    },
    complete: function(xhr) {
        $.unblockUI();
        var rst = xhr['responseText'];
        
        console.log(rst);
        if(rst=='true') {

            //$('#sms_content').val('');
            //$('#sms_title').val('');
            $('#add_campaign')[0].reset();

            toastr.success("New Campaign Has Been Added Successfully", "Status");

        }else {
           toastr.failure("Sorry An Error Occured. Please try again", "Status");
        }

    }
});


$('.magree').click(function(){
    $('.mstory').val(meditor.getContent());
});

$('#edit_campaign').ajaxForm({
    beforeSend: function() {
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });

    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        console.log(percentVal);
        $('.blockUI h1').html(percentVal);
    },
    success: function() {

    },
    complete: function(xhr) {
        $.unblockUI();
        var rst = xhr['responseText'];

        console.log(rst);
        if(rst=='true') {

            //$('#sms_content').val('');
            //$('#sms_title').val('');
            //$('#add_campaign')[0].reset();
            //$('.btn-del').Click();
            toastr.success("Campaign has been updated successfully", "Status");

        }else {
            toastr.error("An Error occured please try again", "Status");
        }

    }
});


$(document).ready(function(){
    $('.dropzone').html5imageupload();

    var text_max = 200;
    $('#count_message').html(text_max + ' remaining');

    $('#text').keyup(function() {
      var text_length = $('#text').val().length;
      var text_remaining = text_max - text_length;
      
      if(text_remaining < 11){
        $('#count_message').html('<span style="color:red">'+text_remaining + ' remaining</span>');
      }else{
        $('#count_message').html(text_remaining + ' remaining');
      }
      
});

    $('#mcountry').on("change",function(){
        var val = $('#mcountry').val();
        console.log(val);
        if(val=="Ghana"){
            $("#mcurrency").html('Goal(GHS)<span style="color:red;" >*</span>');
        }else{
            $("#mcurrency").html('Goal($)<span style="color:red;" >*</span>');
        }
    });
});





