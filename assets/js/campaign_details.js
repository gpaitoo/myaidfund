(function (window, $) {
    'use strict';


    $(document).ready(function () {
        var wlink = $('#wlink').val();
        $(document).on("click", '.whatsapp', function () {
            //if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                var sText = "MYAIDFUND";
                var sUrl = wlink;
                var sMsg = encodeURIComponent(sText) + " - " + encodeURIComponent(sUrl);
                var whatsapp_url = "whatsapp://send?text=" + sMsg;
                window.location.href = whatsapp_url;
            /*}
        else {
                alert("Whatsapp client not available.");
            }*/
        });
    });

    // Cache document for fast access.
    var document = window.document;


    $('a.toggle-menu').click(function () {
        $('ul.menu').fadeToggle("slow");
    });


    var owl = $("#owl-demo");

    owl.owlCarousel({
        items: 6
    });

    // Custom Navigation Events
    $(".next").click(function () {
        owl.trigger('owl.next');
    });
    $(".prev").click(function () {
        owl.trigger('owl.prev');
    });


    $(document).ready(function () {
        $('.btn-facebook').on('click',function(){

            FB.ui({
                method: 'share',
                href: window.location.href,
            }, function(response){});
        });
    });

    $('#card_mtn,#card_airtel,#card_tigo').click(function () {
        hideAll();
        $('#mm_form').toggleClass('hide show');
    });

    $('#card_visa').click(function () {
        hideAll();
        $('#visa_form').toggleClass('hide show');
    });

    $('#card_voda').click(function () {
        hideAll();
        $('#voda_form').toggleClass('hide show');
    });


    function hideAll() {
        $('.sw_form').removeClass('show');
        $('.sw_form').addClass('hide');
    }



    $('#donate_form').submit(function (e) {
        e.preventDefault();
        var phone = $('#mphone').val();
        var opt = $('input[name=optradio]:checked').val();
        console.log(opt);
        if(opt==="mtn_old"){
            send_code(phone);
            BootstrapDialog.show({
                message: 'Please Enter The Code That Was Sent To: '+phone+' (The SMS Might Take a Minute) <input type="text" class="form-control">',
                onhide: function(dialogRef){
                    var ucode = dialogRef.getModalBody().find('input').val();

                    $.blockUI({ css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff',
                            'z-index': '999999'
                        } });

                    var ajaxRequest = $.ajax({
                        type: "POST",
                        url: 'process/check_code.php?code='+ucode,
                        contentType: false,
                        processData: false

                    });
                    ajaxRequest.done(function (xhr, textStatus) {
                        console.log(xhr);
                        $.unblockUI();
                        if (xhr === 'true') {
                            send_donation();
                            //BootstrapDialog.closeAll()
                        } else {
                            alert("Invalid Code Please Try Again");
                            return false
                        }

                    })

                },
                buttons: [{
                    label: 'Confirm',
                    action: function(dialogRef) {
                        dialogRef.close();
                    }
                }]
            });
        }else{
            send_donation();
        }

        //send_donation();
    });


    function send_code(phone) {
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            'z-index': '999999'
        } });

        var ajaxRequest = $.ajax({
            type: "POST",
            url: 'process/sendsms.php?phone='+phone,
            contentType: false,
            processData: false

        });
        ajaxRequest.done(function (xhr, textStatus) {
            console.log(xhr);
            $.unblockUI();
            /*if (xhr == 'true') {

                alert("");
            } else if (xhr == 501) {
                alert("Invalid Code Please Try Again");

            } else {
                //window.location= xhr;
            }*/

        })
    }


    function confirm_code(code) {
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            'z-index': '999999'
        } });

        var ajaxRequest = $.ajax({
            type: "POST",
            url: 'process/check_code.php?code='+code,
            contentType: false,
            processData: false

        });
        ajaxRequest.done(function (xhr, textStatus) {
            //console.log(xhr);
            $.unblockUI();
            if (xhr === 'true') {
                BootstrapDialog.closeAll()
            } else {
                alert("Invalid Code Please Try Again");
            }

        })
    }


    function send_donation(){
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            'z-index': '999999'
        } });

        var myform = document.getElementById("donate_form");
        var formData = new FormData(myform);
        var ajaxRequest = $.ajax({
            type: "POST",
            url: 'process/initiate_payment_hubtel_v2.php',
            contentType: false,
            processData: false,
            data: formData

        });
        ajaxRequest.done(function (xhr, textStatus) {
            console.log(xhr);
            $.unblockUI();
            $('#myModal').modal('hide');
            if (xhr.trim() === 'fail') {

                swal(
                    'Payment Status',
                    'An Error Occured with your payment please try again',
                    'error'
                );
            } else {
                window.location.href=xhr;
            }

        })
    }


    var handler = StripeCheckout.configure({
        key: 'pk_live_xLxigOHUr6NfZOt19dPs66Ks',
        image: 'assets/img/logo.png',
        token: function (token) {
            var damount = Math.floor($("#iamount").val() * 100);
            $("#stripeToken").val(token.id);
            $("#stripeEmail").val(token.email);
            $("#amountInCents").val(damount);
            $('#stripeAmount').val(damount);
            $('#name').val($('#fname').val());
            $("#stripeForm").submit();
        }
    });

    // DISABLED STRIPE
/*    $('.paystripe').on('click', function (e) {
        var amountInCents = Math.floor($("#iamount").val() * 100);
        var displayAmount = parseFloat(Math.floor($("#iamount").val() * 100) / 100).toFixed(2);
        // Open Checkout with further options
        handler.open({
            name: 'MyAidFund',
            description: 'Donation Amount ($' + displayAmount + ')',
            amount: amountInCents,
        });
        e.preventDefault();
    });*/

    $('.judopay').on('click', function (e) {
        e.preventDefault();
        var amount = Math.floor($("#iamount").val());
        var fname = $('#fname').val();
        var femail = $('#iemail').val();

        $('#judo_amount').val(amount);
        $('#judo_name').val(fname);
        $('#judo_email').val(femail);

        $('.p_error').html('');

        if(fname.length < 1 || amount.length < 1 || femail.length < 1){
            console.log('invalid fname');
            $('.p_error').html('All Fields Are Required');
            return false;
        }

        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                'z-index': '999999'
            } });

        var url = "process/judopay/judopay.php"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            url: url,
            data: $("#judoForm").serialize(), // serializes the form's elements.
            success: function (data) {
                console.log(data);
                if (data.length < 35) {
                    $.unblockUI();
                    swal(
                        'Payment Status',
                        'An Error Occured with your payment please try again with correct details',
                        'error'
                    );
                } else {
                    $('.payoptions').modal('hide');
                    $.unblockUI();

                        window.location.href = data;
                }

            },
            error:function(){
                $.unblockUI();
            }
        });

    });

    $('.hubtel_visa').on('click', function (e) {
        e.preventDefault();

        var amount = Math.floor($("#iamount").val());
        var fname = $('#fname').val();
        var femail = $('#iemail').val();

        $('.p_error').html('');

        if(fname.length < 1 || amount.length < 1 || femail.length < 1){
            console.log('invalid fname');
            $('.p_error').html('All Fields Are Required');
            return false;
        }

        var dlogin = $('#dlogin').val();
            $.blockUI({ css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    'z-index': '999999'
                } });


            $('#hub_amount').val(amount);
            $('#hub_name').val(fname);
            $('#hubemail').val(femail);

            var url = "process/hubtel_visa.php"; // the script where you handle the form input.

            $.ajax({
                type: "POST",
                url: url,
                data: $("#hubForm").serialize(), // serializes the form's elements.
                success: function (data) {
                    console.log(data);
                    if (data.length < 35) {
                        $.unblockUI();
                        swal(
                            'Payment Status',
                            'An Error Occured with your payment please try again with correct details',
                            'error'
                        );
                    } else {
                        $('.payoptions').modal('hide');
                        $.unblockUI();
                        if(dlogin !== 'true'){
                            login_form(data);
                        }else{
                            window.location.href = data;
                        }
                    }

                },
                error:function(){
                    $.unblockUI();
                }
            });

    });


    $('.paymm').on('click', function (e) {
        e.preventDefault();
        //var amount = Math.floor($("#iamount").val());
        var fname = $('#fname').val();
        var femail = $('#iemail').val();
        var displayAmount = parseFloat(Math.floor($("#iamount").val())).toFixed(2);

        $('#donate_amount').val(displayAmount);
        $('#email').val(femail);
        $('#amount').val(displayAmount);
        $('#fullname').val(fname);

        $('.p_error').html('');

        if(fname.length < 1 || displayAmount.length < 1 ){
            console.log('invalid fname');
            $('.p_error').html('Amount And Name Fields Are Required');
            return false;
        }else{
            $('.payoptions').modal('hide');
            $('.mmmodal').modal('show');
        }

    });

    $('#payoptions').on('show.bs.modal', function (e) {

        //get data-id attribute of the clicked element
        var campid = $(e.relatedTarget).data('donation-id');
        var camptitle = $(e.relatedTarget).data('title');
        //populate the textbox
        //$(e.currentTarget).find('input[name="bookId"]').val(bookId);
        $('.campid').val(campid);
        $('#hub_camp_id').val(campid);
        $('#hub_title').val(camptitle);
        $('#judo_camp_id').val(campid);
    });

    $('#stripeForm').submit(function (e) {
        e.preventDefault();

        var url = "process/stripe_process.php"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            url: url,
            data: $("#stripeForm").serialize(), // serializes the form's elements.
            success: function (data) {
                console.log(data);
                if (data.indexOf("err") >= 0) {
                    // $('#err').html(data.replace('err',''));
                } else if (data == 'true') {
                    //window.location.href = 'index.php';
                }

            }
        });
    });

    $(document).ready(function() {
        $(".cam").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });

})(window, jQuery);

