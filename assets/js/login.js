(function (window, $) {

    /* #####################################################################
   #
   #   Project       : Modal Login with jQuery Effects
   #   Author        : Rodrigo Amarante (rodrigockamarante)
   #   Version       : 1.0
   #   Created       : 07/29/2015
   #   Last Change   : 08/04/2015
   #
   ##################################################################### */

    $(function () {

        var $formLogin = $('#login-form');
        var $formLost = $('#lost-form');
        var $formRegister = $('#register-form');
        var $divForms = $('#div-forms');
        var $modalAnimateTime = 300;
        var $msgAnimateTime = 150;
        var $msgShowTime = 2000;

        $(".uform").submit(function () {
            switch (this.id) {
                case "login-form":

                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff',
                            'z-index':99999
                        }
                    });
                    var url = "process/process_users.php"; // the script where you handle the form input.
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $('#login-form').serialize(), // serializes the form's elements.
                        success: function (data) {
                            $.unblockUI();
                            if (data == "true") {
                                msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Login OK");
                                setTimeout(
                                    function () {
                                        window.location.href = 'campaign_home.php';
                                    }, 1500);

                            } else if(data=='captcha') {
                                msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Captcha Failed");
                            }else{
                                msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");

                            }

                        }
                    });
                    return false;
                    break;
                case "lost-form":
                    var $ls_email = $('#lost_email').val();
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff',
                            'z-index':99999
                        }
                    });
                    var url = "process/lost_password.php"; // the script where you handle the form input.
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $('#lost-form').serialize(), // serializes the form's elements.
                        success: function (data) {
                            $.unblockUI();
                            if (data == "true") {
                                swal("Password Reset Status", "An Email has been sent to "+$ls_email+" Kindly check your mail for a link to reset your password", "success");

                            } else if(data=='captcha') {
                                swal("Password Reset Status", data, "error");
                            }else{
                                swal("Password Reset Status", data, "error");
                            }

                        }
                    });

                    /*if ($ls_email == "ERROR") {
                        msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
                    } else {
                        msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
                    }*/

                    break;
                case "register-form":
                    var $rg_username = $('#register_username').val();
                    var $rg_email = $('#register_email').val();
                    var $rg_password = $('#register_password').val();
                    if ($rg_username == "ERROR") {
                        msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", "Register error");
                    } else {
                        msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "success", "glyphicon-ok", "Register OK");
                    }

                    break;
                default:
                    return false;
            }

            return false;
        });

        $('#login_register_btn').click(function () {
            modalAnimate($formLogin, $formRegister)
        });
        $('#register_login_btn').click(function () {
            modalAnimate($formRegister, $formLogin);
        });
        $('#login_lost_btn').click(function () {
            modalAnimate($formLogin, $formLost);
        });
        $('#lost_login_btn').click(function () {
            modalAnimate($formLost, $formLogin);
        });
        $('#lost_register_btn').click(function () {
            modalAnimate($formLost, $formRegister);
        });
        $('#register_lost_btn').click(function () {
            modalAnimate($formRegister, $formLost);
        });

        function modalAnimate($oldForm, $newForm) {
            var $oldH = $oldForm.height();
            var $newH = $newForm.height();
            $divForms.css("height", $oldH);
            $oldForm.fadeToggle($modalAnimateTime, function () {
                $divForms.animate({height: $newH}, $modalAnimateTime, function () {
                    $newForm.fadeToggle($modalAnimateTime);
                });
            });
        }

        function msgFade($msgId, $msgText) {
            $msgId.fadeOut($msgAnimateTime, function () {
                $(this).text($msgText).fadeIn($msgAnimateTime);
            });
        }

        function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
            var $msgOld = $divTag.text();
            msgFade($textTag, $msgText);
            $divTag.addClass($divClass);
            $iconTag.removeClass("glyphicon-chevron-right");
            $iconTag.addClass($iconClass + " " + $divClass);
            setTimeout(function () {
                msgFade($textTag, $msgOld);
                $divTag.removeClass($divClass);
                $iconTag.addClass("glyphicon-chevron-right");
                $iconTag.removeClass($iconClass + " " + $divClass);
            }, $msgShowTime);
        }
    });

    $('.prst').ajaxForm({
        beforeSend: function() {

            $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } });

        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            console.log(percentVal);
            $('.blockUI h1').html(percentVal);
        },
        success: function() {

        },
        complete: function(xhr) {
            $.unblockUI();
            var rst = xhr['responseText'];

            console.log(rst);
            if (rst === 'true') {
                swal("Password Reset Status", "Your password has been changed Successfully", "success");
                setTimeout(
                    function () {
                        window.location.href = 'index.php';
                    }, 1500);
                //alert("Your Documents Have Been Uploaded Successfully");
            } else {
                swal("Password Reset Status", rst, "error");

            }

        }
    });

})(window, jQuery);