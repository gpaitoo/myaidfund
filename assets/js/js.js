
(function ($) {
   "use strict";

	// Navigation
	$("#menu-toggle").on("click", function(){
		$(".menu-wrap").toggleClass("open");
	});
	$(".menu li").hover(function() {
  	 	$(this).find(".submenu").stop(true, true).delay(1).slideDown(170);
	}, function() {
	    $(this).find(".submenu").stop(true, true).delay(200).slideUp(170);
	});
	
	$('#mobile-nav').meanmenu();
	
	//Search
	$("#search-toggle").on("click", function() {
	    $("#search-bar").slideDown(170);
	});
	$("#search-close").on("click", function() {
	    $("#search-bar").slideUp(170);
	});


	// Nivo Light-Box
	var nivoActivator = $('.nivo-activator');
	if (nivoActivator.css("display") == "block"){
   		$('.slider-btn, .nivo-trigger').nivoLightbox({
   			theme: 'default'
   		});
	};

    // Countdown
  if ($('.countdown').length>0) {
    $(".countdown").countdown({
      date: "28 june 2016 12:00:00", // Edit this line
      format: "on"
    },
    function() {
      // This will run when the countdown ends
    });
  }

    // Main Slider
    $(".slider-active").owlCarousel({
    	items: 1,
    	responsiveClass:true,
	    dots: true,
    	responsive:{
	      0:{
	        items:1
	      },
	      450:{
	        items:1,
	      },
	      650:{
         items:1,
	      },
	      991:{
	     	 item:1,
	      },
	    }
    });  

	 // Carousels
  if($("#partners-slider").length>0){
    $("#partners-slider").owlCarousel({
    	items: 4,
    	responsiveClass:true,
		autoPlay: true,
	    dots: false,
    	responsive:{
	      0:{
	        items:1
	      },
	      450:{
	        items:2,
	      },
	      650:{
         items:3,
	      },
	      991:{
	     	 item: 4,
	      },
	    }
    });
  };

  if ($("#post-slider").length>0) {
  	$("#post-slider").owlCarousel({
  		items: 1,
	  	nav : true,
	    navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	    autoPlay: true,
	    dots: true,
  	});
  };


  // Causes Filter 
	// Causes Filter 
  var $causeslist = $('.causes-list');
   
  // initialize isotope
  if (($causeslist).length> 0) {
    $causeslist.isotope();
    $(window).resize( function(){ 
      setTimeout(function() { 
        $causeslist.isotope('layout'); 
      }, 1000);  
    });
    $(window).load(function(){ 
       $causeslist.isotope('layout'); 
    });
  };

  $(".causes-filter").on("click", "li", function() {
    $("li.selected").removeClass("selected");
    $(this).addClass("selected");
    var selector = $(this).attr("data-filter");
    $causeslist.isotope({
      filter: selector,
    });
  });
	

	// Dropdown
	$(".dropdown ").on("click", function(){
		$(".dropdown-list").toggleClass("on");
	});

	// Dropdown Replaced Texts
	$(".dropdown-list ").on("click", "li",function(){
	    $('.dropdown-label').text($(this).text());
	    $('.dropdown-label').text();
	});
	
	
	/*--[ progress bar ]--*/
	$('.progress-bar > span').each(function () {
		var $this = $(this);
		var width = $(this).data('percent');
		$this.css({
			'transition': 'width 3s'
		});
		setTimeout(function () {
			$this.appear(function () {
				$this.css('width', width + '%');
			});
		}, 500);
	});
	
	$.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });
	
    /* --- Google Map --- */

	var mapLocation = new google.maps.LatLng(5.5663457,-0.2159146);

	var $mapis = $('#map');

	if ($mapis.length > 0) {

		var map;
		map = new GMaps({
			streetViewControl : true,
			overviewMapControl: true,
			mapTypeControl: true,
			zoomControl : true,
			panControl : true,
			scrollwheel: false,
			center: mapLocation,
			el: '#map',
			zoom: 15,
			styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
		});

		var image = new google.maps.MarkerImage('assets/img/map-icon.png');

		map.addMarker({
			position: mapLocation,
			icon: image,
			title: 'Myaidfund',
			infoWindow: {
				content: '<p><strong>Myaidfund </strong><br/>114 kwame nkrumah Ave. Adabraka - Accra, Ghana<br/>P: +233 24 653 1362<br/>Accra, GH, Ghana</p>'
			}
		});

	}
	

	var map2Location = new google.maps.LatLng(40.7557182,-74.2129342);

	var $mapis = $('#map2');

	if ($mapis.length > 0) {

		var map;
		map = new GMaps({
			streetViewControl : true,
			overviewMapControl: true,
			mapTypeControl: true,
			zoomControl : true,
			panControl : true,
			scrollwheel: false,
			center: map2Location,
			el: '#map2',
			zoom: 15,
			styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
		});

		var image = new google.maps.MarkerImage('assets/img/map-icon.png');

		map.addMarker({
			position: map2Location,
			icon: image,
			title: 'Myaidfund',
			infoWindow: {
				content: '<p><strong>Myaidfund </strong><br/>67 South Muwn Ave apt 89, <br/>East Orange , NJ <br/>07018,USA <br/>P: +1(201)673-6337<br/>Accra, GH, Ghana</p>'
			}
		});

	}


	$('#contact_us').ajaxForm({
    beforeSend: function() {
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });

    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        console.log(percentVal);
        $('.blockUI h1').html(percentVal);
    },
    success: function() {

    },
    complete: function(xhr) {
        $.unblockUI();
        var rst = xhr['responseText'];

        console.log(rst);
        if(rst=='true') {

            //$('#sms_content').val('');
            //$('#sms_title').val('');
            //$('#add_campaign')[0].reset();
            //$('.btn-del').Click();
            toastr.success("Your message has been submitted successfully", "Status");
            document.getElementById("contact_us").reset();

        }else {
            toastr.error("An Error occured please try again", "Status");
        }

    }
});
	
	

})(jQuery);	