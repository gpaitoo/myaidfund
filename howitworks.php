<?php
$page = 'How it Works';
include_once ('header.php');
include ('includes/allfunctions.php');

?>

    <style>
        .container{
            width:95%;
        }
    </style>
    <!-- basic-slider start -->
    <!--<div class="slider-section">
        <div class="slider-active owl-carousel">
            <div class="single-slider slider-screen nrbop bg-black-alfa-40" style="background-image: url(assets/img/slides/team.jpg);">
                <div class="container">
                    <div class="slider-content text-white">
                        <h2 class="b_faddown1 cd-headline clip is-full-width" >Empowering Social Funding</h2>
                        <p class="b_faddown2">Myaidfund.com is the number 1 crowdfunding platform in Ghana and beyond.
                        <div class="slider_button b_faddown3"><a href="#our_mission">Our Mission</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>-->
    <!-- basic-slider end -->

    <!-- Features -->


    <!-- Special Cuase Paralax -->


    <!-- Causes -->



    <!-- team -->
    <div class="team-wrapper" style="padding-top: 0px" id="our_mission">
        <div class="container">
            <div class="section-name one">
                <h2>How it Works</h2>
                <br/>
                <div class="tags" style="text-align: left;">
                    <p style="font-size: 18px"><strong>Step 1: Create an account, log in and create your fundraising Campaign -</strong>
                        There is no easier way to share your story and attract support from friends, family and love one cross the world.

                    </p>
                    <br/>
                    <p style="font-size: 18px">
                        <strong>Step 2: Upload your personal Identification and/or Business registration document for verification - </strong>
                        In accordance with national laws and central bank’s guidelines on KYC (Know Your Customer), we are mandated to verify every person or organization who have created an account on our website, in order to allow your campaign to go live on our website and start accepting donation. We verify personal ID (Passport, Voters ID, Drivers Licence etc) for personal accounts and business documents, business owner or Director ID for business or organization accounts.
                    </p>

                    </p>
                    <br/>
                    <p style="font-size: 18px">
                        <strong>Step 3: Share with Friends, Family and love ones on social media platforms - </strong>
                        Our built in connections to Facebook, twitter, whatsapp etc. make sharing your campaign much easier
                    </p>

                    <br/>
                    <p style="font-size: 18px">
                        <strong>Step 4: Start accepting Donations - </strong>
                        You can receive your money by requesting a bank transfer or cheque after filling your bank account details and providing us with a confirmation letter from your banker’s confirming your accounts details with the bank on the banks letterhead.

                    </p>
                    <br/>
                    <p style="font-size: 18px">
                        <strong>Step 5: Monitor your Campaign results - </strong>
                        With our real time updates on donations you can monitor your campaign performance anytime, anywhere. You can also make changes, post updates and send thank you notes from dashboard
                    </p>

                </div>
            </div>


        </div>
    </div>





    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>