<?php
$donation_id = trim($_REQUEST['donate_id']);
$page        = 'campaign_details';
include ('includes/allfunctions.php');
include 'comments/comments.class.php';

$ipinfo = ip_info("Visitor", "Country");
if($is_production){
    $db_details = array(
        'db_host' => HOST,
        'db_user' => USERNAME,
        'db_pass' => PASSWORD,
        'db_name' => DATABASE,
    );
}else{
    $db_details = array(
        'db_host' => HOST,
        'db_user' => LOCAL_USERNAME,
        'db_pass' => LOCAL_PASSWORD,
        'db_name' => LOCAL_DATABASE,
    );
}

$title           = '';
$story           = '';
$date_created    = '';
$date_end        = '';
$amount_goal     = '';
$amount_achieved = '';
$top_img         = '';
$brief           = '';
$video_title = '';
$video_url  = '';
$ownerid         = 0;
$results         = $mysqli->query("SELECT * FROM campaign where id = $donation_id");
//echo $results->num_rows;
while ($row = $results->fetch_object()) {
	$title        = $row->title;
	$story        = $row->story;
	$date_created = $row->date_created;
	$date_end     = $row->date_end;
	$amount_goal  = $row->amount_goal;
	//$amount_achieved = $row->amount_achieved;
	$top_img = $row->top_img;
	$brief   = $row->brief;
	$ownerid = $row->ownerid;
	$video_url = $row->video_url;
	$video_title = $row->video_title;
}
$amount_achieved = get_donation_sum($donation_id);
$crst            = determine_donation_currency($donation_id);
$currency        = '$';
if ($crst == 'USD') {
	$currency = '$';
} else {
	$currency = 'GHS';
}

    $video_tag = '';

if(contains("youtube.com",strtolower($video_url))){
    $video_tag = '<h2>'.$video_title.'</h2>
                    <iframe style="width:100%" height="300" frameborder="0" allowfullscreen
                            src="'.$video_url.'?controls=1">
                    </iframe>';
}
$meta = '
<meta property="og:url"           content="https://myaidfund.com/campaign_details.php?donate_id='.$donation_id.'"/>
<meta property="og:type"          content="website" />
<meta property="og:title"         content="'.$title.'" />
<meta property="og:image"         content="'.$top_img.'" />
<meta property="og:description"   content="'.$brief.'" />

<!-- Twitter metadata -->
<meta content="summary" name="twitter:card"/>
<meta content="@MyaidFund" name="twitter:site"/>
<meta content="'.$title.'" name="twitter:title"/>
<meta content="'.$brief.'" name="twitter:description"/>
<meta content="https://myaidfund.com/'.$top_img.'" name="twitter:image"/>
';

/*$meta = "<meta content='summary' name='twitter:card'>
<meta content='@Fundly' name='twitter:site'>
<meta content='Click here to support Mandys Expedition to Ghana by Mandy Raddon' name='twitter:title'>
<meta content='' name='twitter:description'>
<meta content='https://s3.amazonaws.com/fundly_uploads/uploads/39850bed-a65a-44fa-9ce0-628a9fdb1a74.png' name='twitter:image'>";*/

include_once ('header.php');
insert_page_visit($donation_id);
?>
<div class="container" style="margin-top: 10px;background-color: white">


    <div class="content-section">
    <div class="container">
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8 col-md-8 col-sm-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php echo $title;?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#"><?php echo get_owner_name($ownerid);?></a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Campaign Started on <?php echo $date_created;
?> and will end on <?php echo $date_end;
?></p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" style="width: 100%;" src="<?php echo $top_img;?>" alt="">

                <hr>

                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <button style="width:100%; margin-top:10px;" type="button" class="btn btn-hot text-uppercase btn-lg" data-toggle="modal" data-target="#payoptions" data-donation-id="<?php echo $donation_id?>"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Donate </button>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <a  class="whatsapp"  >
                            <div style="width:100%; margin-top:10px;" type="button"
                                 class="btn btn-whatapp btn-lg"><i class="fa fa-whatsapp fa-2"></i> Share
                            </div>
                        </a>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <button style="width:100%; margin-top:10px;" type="button" class="btn btn-facebook btn-lg fb-share-button" data-href="http://www.your-domain.com/your-page.html"  data-layout="button_count"><i class="fa fa-facebook fa-2"></i> Share</button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                    <a target="_blank" href="https://twitter.com/share?url=https://myaidfund.com/campaign_details.php?donate_id=<?echo $donation_id;?>&via=MyAidFund&related=Help Raise Fund&hashtags=MyAidFund&text=<?echo substr($title, 0, 140)?>">
                        <button style="width:100%; margin-top:10px;" type="button"
                                class="btn btn-twitter btn-lg"><i class="fa fa-twitter fa-2"></i> Share
                        </button>
                    </a>
                    </div>
                </div>

                <div>
                    <?php echo $video_tag; ?>
                </div>



                <hr>


                <hr>



                <hr>

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Campaign Details</a></li>
                    <li><a data-toggle="tab" href="#menu1">Campaign Updates</a></li>
<!--                    <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>-->
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
<!--                        FIRST TAB CONTENT-->
<br/>
                        <!-- Post Content -->
                        <p class="lead"><?php echo $story;?></p>

                        <!-- Blog Comments -->

                        <!-- Comments Form -->
                        <div class="well">
                            <?php
                            $page_id  = $donation_id;
                            $comments = new Comments_System($db_details);
                            $comments->grabComment($page_id);

                            if ($comments->success) {
                                echo "<div class='alert alert-success' id='comm_status'>".$comments->success."</div>";
                            } else if ($comments->error) {
                                echo "<div class='alert alert-error' id='comm_status'>".$comments->error."</div>";
                            }

                            // a simple form
                            echo $comments->generateForm();

                            // we show the posted comments
                            echo $comments->generateComments($page_id);// we pass the page id
                            ?>
                        </div>

                        <hr>

                        <!-- Posted Comments -->

                        <!-- Comment -->


                        <!-- Comment -->


                    </div>
                    <div id="menu1" class="tab-pane fade">
<!--                        SECOND TAB CONTENT-->
<br/>
                        <?php
                        $update = get_campaign_update_by_id($donation_id);
                        if(strlen($update)>5){
                            print_r($update);
                        }else{
                            echo "There are no updates for this campaign";
                        }
                        ?>

                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                </div>


            </div> <!-- /.col-md-5 -->
            <!-- Blog Sidebar Widgets Column -->
            <div class="col-lg-4 col-md-4 col-sm-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Search Campaigns</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Support Us</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well text-center">
                                <button style="width:100%; margin-top:10px;" type="button" class="btn btn-hot text-uppercase btn-lg" data-toggle="modal" data-target="#payoptions" data-donation-id="<?php echo $donation_id?>"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Donate</button>


                                <input type="hidden" id="wlink" value="https://myaidfund.com/campaign_details.php?donate_id=<? echo $donation_id ?>">
                                <a  class="whatsapp"  >
                                    <button style="width:100%; margin-top:10px;" type="button"
                                            class="btn btn-whatapp btn-lg"><i class="fa fa-whatsapp fa-2"></i> Share
                                    </button>
                                </a>

                                <button style="width:100%; margin-top:10px;" type="button" class="btn btn-facebook btn-lg" data-layout="button_count"  ><i class="fa fa-facebook fa-2" ></i> Share</button>
                                <a target="_blank" href="https://twitter.com/share?url=https://myaidfund.com/campaign_details.php?donate_id=<?echo $donation_id;?>&via=MyAidFund&related=Help Raise Fund&hashtags=MyAidFund&text=<?echo substr($title, 0, 140);?>">
                                    <button style="width:100%; margin-top:10px;" type="button"
                                            class="btn btn-twitter btn-lg"><i class="fa fa-twitter fa-2"></i> Share
                                    </button>
                                </a>
                                <div class="f-p-t-medium text-left">
                                    <h3 class="f-lh-xsmall" style="font-size: 45px; margin:0px;">
<?php
echo get_number_of_donations($donation_id);
?>
</h3>
                                    <a class="scroll f-font-condensed f-caps f-fw-bold " href="#donors">
                                        Donors
                                    </a>
                                </div>
                                <!-- AMOUNT DONATED -->
                                <br/>
                                <div class="f-m-t-medium text-left">
                                    <h4 class="f-m-t-small f-lh-xsmall js-raise" style="font-size: 45px; margin:0px;">
<?echo $currency.number_format($amount_achieved)?>
                                    </h4>
                                    <a class="scroll f-font-condensed f-ilb f-caps f-fw-bold">
                                        Raised
                                        <span>
(USD)
</span>
                                    </a>
                                </div>

                                <div class="f-m-t-medium">
                                    <div class="f-thermometer f-m-b-xsmall">
                                        <div class="f-mercury f-bg-primary" style="width: <?echo (($amount_achieved/$amount_goal)*100);?>%"></div>
                                    </div>


                                    <div class="f-font-condensed text-left">
<span class="f-fw-bold">
Goal:
</span>
                                        <span class="js-campaign-goal-target">
<?echo $currency.number_format($amount_goal);?>
</span>
                                    </div>

                                    <div class="f-fr f-font-condensed text-left">
<span class="f-fw-bold">
Days Left:
</span>
                                        <span class="js-days-left-target"><?php echo get_days_left($date_end); ?></span>
                                    </div>

                                </div>

                                <br/>
                                <a href="#" style="display: block;text-align: right;"><img src="assets/img/logo.png" width="50px"/></a>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
<!--                <div class="well">-->
<!--                    <h4>Donars</h4>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>-->
<!--                </div>-->


                <div class="well text-center">
                    <h4>Report This Campaign</h4>
                    <a href="contact-us.php"> <div class="btn btn-big btn-solid " style="background-color: red">Report</div></a>
                </div>
            </div>
        </div> <!-- /.row -->

</div>

    </div>

</div>


    <div class="modal fade payoptions" id="payoptions" tabindex="-1" role="dialog" aria-labelledby="payoptions">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content">
                    <div class="row" style="margin-left:0; margin-right:0; padding-bottom:10px">
                        <form class="inidon">
                            <input type="hidden" value="<?php echo $title; ?>" id="camp_title"/>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <h3 style="margin:10px 0 10px 0;" class="text-center">Payment Option</h3>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Amount</label>
                                    <input required="required" class="form-control cam" style="width:100%;" type="text" id="iamount" value="10" placeholder="Amount" />
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Full Name</label>
                                    <input required class="form-control" style="width:100%;" type="text" id="fname" value="" placeholder="Full Name"/>
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Email Address</label>
                                    <input required class="form-control" style="width:100%;" type="email" id="iemail" value="" placeholder="Email Address"/>
                                </div>

                            </div>
                            <br/>
                            <div><p style="color:red;font-weight: bold" class="p_error text-center"></p></div>
                            <br/>


                            <?php if($ipinfo=='Ghana'){ ?>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 hide">
                                <button style="width:100%;" type="submit" class="btn btn-primary btn-lg paystripe hubtel_visa" ><i class="fa fa-credit-card fa-2"></i> Visa/Mastercard</button>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <?php }else{?>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 hide">
                                <button style="width:100%;" type="submit" class="btn btn-primary btn-lg judopay" ><i class="fa fa-credit-card fa-2"></i> Visa/Mastercard</button>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <?php }?>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <button style="width:100%;" type="submit" class="btn btn-danger btn-lg paymm" data-dismiss="modal" aria-hidden="true"><i class="fa fa-money fa-2"></i> Donate </button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade mmmodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content">

                    <h2 class="text-center">Payment Options</h2>
                    <div class="modal-body">
                        <div class="">
                            <form class="form-goup" style="padding: 8px;" id="donate_form">

                                <input type="hidden" name="email" id="email"/>
                                <input type="hidden" name="campid" value="<?php echo $donation_id?>"/>
                                <input type="hidden" name="isregistered" value="0"/>
                                <input type="hidden" name="donatorid" value="-1"/>
                                <input type="hidden" name="trans_source" value="WEB"/>

                                <div class="form-group">
                                    <label class="sr-only" for="donate_amount">Amount (in Ghana Cedis)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">GH₵</div>
                                        <input type="number" name="amount" class="form-control" id="donate_amount" placeholder="Amount">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input type="hidden" name="optradio" value="mtn"/>
                                    <!--<div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input checked style="margin-top: 18px;" type="radio" id="card_mtn" value="mtn" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/mtn-mobile-money.png"></label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_airtel" value="airtel" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/airtel-money.png"/></label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_tigo" value="tigo" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/tigo-cash.png"/></label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_voda" value="voda" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/vodafone-cash-ghana.png"/></label>
                                        </div>
                                    </div>-->
                                    <!--                    <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_visa" value="visa" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="http://www.catnets.com.au/images/visa_mastercard_logo.png"/></label>-->




                                </div>

                                <!--VISA MASTER CARD-->
                                <!--<div id="visa_form" class="sw_form show">
                                    <div class="form-group">
                                        <label class="sr-only" for="fname">First Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">First Name</div>
                                            <input name="fname" type="text" class="form-control" id="fname" placeholder="First Name"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only" for="lname">Last Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Last Name</div>
                                            <input name="lname" type="text" class="form-control" id="lname" placeholder="Last Name"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only" for="email">Email</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Email</div>
                                            <input name="email" type="text" class="form-control" id="email" placeholder="Email"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only" for="phone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone Number</div>
                                            <input name="vmphone" type="text" class="form-control" id="phone" placeholder="Phone Number"/>
                                        </div>
                                    </div>
                                </div>-->


                                <!--OTHER MOBILE MONEY-->

                                <div id="mm_form" class="sw_form">
                                    <div class="form-group">
                                        <label class="sr-only" for="fullname">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input name="fullname" type="text" class="form-control" id="fullname" placeholder="Name"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="sr-only" for="mphone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone</div>
                                            <input name="phone" type="tel" class="form-control" id="mphone" placeholder="Phone Number"/>
                                        </div>
                                    </div>
                                </div>
                                <!--VODAFONE CASH-->

                                <div id="voda_form" class="sw_form hide">
                                    <div class="form-group">
                                        <label class="sr-only" for="fullname">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input name="vfullname" type="text" class="form-control" id="vfullname" placeholder="Name"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="sr-only" for="mphone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone</div>
                                            <input name="vphone" type="text" class="form-control" id="vphone" placeholder="Phone Number"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only" for="token">Token</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Token</div>
                                            <input name="token" type="text" class="form-control" id="token" placeholder="Token"/>
                                        </div>
                                    </div>

                                </div>
                                <div style="display: block;text-align: right;">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Donate</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>



    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid" value="<?php echo $donation_id;?>"/>

    </form>


    <!--    HUBTEL SCRIPTS-->

    <form id="hubForm" action="process/hubtel_visa.php" method="POST">
        <input type="hidden" id="hubemail" name="email" />
        <input type="hidden" id="hub_amount" name="amount"/>
        <input type="hidden" id="hub_title" name="title" value="<?php echo $title ?>"/>
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="hub_name" name="name"/>
        <input type="hidden" class="hub_campid" name="campid" value="<?php echo $donation_id;?>"/>
    </form>


<!--JUDO VISA SCRIPT-->
    <form id="judoForm" action="process/judopay/judopay.php" method="POST">
        <input type="hidden" id="judo_email" name="email" />
        <input type="hidden" id="judo_amount" name="amount"/>
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="judo_name" name="name"/>
        <input type="hidden" class="judo_campid" name="campid" value="<?php echo $donation_id;?>"/>
    </form>


<?php include_once ('footer.php');?>