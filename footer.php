<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="assets/img/logo.png">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>

            <!-- Begin # DIV Form -->
            <div id="div-forms">

                <!-- Begin # Login Form -->
                <form class="uform" id="login-form">
                    <input type="hidden" name="opera" value="login">
                    <div class="modal-body">
                        <div id="div-login-msg">
                            <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-login-msg">Type your username and password.</span>
                        </div>
                        <input id="login_username" name="xxxname" class="form-control" type="text" placeholder="Username" required>
                        <input id="login_password" name="xxxpass" class="form-control" type="password" placeholder="Password" required>
                        <br/>

                            <div class="g-recaptcha" data-sitekey="6Ld5qUsUAAAAAFoDW9Oj_ugh0k9XyWTYaKMe4IYG"></div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                        </div>
                        <div>
                            <button id="login_lost_btn" type="button" style="color:blue" class="btn-link">Lost Password?</button>
<!--                            <button id="login_register_btn" type="button" style="color:blue" class="btn-link">Register</button>-->
                        </div>
                    </div>
                </form>
                <!-- End # Login Form -->

                <!-- Begin | Lost Password Form -->
                <form class="uform" id="lost-form" style="display:none;">
                    <input type="hidden" name="opera" value="lost">
                    <div class="modal-body">
                        <div id="div-lost-msg">
                            <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-lost-msg">Type your e-mail.</span>
                        </div>
                        <input id="lost_email" name="xxxemail" class="form-control" type="text" placeholder="E-Mail" required>
                        <br/>
<!--                        <div class="g-recaptcha" data-sitekey="6Ld5qUsUAAAAAFoDW9Oj_ugh0k9XyWTYaKMe4IYG"></div>-->
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
                        </div>
                        <div>
                            <button id="lost_login_btn" type="button" style="color:blue" class=" btn-link">Log In</button>
<!--                            <button id="lost_register_btn" type="button" style="color:blue" class=" btn-link">Register</button>-->
                        </div>
                    </div>
                </form>
                <!-- End | Lost Password Form -->

                <!-- Begin | Register Form -->
                        <form class="uform" id="register-form" style="display:none;">
                            <input type="hidden" name="opera" value="register">
                    <div class="modal-body">
                        <div id="div-register-msg">
                            <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-register-msg">Register an account.</span>
                        </div>
                        <input id="register_username" name="xxxname" class="form-control" type="text" placeholder="Username (type ERROR for error effect)" required>
                        <input id="register_email" name="xxxemail" class="form-control" type="text" placeholder="E-Mail" required>
                        <input id="register_password" name="xxxpass" class="form-control" type="password" placeholder="Password" required>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                        </div>
                        <div>
                            <button id="register_login_btn" type="button" style="color:blue" class=" btn-link">Log In</button>
                            <button id="register_lost_btn" type="button" style="color:blue" class=" btn-link">Lost Password?</button>
                        </div>
                    </div>
                </form>
                <!-- End | Register Form -->

            </div>
            <!-- End # DIV Form -->

        </div>
    </div>
    </div>
</div>


<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="about widget clearfix">
						<div class="logo-wrap">
							<a href="index.php">MyAidFund</a>
						</div>
						<p>MyAidFund Gives You The Best In Crowd Fundraising.
                            <br>Online Fundraising Has Never Been Easier. Get started and accept online donations instantly  </p>
						<div class="social-media-icons">
							<a href="http://www.twitter.com/myaidfund/" target="_blank"><i class="fa fa-twitter"></i><span>Twitter</span></a>
							<a href="#" target="_blank"><i class="fa fa-google-plus"></i><span>Google +</span></a>
							<a href="http://www.facebook.com/Myaidfund/" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
							<a href="http://www.linkedin.com/company/myaidfund/" target="_blank"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
							<a href="https://www.instagram.com/myaidfund/" target="_blank"><i class="fa fa-instagram"></i><span>Instagram</span></a>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 ">
					<div class="quick-links widget clearfix">
						<h4 class="title">Quick Links</h4>
						<div class="links">
							<a href="about_us.php"><i class="fa fa-angle-double-right"></i>About Us</a>
                            <a href="contact-us.php"><i class="fa fa-angle-double-right"></i>Contact Us</a>
<!--                            <a href="faq.php"><i class="fa fa-angle-double-right"></i>Faq</a>-->
							<a href="pricing.php"><i class="fa fa-angle-double-right"></i>Pricing and Fee</a>
<!--							<a href="termsandconditions.php"><i class="fa fa-angle-double-right"></i>Terms and Conditions</a>-->
                            <a href="terms_of_use.php"><i class="fa fa-angle-double-right"></i>Terms of Use</a>
							<a href="privacy_policy.php"><i class="fa fa-angle-double-right"></i>Privacy Policy</a>
							<a href="howitworks.php"><i class="fa fa-angle-double-right"></i>How it Works</a>
                            <a href="safety.php"><i class="fa fa-angle-double-right"></i>Safety</a>
							<a href="register_campaign.php"><i class="fa fa-angle-double-right"></i>Create Campaign</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 ">
					<div class="tags-outer widget clearfix">
						<h4 class="title">Categories</h4>
						<div class="tags">
                            <a href="campaigns.php?catg=community">Clubs &amp; Community</a>
                            <a href="campaigns.php?catg=arts">Creative Projects </a>
                            <a href="campaigns.php?catg=greek">Fraternities &amp; Sororities</a>
                            <a href="campaigns.php?catg=other">Fun &amp; Special Events</a>
                            <a href="campaigns.php?catg=children">Kids &amp; Family</a>
<!--                            <a href="campaigns.php?catg=lgbt">LGBT</a>-->
                            <a href="campaigns.php?catg=health">Medical &amp; Health</a>
                            <a href="campaigns.php?catg=memorial">Memorials &amp; Funerals</a>


						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
                    <div class="tags-outer widget clearfix">
                        <h4 class="title"></h4>
                        <div class="tags">
                            <a href="campaigns.php?catg=military">Military</a>
                            <a href="campaigns.php?catg=non-profit">Non-Profit and Charity</a>
                            <a href="campaigns.php?catg=animal">Pets &amp; Animals </a>
                            <a href="campaigns.php?catg=political">Politics &amp; Public Office</a>
                            <a href="campaigns.php?catg=church">Religious Organizations</a>
                            <a href="campaigns.php?catg=run-walk-ride">Run/Walk/Rides </a>
                            <a href="campaigns.php?catg=education">Schools &amp; Education</a>
                            <a href="campaigns.php?catg=sports">Sports &amp; Teams</a>
                            <a href="campaigns.php?catg=trips">Trips &amp; Adventures</a>
                        </div>
                    </div>
					<!--<div class="subcribe widget clearfix">
						<h4 class="title">Subscribe</h4>
						<p>To receive email on Trending campaigns, please provide you email address below.</p>
						<form action="#">
							<div class="field">
								<input type="email" name="e-mail" placeholder="Your E-mail">
							</div>
							<div class="field">
								<button class="btn btn-min btn-solid"><span>Subscibe</span></button>
							</div>
						</form>
					</div>-->
				</div>
			</div>
		</div>
		<div class="footer-bar">
			<div class="container">
				<h5>Copyright ©2018 MyAidfund. All Rights Reserved</h5>
                <h5>Powered by <a href="https://apptechhubglobal.com" target="_blank">AppTechHubGlobal</a></h5>
			</div>
		</div>
	</footer>

<?php include('addons.php'); ?>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<!--<script async defer src="https://apis.google.com/js/api.js"  onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>-->
	<!-- Scripts -->
	<script type="text/javascript" src="assets/js/jquery2.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.meanmenu.js"></script>
	<script type="text/javascript" src="assets/js/progress-bar-appear.js"></script>
	<script type="text/javascript" src="assets/owl-carousel/owl.carousel.min.js"></script>
	<script type="text/javascript" src="assets/js/nivo-lightbox.min.js"></script>
	<script type="text/javascript" src="assets/js/isotope.min.js"></script>
	<script type="text/javascript" src="assets/js/countdown.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBEypW1XtGLWpikFPcityAok8rhJzzWRw "></script>
	<script type="text/javascript" src="assets/js/gmaps.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.blockui.min.js"></script>
    <script src="assets/js/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert2"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js" ></script>
	<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/login.js" ></script>
<script type="text/javascript" src="assets/js/main.js" ></script>
<script type="text/javascript" src="assets/js/general.js" ></script>

<?php if (@$is_post_form):?>
<script type="text/javascript" src="assets/js/registration.js" ></script>
<?php endif;?>

<?php if (@$page == 'create_campaign'):?>
<script type="text/javascript" src="assets/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="assets/js/create_campaign.js" ></script>
<script src="assets/js/h5upload.js" ></script>

<?php endif;?>

<?php if (@$page == 'payment_request'):?>
    <script type="text/javascript" src="assets/js/payment_request.js" ></script>
<?php endif;?>

<?php if (@$page == 'campaign_details'):?>
<script type="text/javascript" src="assets/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="assets/js/campaign_details.js" ></script>
<!--    <script type="text/javascript">	if(typeof wabtn4fg==="undefined")	{wabtn4fg=1;h=document.head||document.getElementsByTagName("head")[0],s=document.createElement("script");s.type="text/javascript";s.src="assets/js/whatsapp-button.js";h.appendChild(s)}</script>-->

<?php endif;?>

<?php if (@$page == 'home'):?>
<script type="text/javascript" src="assets/js/campaign_details.js" ></script>

<?php endif;?>


<?php if (@$page == 'campaign_home'):?>
<script type="text/javascript" src="assets/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/Chart.js"></script>
        <script type="text/javascript" src="assets/js/campaign_home.js" ></script>
<?php endif;?>

<?php if (@$page == 'donation_report'):?>
<script type="text/javascript" src="assets/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/Chart.js"></script>
        <script type="text/javascript" src="assets/js/jquery.dataTables.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" ></script>

        <script src="assets/js/moment.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="assets/js/donation_report.js" ></script>
<?php endif;?>

<!--<script>(function(d,a){function c(){var b=d.createElement("script");b.async=!0;b.type="text/javascript";b.src=a._settings.messengerUrl;b.crossOrigin="anonymous";var c=d.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}window.kayako=a;a.readyQueue=[];a.newEmbedCode=!0;a.ready=function(b){a.readyQueue.push(b)};a._settings={apiUrl:"https://myaidfund.kayako.com/api/v1",teamName:"MyAidFund",homeTitles:[{"locale":"en-us","translation":"Hello! 👋"}],homeSubtitles:[{"locale":"en-us","translation":"Welcome to MyAidFund. Let's chat — start a new conversation below."}],messengerUrl:"https://myaidfund.kayakocdn.com/messenger",realtimeUrl:"wss://kre.kayako.net/socket",widgets:{presence:{enabled:true},twitter:{enabled:false,twitterHandle:"null"},articles:{enabled:false,sectionId:null}},styles:{primaryColor:"#F1703F",homeBackground:"#0aaaa0",homePattern:"https://assets.kayako.com/messenger/pattern-1--dark.svg",homeTextColor:"#FFFFFF"}};window.attachEvent?window.attachEvent("onload",c):window.addEventListener("load",c,!1)})(document,window.kayako||{});</script>-->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5acb2a874b401e45400e769c/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->


<script>
    // Detect JS support
    document.body.className = document.body.className + " js_enabled";
</script>
<script src="assets/js/cookie-message.js"></script>

</body>
</html>