<?php
$page = 'Temrs of Use';
include_once ('header.php');
include ('includes/allfunctions.php');

?>

    <style>
        .container{
            width:95%;
        }
    </style>
    <!-- basic-slider start -->
    <!--<div class="slider-section">
        <div class="slider-active owl-carousel">
            <div class="single-slider slider-screen nrbop bg-black-alfa-40" style="background-image: url(assets/img/slides/team.jpg);">
                <div class="container">
                    <div class="slider-content text-white">
                        <h2 class="b_faddown1 cd-headline clip is-full-width" >Empowering Social Funding</h2>
                        <p class="b_faddown2">Myaidfund.com is the number 1 crowdfunding platform in Ghana and beyond.
                        <div class="slider_button b_faddown3"><a href="#our_mission">Our Mission</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>-->
    <!-- basic-slider end -->

    <!-- Features -->


    <!-- Special Cuase Paralax -->


    <!-- Causes -->



    <!-- team -->
    <div class="team-wrapper" style="padding-top: 0px" id="our_mission">
        <div class="container">
            <div class="section-name one">
                <h2>Terms of Use</h2>

                <div class="" style="text-align: left; padding: 10px 30px 30px 30px">

                    <p>
                        Last Updated: <strong>26<sup>th</sup> May, 2018</strong>
                    </p>
                    <p>
                        <strong>Welcome to Myaidfund.com.</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        Apptechhub Global, Limited. (“<strong>Myaidfund.com</strong>”) maintains
                        this website and its mobile applications and related services
                        (collectively, the “<strong>Myaidfund.com Site</strong>”) as a service to:
                        its visitors; its clients who create websites (“<strong>Client Websites</strong>”) using Myaidfund.com services (“<strong>Services</strong>”) through a separate Services Agreement (“    <strong>Clients</strong>”); and visitors to Client’s Websites, (visitors,
                        Clients, and visitors to Client’s Websites are collectively referred to as,
                        the “<strong>Users</strong>” and individually as “<strong>User</strong>”).
                    </p>
                    <p>
                        Please review the following terms and conditions carefully. This Terms of
                        Use Agreement (the “<strong>Terms of Use</strong>”) is a legally binding
                        contract between each User and Myaidfund.com regarding User’s access to and
                        use of the Myaidfund.com Site and Client’s Websites (collectively, the “    <strong>Sites</strong>”) and any use by User of the Services. By accessing
                        the Sites or Services, the User agrees to these Terms of Use. If a User
                        does not agree with any part of the following Terms of Use, User must not
                        use the Sites or Services.
                    </p>
                    <p>
                        <strong>Definitions</strong>
                    </p>
                    <p>
                        “<strong>Campaign”</strong> means a cause, goal, or event for which a
                        Campaign Organizer seeks donations through the Services.
                    </p>
                    <p>
                        “<strong>Campaign Organizer”</strong> means an individual or organization
                        who creates or manages a Campaign.
                    </p>
                    <p>
                        <strong>“Donor”</strong>
                        means an individual or organization who donates to a Campaign or Campaign
                        Challenge.
                    </p>
                    <p>
                        <strong>“Members”</strong>
                        means Donors, Campaign Organizers, and other users of the Services.
                    </p>
                    <p>
                        <strong>Service Description and Access</strong>
                    </p>
                    <p>
                        The Services are offered as a platform to users of the Services, which may
                        include Campaign Organizers and Donors (each defined herein), entrants to
                        Promotions (defined below) and other registered and unregistered users of
                        the Services (which may include users who simply "like" or "heart"
                        Campaigns or otherwise interact with the Platform or Services). Among other
                        features, the Services are designed to allow a user (a "Campaign
                        Organizer") to post a fundraising campaign ("Campaign") to the Platform to
                        accept monetary donations ("Donations") from those registered users wishing
                        to contribute funds to the Campaign ("Donors"). For purposes hereof, the
                        term "Campaign Organizer" shall also be deemed to include any individual(s)
                        designated as a beneficiary of a Campaign. Although there are no fees to
                        set up a Campaign, a standard payment processing fees apply to each
                        Donation. To learn more about MyaidFund’s Platform and payment processing
                        fees, visit
                        <a href="https://myaidfund.com/pricing.php">
                            Myaidfund.com’s pricing page
                        </a>
                        .
                    </p>
                    <p>
                        <strong>
                            You understand and acknowledge that MyaidFund is not a professional
                            charitable organization, we only provide a Platform as a Service
                            (PaaS); we are not a Broker, Financial Institution, Creditor or
                            Charitable Institution.
                        </strong>
                    </p>
                    <p>
                        The Services are an administrative platform only. MyaidFund facilitates the
                        Donation transaction between Campaign Organizers and Donors, but is not a
                        party to any agreement between a Campaign Organizer and a Donor, or between
                        any user and an NGO. MyaidFund is not a broker, agent, financial
                        institution, creditor or insurer for any user. MyaidFund has no control
                        over the conduct of, or any information provided by, a Campaign Organizer,
                        an NGO or any other user, and MyaidFund hereby disclaims all liability in
                        this regard to the fullest extent permitted by applicable law.
                    </p>
                    <p>
                        All information and content provided by MyaidFund through the Services is
                        for informational purposes only, and MyaidFund does not guarantee the
                        accuracy, completeness, timeliness or reliability of any such information
                        or content. No content is intended to provide financial, legal, tax or
                        other professional advice. Before making any decisions regarding any
                        Campaigns, Charities, Donations, Donors, users or any products, services,
                        information or content relating to the Services, you should consult your
                        financial, legal, tax advisers or other professional advisor as
                        appropriate. You acknowledge that all information and content accessed by
                        you using the Services is at your own risk.
                    </p>
                    <p>
                        MyaidFund does not guarantee that a Campaign or a NGO will obtain a certain
                        amount of Donations or any Donations at all. We do not personally endorse
                        any Campaign, Campaign Organizer, or NGO, and we make no guarantee,
                        explicit or implied, that any information provided through the Services by
                        a user is accurate. We expressly disclaim any liability or responsibility
                        for the success of any Campaign, or the outcome of any fundraising purpose.
                        You, as a Donor, must make the final determination as to the value and
                        appropriateness of contributing to any Campaign, Campaign Organizer, or
                        NGO.
                    </p>
                    <p>
                        All Donations are at your own risk. When you make a Donation to a Campaign
                        or NGO, or otherwise through the Services, it is your responsibility to
                        understand how your money will be used. MyaidFund is not responsible for
                        any offers, promises, rewards or promotions made or offered by Charities,
                        Campaigns or Campaign Organizers. We do not and cannot verify the
                        information that Campaign Organizers supply, nor do we guarantee that the
                        Donations will be used in accordance with any fundraising purpose
                        prescribed by a Campaign Organizer or NGO. We assume no responsibility to
                        verify whether the Donations are used in accordance with any applicable
                        laws; such responsibility rests solely with the Campaign Organizer or NGO,
                        as applicable. While we have no obligation to verify that the use of any
                        funds raised is in accordance with applicable law and these Terms of
                        Service, we take possible fraudulent activity and the misuse of funds
                        raised very seriously. You can learn more about our Fraud Policy at
                        <a href="safety.php">https://MyaidFund.com/safety</a>. If you have reason to believe that a
                        Campaign Organizer or NGO is not raising or using the funds for their
                        stated purpose, please use the <strong>"Report "</strong> button on the
                        Campaign to alert our team of this potential issue and we will investigate.
                    </p>
                    <p>
                        You, as a Campaign Organizer, represent, warrant, and covenant that;
                    </p>
                    <p>
                        <strong>(i) </strong>
                        <strong>
                            all information you provide in connection with a Campaign is accurate,
                            complete, and not otherwise designed to mislead, defraud, or deceive
                            any user;
                        </strong>
                    </p>
                    <p>
                        <strong>(ii) </strong>
                        <strong>
                            all Donations contributed to your Campaign will be used solely as
                            described in the materials that you post or otherwise provide;
                        </strong>
                    </p>
                    <p>
                        (iii)
                        <strong>
                            you will comply with your jurisdiction’s applicable laws and
                            regulations when you solicit funds, particularly, but not limited to,
                            laws relating to your marketing and solicitation for your project;
                        </strong>
                        and
                    </p>
                    <p>
                        <strong>(iv) </strong>
                        <strong>
                            to the extent you share with us any personal data of any third party
                            for any purpose, including the names, email addresses and phone numbers
                            of your personal contacts, you have the authority (including any
                            necessary consents), as required under applicable law, to provide us
                            with such personal data and allow us to use such personal data for the
                            purposes for which you shared it with us. You authorize MyaidFund, and
                            MyaidFund reserves the right to, provide information relating to your
                            Campaign to donors and beneficiaries of your Campaign or law
                            enforcement or to assist in any investigation.
                        </strong>
                    </p>
                    <p>
                        <strong>Registration Obligations</strong>
                    </p>
                    <p>
                        You may be required to register with MyaidFund in order to access and use
                        certain features of the Services. If you choose to register for the
                        Services, you agree to provide and maintain true, accurate, current and
                        complete information about yourself as prompted by the Services'
                        registration form. Campaign Organizers must register using their true
                        identities, including their name and any image or video purporting to
                        depict the Campaign Organizer or the beneficiary of such campaign.
                        Registration data and certain other information about you are governed by
                        our <a href="https://myaidfund.com/privacy_policy.php">Privacy Policy</a>.
                        If you are under 13 years of age (16 in Europe), you are not authorized to
                        use the Services, with or without registering. In addition, if you are
                        under the age of majority in your jurisdiction (typically 18 or 19 years of
                        age), you may use the Services, with or without registering, only with the
                        approval of your parent or guardian. Certain aspects of our Services may
                        also require you to register with, and agree to the terms of, third-party
                        service providers (e.g., payment processors) in order to utilize such
                        Services. For example, Stripes is one of MyaidFund’s payment processing
                        partners. When you use Stripes’s services, your right to use such services
                        is strictly confidential for the purpose you provide in the details of your
                        registration. You may not resell, hire, or on any other basis, allow third
                        parties to use the payment services to enable such third parties to be paid
                        for their services. You may not use the payment services for any different
                        purpose than as registered with our application. If MyaidFund or Stripes at
                        any time discovers that the information you provided about you or the
                        purpose of your campaign is incorrect or changed without informing us or if
                        you violate any of these conditions, the services may be suspended and/or
                        terminated with immediate effect and fines may be applied by the credit
                        card schemes and/or the authorities for unregistered use of payment
                        services which will in such case be payable by you. MyaidFund will assist
                        and support you in your use of the payment processing services to be
                        provided by Stripes (i.e., the processor) and we will provide you with
                        first-line assistance with and enable you to connect to the systems of
                        Stripes to be able to use its services. For this purpose, you understand
                        that you are instructing Stripes to provide MyaidFund with access to your
                        data and setting in Stripes’s systems, which are used by Stripes to provide
                        the services and authorize us to manage these on your behalf. While we may
                        help facilitate such registration in some cases in connection with payment
                        processors with which MyaidFund has partnered, we are not a party to any
                        such relationships and disclaim any responsibility or liability for the
                        performance by such third parties. Our processors may, in their sole
                        discretion, require underwriting on any account, and you agree to cooperate
                        with all underwriting requests. We may exchange information with such
                        third-party services in order to facilitate the provision of Services (and
                        related third party services) as set out in our    <a href="https://myaidfund.com/privacy_policy.php">Privacy Policy</a>.
                    </p>
                    <p>
                        <strong>Registration, Accounts, Passwords and Use.</strong>
                    </p>
                    <p>
                        The Services or Myaidfund.com Site may require registration. Should a User
                        choose to register for such Services, User agrees to provide and maintain
                        true, accurate and current information as required by the relevant
                        registration process, and to promptly update such information as necessary
                        to ensure that it is kept accurate and complete. Further, User agrees to
                        notify Myaidfund.com of any unauthorized use of User’s password or account.
                        Registration data and certain other information about you are governed by
                        our Privacy Policy. User also agrees to sign out from your account at the
                        end of each session when accessing the Sites or Services. Myaidfund.com
                        will not be liable for any loss or damage arising from your failure to
                        comply with this. The Sites and Services are intended for use by
                        individuals 16 years of age or older. If User is under 18, User may use the
                        Site only with involvement of a parent or legal guardian who agrees to be
                        bound by these Terms of Use.
                    </p>
                    <p>
                        <strong> Additional Terms</strong>
                    </p>
                    <p>
                        Some of Myaidfund.com’s Services have additional terms and conditions (“    <strong>Additional Terms</strong>”). Where Additional Terms apply to a
                        Service, Myaidfund.com will make them available for User to read through
                        prior to User’s use of that Service. By using such Services, User agrees to
                        the applicable Additional Terms.
                    </p>
                    <p>
                        <strong>Ownership.</strong>
                    </p>
                    <p>
                        Except for User Content, the Myaidfund.com Site is the property of
                        Apptechhub Global Inc. and Apptechhub Global Limited, its licensors and/or
                        its various third party providers and distributors. The Myaidfund.com Site
                        is protected by copyright, trademark and other intellectual property laws.
                        User must retain all copyright and trademark notices, including any other
                        proprietary notices, contained in the materials, and User agrees not to
                        alter, obscure or obliterate any of such notices. Unless otherwise set
                        forth in these Terms of Use, none of the content or data found in the
                        Myaidfund.com Site may be copied, reproduced, republished, sold,
                        transferred, modified or distributed in any way without the prior written
                        consent of Myaidfund.com, its licensors and/or its third party providers
                        and distributors. Trademarks, logos, images and service marks displayed on
                        the Myaidfund.com Site are the property of either Myaidfund.com or other
                        third parties. Users agree not to display or use any Myaidfund.com or third
                        party marks without prior written consent of Myaidfund.com or the
                        appropriate third party licensor. Myaidfund.com reserves all rights not
                        expressly granted to User.
                    </p>
                    <p>
                        <strong>Privacy.</strong>
                    </p>
                    <p>
                        For information on how User information is collected, used and disclosed by
                        Myaidfund.com, please see the    <a href="http://fundly.com/privacy-policy/">Privacy Policy</a>.
                    </p>
                    <p>
                        <strong>Change of Terms of Use.</strong>
                    </p>
                    <p>
                        From time to time Myaidfund.com may revise or modify these Terms of Use by
                        posting the revised Terms of Use through a link on the Myaidfund.com Site.
                        Those changes will go into effect on the “Last Updated” date shown in the
                        revised Terms of Use. By continuing to access or use the Site or Services,
                        User agrees to the revised Terms. If User does not agree to any of the
                        Terms of Use set forth herein, User may not use the Sites and Services.
                    </p>
                    <p>
                        <strong>User Content.</strong>
                    </p>
                    <p>
                        The Myaidfund.com Site may allow Users to post or send information, text,
                        data, photographs, audio, video, images, graphics, logos, trademarks,
                        service marks, and other materials (collectively “    <strong>User Content</strong>”) to or through the Sites. User understands
                        that all of the User Content and the consequences of such posting User
                        Content is User’s sole responsibility and is subject to the Terms of Use
                        and the Myaidfund.com Privacy Policy.
                    </p>
                    <p>
                        User understands that by accessing the Sites or Services, User may be
                        exposed to User Content that may be offensive or objectionable to User.
                        Myaidfund.com may, but is not obligated to, preview or review any User
                        Content and, in its sole discretion, may block or may remove from the
                        Sites, without notice, any User Content that violates the Terms of Use or
                        is otherwise objectionable. However, failure to block or remove any User
                        Content is not an endorsement, warranty, representation, or guarantee
                        regarding such User Content.
                    </p>
                    <p>
                        User represents and warrants that the User Content does not infringe the
                        copyright, trademark, publicity/privacy right or other intellectual
                        property or proprietary right of any third party and that User has paid and
                        will pay in full any fees or other payments that may be related to the use
                        of User’s User Content.
                    </p>
                    <p>
                        By posting, uploading or otherwise distributing the User Content on the
                        Myaidfund.com Site, User hereby grants (and represent and warrant that User
                        has all necessary rights to grant) to Myaidfund.com a perpetual,
                        sub-licensable, transferable, world-wide, non-exclusive, royalty free,
                        fully paid license to all User’s rights in the User Content (including
                        moral rights) for all purposes, including but not limited to use, copy,
                        reproduce, modify, adapt, publish, edit, translate, create derivative works
                        from, transmit, distribute, display, perform, or derive revenue or other
                        remuneration from such User Content and incorporate such User Content into
                        other works in any form, media or technology.
                    </p>
                    <p>
                        By providing User Content to the Sites or Services, User hereby permits
                        Myaidfund.com to identify User as the provider of such User Content in any
                        form, media and technology.
                    </p>
                    <p>
                        User agrees that Myaidfund.com has no liability or responsibility for the
                        storage or deletion of any User Content that User submits or posts through
                        the Sites or Services.
                    </p>
                    <p>
                        User acknowledges and agrees that any questions, comments, suggestions,
                        ideas, feedback or other information about the Sites or Services provided
                        by User to Myaidfund.com are non-confidential and Myaidfund.com will be
                        entitled to the unrestricted use and dissemination of these Submissions for
                        any purpose, commercial or otherwise, without acknowledgement or
                        compensation to User.
                    </p>
                    <p>
                        User further acknowledges and agrees that Myaidfund may preserve content
                        and may also disclose content if required to do so by law in the good-faith
                        belief that such preservation or disclosure is reasonably necessary to (a)
                        comply with legal process, applicable laws or government requests; (b)
                        enforce these Terms of Use; (c) respond to claims that any content violates
                        the rights of third parties; or (d) protect the rights, property, or
                        personal safety of Myaidfund.com, Users or the public.
                    </p>
                    <p>
                        <strong>Rules and Conduct.</strong>
                    </p>
                    <p>
                        Any information; ideas or opinions posted by Users of the Service or
                        Myaidfund.com Site does not necessarily reflect Myaidfund.com’s views.
                        Myaidfund.com does not assume responsibility for the accuracy of any
                        information, ideas or opinions posted by Users and are not liable for any
                        claims, damages or losses resulting from such information, ideas or
                        opinions. When posting any information, materials or content or otherwise
                        accessing the Sites or Services, User agrees that without limiting the
                        foregoing, User will not:
                    </p>
                    <p>
                        · Harass, defame, intimidate or threaten another user;
                    </p>
                    <p>
                        · Interfere with another user’s rights to privacy;
                    </p>
                    <p>
                        · Post any material that is defamatory (i.e., disparaging to the reputation
                        of an individual or business);
                    </p>
                    <p>
                        · Post any material that is obscene, offensive or indecent;
                    </p>
                    <p>
                        · Post any trademarks, logos, copyrighted material or other intellectual
                        property without the authorization of the owner;
                    </p>
                    <p>
                        · Operate, conduct, or promote, directly or indirectly, raffles, lotteries
                        or other similar gaming activities, whether for charitable purposes or
                        otherwise;
                    </p>
                    <p>
                        · Post any materials that may damage the operation of a computer (such as a
                        virus, worm or Trojan horse);
                    </p>
                    <p>
                        · Use any means to “scrape,” “crawl,” or “spider” any web pages contained
                        in the Sites (although Myaidfund.com does allow operators of public search
                        engines to use spiders to index materials from the Site for the sole
                        purpose of creating publicly available searchable indices of the materials,
                        but not caches or archives of such materials, and Myaidfund.com reserves
                        the right to revoke these exceptions either generally or in specific
                        cases);
                    </p>
                    <p>
                        · Recruit or otherwise solicit any User to join third party services or
                        websites that are competitive to Myaidfund.com, without Myaidfund.com’s
                        prior written approval;
                    </p>
                    <p>
                        · Use, display, mirror, or frame the Sites or any individual element within
                        the Sites or Services, Myaidfund.com’s name, any Myaidfund.com trademark,
                        logo, or other proprietary information, or the layout and design of any
                        page or form contained on a page, without Myaidfund.com’s express written
                        consent;
                    </p>
                    <p>
                        · Access, tamper with, or use non-public areas of the Sites,
                        Myaidfund.com’s computer systems, or the technical delivery systems of
                        Myaidfund.com’s providers;
                    </p>
                    <p>
                        · Attempt to probe, scan, or test the vulnerability of any Myaidfund.com
                        system or network or breach any security or authentication measures;
                    </p>
                    <p>
                        · Avoid, bypass, remove, deactivate, impair, descramble, or otherwise
                        circumvent any technological measure implemented by Myaidfund.com or any of
                        Myaidfund.com’s providers or any other third party (including another user)
                        to protect the Sites or Services; or
                    </p>
                    <p>
                        · Attempt to decipher, decompile, disassemble, or reverse engineer any of
                        the software used to provide the Sites or Services.
                    </p>
                    <p>
                        · Engage in any other activity that Myaidfund.com may deem in its sole
                        discretion to be unacceptable.
                    </p>
                    <p>
                        User must at all times use the Sites and the Services in a responsible and
                        legal manner. In particular (but not exclusively) User must not do any of
                        the following: misrepresent User’s identity or User’s affiliation with any
                        other person or organization; send junk email or spam to people who do not
                        wish to receive mail from User; delete or falsify any attributions,
                        trademarks or designations of source from any website content; or interfere
                        with or disrupt the service or services or networks connected to the
                        Services or Sites; collect or store personal data about other users
                        including email addresses without consent. User agrees that User will
                        comply with all applicable statutes and regulations regarding use of the
                        Sites and Services.
                    </p>
                    <p>
                        Myaidfund.com will investigate and prosecute violations of any of the above
                        to the fullest extent of the law. Myaidfund.com may involve and cooperate
                        with law enforcement authorities in prosecuting Users who violate these
                        Terms of Use.
                    </p>
                    <p>
                        <strong>Copyright Policy</strong>
                    </p>
                    <p>
                        <em>Notice of Copyright Infringement</em>
                    </p>
                    <p>
                        Myaidfund.com respects the intellectual property rights of others. Please
                        notify Myaidfund.com in writing, by e-mail or mail to Myaidfund.com’s
                        designated agent listed below, if you believe that a User has infringed
                        your intellectual property rights. Notify Apptechhub Global Inc or
                        Apptechhub Global Limited, property owners of Myaidfund.com.
                    </p>
                    <p>
                        To be effective the notification should include:
                    </p>
                    <p>
                        · identification of the copyrighted work claimed to have been infringed,
                        or, if multiple copyrighted works are covered by a single notification, a
                        representative list of such works;
                    </p>
                    <p>
                        · identification of the claimed infringing material and information
                        reasonably sufficient to permit Myaidfund.com to locate the material on the
                        Sites or Services;
                    </p>
                    <p>
                        · information reasonably sufficient to permit Myaidfund.com to contact you,
                        such as an address, telephone number, and, if available, an e-mail address;
                    </p>
                    <p>
                        · a statement by you that you have a good faith belief that the disputed
                        use is not authorized by the copyright owner, its agent, or the law;
                    </p>
                    <p>
                        · a statement by you, made under penalty of perjury, that the above
                        information in your notification is accurate and that you are the copyright
                        owner or authorized to act on the copyright owner’s behalf; and
                    </p>
                    <p>
                        · Your physical or electronic signature.
                    </p>
                    <p>
                        You acknowledge and agree that upon receipt and notice of a claim of
                        infringement, Myaidfund.com may immediately remove the identified materials
                        from the Sites and Services without liability.
                        <br/>
                        <br/>
                    </p>
                    <p>
                        <em>Repeat Infringers</em>
                    </p>
                    <p>
                        User’s account will be terminated if, in Myaidfund.com’s discretion, User’s
                        determined to be a repeat infringer. Repeat infringers are Users who have
                        been the subject of more than one valid takedown request that has not been
                        successfully rebutted.
                    </p>
                    <p>
                        <strong>Pricing</strong>
                    </p>
                    <p>
                        For information about Myaidfund.com’s pricing, please see    <a href="http://myaidfund.com/pricing/">Myaidfund.com’s pricing page</a>.
                        Myaidfund.com reserves the right to change its pricing at any time, with or
                        without notice. Myaidfund.com will post any changes to the Pricing on the
                        Pricing page, so please check often. All fees are exclusive of all taxes,
                        levies, or duties imposed by taxing authorities, and User shall be
                        responsible for payment of all taxes, levies, or duties associated with
                        User’s purchases hereunder.
                    </p>
                    <p>
                        <strong>Taxes</strong>
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        It is User’s responsibility to determine what, if any, taxes apply to
                        donations received through the use of the Services or Sites. It is solely
                        User’s responsibility to assess, collect, report or remit the correct tax,
                        if any, to the appropriate tax authority.
                    </p>
                    <p>
                        <strong>Dealing with Third Parties.</strong>
                    </p>
                    <p>
                        The Myaidfund.com Site may provide links to or frame third-party websites
                        or services and may link User automatically to sponsors’ or third party’s
                        websites or services. Myaidfund.com provides such links and framing solely
                        for the convenience of Users. Myaidfund.com does not review or endorse, and
                        are not responsible for, any content, advertising, products, services or
                        other materials on or available from such websites or services. User
                        assumes full responsibility for User’s use of third-party websites or
                        services. User’s interactions with organizations or individuals found on or
                        through the Sites and Services are solely between User and such
                        organizations or individuals. User should make whatever investigation User
                        feels necessary or appropriate before proceeding with any interaction with
                        any of these third parties. User agree that Myaidfund.com is not
                        responsible or liable for any loss or damage of any kind or nature incurred
                        as the result of any such dealings. If there is a dispute between Users, or
                        between Users and any third party, User understands and agrees that
                        Myaidfund.com is under no obligation to become involved. In the event that
                        User has a dispute with any other User, User hereby releases Myaidfund.com
                        and its affiliates, and their officers, employees, agents, and successors
                        from claims, demands, and damages (actual and consequential) of every kind
                        or nature, known and unknown, suspected and unsuspected, disclosed and
                        undisclosed, arising out of or in any way related to such disputes or the
                        Sites and Services. A general release does not extend to claims which the
                        creditor does not know or suspect to exist in his favor at the time of
                        executing the release, which, if known by him must have materially affected
                        his settlement with the debtor.
                    </p>
                    <p>
                        <strong>Payment Processing Services</strong>
                    </p>
                    <p>
                        Payment processing services are provided by Stripe ((    <a href="https://stripe.com/">https://stripe.com/</a>), Mobile money
                        wallets through Hubtel (https://hubtel.com/) in Ghana (MTN, Airtel, Tigo
                        and Vodafone) and the terms and conditions of those services will apply to
                        the payments you make using them. Any personal information you provide to
                        these payment providers will be processed in accordance with their privacy
                        policies and not ours. Please see our    <a href="https://myaidfund.com/privacy_policy.php">Privacy Policy</a> for
                        further details regarding your personal information and the third party
                        service providers that we use.
                    </p>
                    <p>
                        <strong></strong>
                    </p>
                    <p>
                        <strong>Exclusion of Warranty.</strong>
                    </p>
                    <p>
                        USER USES THE SITES AND SERVICES AT USER’S OWN RISK. THE SITES, SERVICES,
                        AND THE CONTENTS THEREIN AND/OR THE CONTENT PROVIDED BY MYAIDFUND.COM.COM,
                        ARE PROVIDED “AS IS” AND “AS AVAILABLE”, AND, TO THE FULLEST EXTENT
                        PERMITTED BY APPLICABLE LAW, ALL WARRANTIES, EXPRESS OR IMPLIED, ARE
                        DISCLAIMED, INCLUDING BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF
                        MERCHANTABILITY, QUIET ENJOYMENT, NON-INFRINGEMENT AND FITNESS FOR A
                        PARTICULAR PURPOSE.
                    </p>
                    <p>
                        IN PARTICULAR, MYAIDFUND.COM AND ITS OFFICERS, EMPLOYEES, DIRECTORS,
                        SHAREHOLDERS, PARENTS, SUBSIDIARIES, AFFILIATES, AGENTS, AND LICENSORS
                        (REFERRED TO COLLECTIVELY AS “<strong>AFFILIATES</strong>”) MAKE NO
                        REPRESENTATIONS OR WARRANTIES ABOUT THE ACCURACY OR COMPLETENESS OF CONTENT
                        AVAILABLE ON OR THROUGH THE SITES OR SERVICES (INCLUDING, WITHOUT
                        LIMITATION, any recommendations or other content available on or through
                        the Sites or Services), OR THE CONTENT OF ANY WEBSITES OR RESOURCES LINKED
                        TO OR FRAMED ON THE SITES OR SERVICES. MYAIDFUND.COM AND ITS AFFILIATES
                        WILL HAVE NO LIABILITY FOR ANY: (a) ERRORS, MISTAKES, OR INACCURACIES OF
                        CONTENT; (b) PERSONAL INJURY OR PROPERTY DAMAGE RESULTING FROM USER’S
                        ACCESS TO OR USE OF THE SITES OR SERVICES; (c) ANY UNAUTHORIZED ACCESS TO
                        OR USE OF MYAIDFUND.COM’S SERVERS OR OF ANY PERSONAL OR FINANCIAL
                        INFORMATION; (d) ANY INTERRUPTION OF TRANSMISSION TO OR FROM THE SITES OR
                        SERVICES; (e) ANY COMPUTER VIRUSES OR MALICIOUS CODE THAT MAY BE
                        TRANSMITTED ON OR THROUGH THE SITES OR SERVICES; OR (f) ANY LOSS OR DAMAGE
                        OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY FUNCTIONALITY OR CONTENT
                        POSTED, E-MAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE ON OR THROUGH
                        THE SITES OR SERVICES.
                    </p>
                    <p>
                        MYAIDFUND.COM AND ITS AFFILIATES DO NOT WARRANT, ENDORSE, GUARANTEE, OR
                        ASSUME RESPONSIBILITY FOR ANY THIRD PARTY PRODUCT OR SERVICE RECOMMENDED,
                        ADVERTISED, OR OFFERED ON OR THROUGH THE SITES OR SERVICES OR ANY LINKED OR
                        FRAMED WEBSITE.
                    </p>
                    <p>
                        USER UNDERSTANDS AND AGREES THAT ANY MATERIAL OR INFORMATION DOWNLOADED OR
                        OTHERWISE OBTAINED THROUGH THE USE OF THE SITES OR SERVICES IS DONE AT
                        USER’S OWN RISK AND THAT USER WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE
                        ARISING FROM DOING SO. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN,
                        OBTAINED BY USER FROM MYAIDFUND.COM OR THROUGH THE SITES OR SERVICES WILL
                        CREATE ANY WARRANTY NOT EXPRESSLY MADE IN THESE TERMS OF USE.
                    </p>
                    <p>
                        <strong>Limitation of Liability.</strong>
                    </p>
                    <p>
                        TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, NEITHER MYAIDFUND.COM
                        NOR ITS AFFILIATES WILL BE LIABLE UNDER ANY THEORY OF LIABILITY (WHETHER IN
                        CONTRACT, TORT, NEGLIGENCE, OR OTHERWISE) FOR ANY INDIRECT, INCIDENTAL,
                        SPECIAL, CONSEQUENTIAL, PUNITIVE, OR EXEMPLARY DAMAGES, INCLUDING, DAMAGES
                        FOR LOSS OF REVENUES, PROFITS, GOODWILL, USE, DATA, OR OTHER INTANGIBLE
                        LOSSES (EVEN IF SUCH PARTIES WERE ADVISED OF, KNEW OF, OR SHOULD HAVE KNOWN
                        OF THE POSSIBILITY OF SUCH DAMAGES), ARISING FROM OR RELATING TO THE SITES
                        OR SERVICES OR THESE TERMS OF USE.
                    </p>
                    <p>
                        Some jurisdictions do not allow the exclusion of certain warranties or the
                        limitation or exclusion of liability for incidental or consequential
                        damages. Accordingly, some of the above limitations and disclaimers may not
                        apply to User. To the extent Myaidfund.com may not, as a matter of
                        applicable law, disclaim any implied warranty or limit its liabilities, the
                        scope and duration of such warranty and the extent of Myaidfund.com’s
                        liability will be the minimum permitted under such law<em>.</em>
                    </p>
                    <p>
                        <strong>Indemnification.</strong>
                    </p>
                    <p>
                        Users are entirely responsible for maintaining the confidentiality of
                        passwords and accounts. Furthermore, Users are entirely responsible for any
                        and all activities that occur under their account. User agrees to
                        indemnify, defend and hold harmless Myaidfund.com and its Affiliates from
                        and against any and all claims, liabilities, damages, losses, costs,
                        expenses, and fees of any kind (including reasonable attorneys’ fees and
                        legal costs), arising from or relating to: (a) any information (including
                        User’s User Content or any other content) that User or anyone using User’s
                        account submits, posts, or transmits on or through the Sites or Services;
                        (b) the use of the Sites or Services by User or anyone using User’s
                        account; (c) the violation of these Terms of Use by User or anyone using
                        User’s account; or (d) the violation of any rights of any third party,
                        including intellectual property, privacy, publicity, or other proprietary
                        rights by User or anyone using User’s account. Myaidfund.com reserves the
                        right, at its own expense, to assume the exclusive defense and control of
                        any matter otherwise subject to indemnification by User. If Myaidfund.com
                        does assume the defense of such a matter, User will reasonably cooperate
                        with Myaidfund.com in such defense. User agrees to immediately notify
                        Myaidfund.com of any unauthorized use of User’s account or any other breach
                        of security known to User.
                    </p>
                    <p>
                        <strong>International Use.</strong>
                    </p>
                    <p>
                        The Sites and Services are controlled and operated from within Ghana.
                        Myaidfund.com makes no representation that the Sites and Services will
                        comply with laws of locations outside of the Ghana. Those who choose to
                        access the Sites or Services from other locations do so at their own risk
                        and are responsible for compliance with applicable laws.
                    </p>
                    <p>
                        <strong>Governing Law and Jurisdiction</strong>
                    </p>
                    <p>
                        By accessing or using the Sites or Services, User agrees: (i) that any and
                        all disputes User may have with, or claims User may have against
                        Myaidfund.com relating to, arising out of or connected in any way with (a)
                        the Sites or Services, (b) these Terms of Use, or (c) the determination of
                        the scope or applicability of this agreement to arbitrate (a “    <strong>Claim</strong>”), will be governed by and construed in accordance
                        with the law of Ghana. Use and Myaidfund.com will both attempt to settle it
                        by mediation in accordance with the Centre for Dispute Resolution (CEDR)
                        Model Mediation Procedure. If either or both User and Myaidfund.com refuse
                        to initiate the mediation procedure within 30 days of the dispute arising
                        or if they both fail to agree terms of settlement within a further 40 days
                        of the initiation of the procedure, either User or Myaidfund.com will be
                        free to initiate proceedings in the courts of Ghana which will, subject to
                        paragraph have exclusive jurisdiction to deal with such dispute.
                    </p>
                    <p>
                        If any provision of this Section is found to be invalid or unenforceable,
                        then that specific provision shall be of no force and effect and shall be
                        severed, but the remainder of this Section will continue in full force and
                        effect. This Section of these Terms of Use will survive the termination of
                        User’s relationship with Myaidfund.com.
                    </p>
                    <p>
                        <strong>Termination.</strong>
                    </p>
                    <p>
                        Myaidfund.com reserves the right, at its sole discretion, to immediately,
                        with or without notice, suspend or terminate User’s registration, the Terms
                        of Use, and/or User’s access to all or a portion of the Sites or Services
                        and/or remove any registration information or User Content from the Sites
                        or Services, for any reason. Upon termination or expiration of the Terms of
                        Use, User’s obligations and Myaidfund.com’s rights and disclaimers survive,
                        but User’s right to use the Sites and Services immediately ceases.
                    </p>
                    <p>
                        <strong>General</strong>
                    </p>
                    <p>
                        These Terms of Use constitute the entire agreement between you and
                        Myfundaid.com and govern your use of the Sites and Services, superseding
                        any prior agreements between you and Myfaundaid.com with respect to the
                        Sites and Services. You also may be subject to additional terms and
                        conditions that may apply when you use affiliate or third-party services,
                        third-party content or third-party software. You agree that regardless of
                        any statute or law to the contrary, any claim or cause of action arising
                        out of or related to use of the Sites and Services or these Terms of
                        Service must be filed within one (1) year after such claim or cause of
                        action arose or be forever barred. A printed version of this agreement and
                        of any notice given in electronic form will be admissible in judicial or
                        administrative proceedings based upon or relating to this agreement to the
                        same extent and subject to the same conditions as other business documents
                        and records originally generated and maintained in printed form. You may
                        not assign these Terms of Service without the prior written consent of
                        Myfundaid.com, but Myfundaid.com may assign or transfer these Terms of
                        Service, in whole or in part, without restriction. The section titles in
                        these Terms of Service are for convenience only and have no legal or
                        contractual effect.
                    </p>
                    <p>
                        © 2018 Myaidfund.com. All rights reserved.
                    </p>

                </div>
            </div>


        </div>
    </div>





    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>