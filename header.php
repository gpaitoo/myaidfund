<?php

if (session_status() == PHP_SESSION_NONE) {
	session_start();
	if (!isset($_SESSION['identity'])) {
		$_SESSION['identity'] = uniqid();
	}
}
if (!isset($_SESSION['id']) && (@$page != "home" && @$page != "campaign_details" && @$page != 'register_campaign' && @$page != 'Terms and Conditions'
		 && @$page != 'About Us' && @$page != 'How it Works' && @$page != 'Pricing' && @$page != 'Privacy Policy' && @$page != 'Contact Us'
        && @$page != 'Temrs of Use' && @$page != 'Activation' && @$page != 'FAQ' && @$page != 'Safety')) {
	header('location:index.php');
}
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="854778400919-0fb9rj06t4o1r8q08nbjpghs7un67igc.apps.googleusercontent.com">
<?php echo @$meta;?>
<title>MyAidFund</title>
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png" />
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/owl-carousel/assets/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/animate.css">
	<link rel="stylesheet" href="assets/css/meanmenu.css">
	<link rel="stylesheet" href="assets/css/nivo-lightbox.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/formoid-metro-cyan.css">
	<link href="assets/js/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
	<!-- <link href="assets/css/croppie.css" type="text/css" /> -->
	<link href="assets/css/h5upload.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/css/bootstrap-dialog.min.css" type="text/css" rel="stylesheet"/>

<?php if (@$page == 'Terms and Conditions'):?>
    <?php endif;?>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<?php if (@$page == 'campaign_details'):?>
<script src="https://checkout.stripe.com/checkout.js"></script>
<?php endif;?>

<?php if (@$page == 'home'):?>
<script src="https://checkout.stripe.com/checkout.js"></script>
<?php endif;?>


<?php if (@$page == 'donation_report'):?>
<link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
<link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" type="text/css">
<link href="assets/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"/>
<link rel="stylesheet" href="assets/js/bootstrap-daterangepicker/daterangepicker.min.css">
<?endif;?>
<!-- Load Facebook SDK for JavaScript -->
<!--    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
 js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10&appId=1997854547143864';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>-->


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119774724-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-119774724-1');
    </script>


</head>
<body>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1997854547143864',
            xfbml      : true,
            version    : 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s);
 js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

	<!-- header -->

<div id="cookie-message">
<!--    <p>
        Your cookie message goes here
    </p>-->
</div>
	<header style="position:static;z-index: 100">
		<div id="search-bar">
			<div class="container">
				<div class="row">
					<form action="#" name="search" class="col-xs-12">
						<input type="text" name="search" placeholder="Type and Hit Enter"><i id="search-close" class="fa fa-close"></i>
					</form>
				</div>
			</div>
		</div>
		<nav  class="navigation">
			<div class="container">
				<div class="row">
					<div class="logo-wrap col-md-4 col-xs-6">
						<h1 ><a href="index.php"><img src="assets/img/logo.png" width="60px"/><span style="min-width:400px;" class="hidden-xs">MyAidFund</span></a></h1>
                        <h3>
                            <?php
                                    if (isset($_SESSION['d_id'])){
                                        $n = $_SESSION['d_name'];
                                        if(strlen($n)>10){
                                            echo substr($n,0,7).'...';
                                        }else{
                                            echo $n;
                                        }
                                    }
                            ?>
                        </h3>
					</div>
					<div class="menu-wrap col-md-8 ">
						<ul class="menu">
							<li class="active">
								<a href="index.php" >Home</a>
							</li>
							<!-- <li>
								<a href="causes-grid.html">Causes</a>

								<span>Causes</span>
								<ul class="submenu">
									<li><a href="causes-grid.html">Causes Grid</a></li>
									<li><a href="causes-list.html">Causes List</a></li>
									<li><a href="causes-single.html" class="active">Causes Single</a></li>
								</ul>
							</li> -->

<?php
if (isset($_SESSION['id'])) {
	echo '<li><a href="campaign_home.php" >Dashboard</a></li>';
} else {
	echo '<li><a href="register_campaign.php">Create Campaign</a></li>';
}
?>

<?php
if (isset($_SESSION['id']) || isset($_SESSION['d_id'])) {
	echo '<li><a href="process/logout.php" onclick="fbLogoutUser(); gl_logout();">LOGOUT</a></li>';
} else {
	echo '<li><a href="#"  data-toggle="modal" data-target="#login-modal">LOGIN</a></li>';
}
?>
<!-- <li>
								<a href="gallery.html">gallery</a>
							</li>
							<li>
								<a href="about-us.html">About</a>
							</li>
							<li>
								<span>Blog</span>
								<ul class="submenu">
									<li><a href="blog-list.html">Blog List</a></li>
									<li><a href="blog-single.html">Blog Single</a></li>
								</ul>
							</li>
							<li>
								<a href="contact-us.html">Contact</a>
							</li> -->
						</ul>
					</div>
					<!-- <div class="col-md-1 col-xs-6">
						<div id="search-toggle">
							<i class="fa fa-search"></i>
						</div>
					</div> -->
				</div>
			</div>

			<!--[ MOBILE-MENU-AREA START ]-->
			<div class="mobile-menu-area">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="mobile-area">
								<div class="mobile-menu">
									<nav id="mobile-nav">
										<ul>
											<li><a href="index.php">Home </a></li>
<?php
if (isset($_SESSION['id'])) {
	echo '<li><a href="campaign_home.php" >Dashboard</a></li>';
} else {
	echo '<li><a href="register_campaign.php">Create Campaign</a></li>';
}
?>

<?php
if (isset($_SESSION['id']) || isset($_SESSION['d_id'])) {
	echo '<li><a href="process/logout.php" onclick="fbLogoutUser(); gl_logout();" >LOGOUT</a></li>';
} else {
	echo '<li><a onclick="$(\'.meanmenu-reveal\').click();" href="#"  data-toggle="modal" data-target="#login-modal">LOGIN</a></li>';
}
?>
</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--[ MOBILE-MENU-AREA END  ]-->
		</nav>
	</header>

<?php
if(isset($_SESSION['d_id'])){
    echo "<input type='hidden' value='true' id='dlogin'/>";
}
?>


