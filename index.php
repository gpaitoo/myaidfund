<?php
$page = 'home';
include_once ('header.php');
include ('includes/allfunctions.php');
$ipinfo = ip_info("Visitor", "Country");
//insert_and_process_transaction_report('9','paypal','5','1234567890');
?>

<style>
    .container{
        width:95%;
    }
</style>
<!-- basic-slider start -->
	<div class="slider-section">
		<div class="slider-active owl-carousel">
			<div class="single-slider slider-screen nrbop bg-black-alfa-40" style="background-image: url(assets/img/slides/sd1.jpg);">
				<div class="container">
					<div class="slider-content text-white">
						<h2 class="b_faddown1 cd-headline clip is-full-width" >Start Your Campaign </h2>
						<p class="b_faddown2">MyAidFund Gives You The Best In Crowd Fundraising.
						<br />Online Fundraising Has Never Been Easier. Get started and accept online donations instantly </p>
						<div class="slider_button b_faddown3"><a href="register_campaign.php">Start Campaign</a></div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- basic-slider end -->

	<!-- Features -->
<!--	<div class="features-wrapper one">
		<div class="container">
			<div class="section-name one">
				<h2>What We Do</h2>
				<div class="short-text">
					<h5>Here is all Reasons to Work With Us</h5>
				</div>
			</div>
			<div class="row features">
				<div class="col-md-4 col-sm-6 ">
					<div class="feature clearfix">
						<div class="icon_we"><i class="fa fa-handshake-o"></i></div>
						<h4>Food Delivering</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, maiores officia placeat incidunt aperiam</p>
						<a href="#" class="btn btn-min btn-secondary
						"><span>Learn More</span></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature  higlight clearfix">
						<div class="icon_we"><i class="fa fa-medkit" aria-hidden="true"></i></div>
						<h4> Aids For Health </h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, maiores officia placeat incidunt aperiam</p>
						<a href="#" class="btn btn-min btn-secondary
						"><span>Learn More</span></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 hidden-sm ">
					<div class="feature clearfix">
						<div class="icon_we"><i class="fa fa-book" aria-hidden="true"></i></div>
						<h4>Build Education</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, maiores officia placeat incidunt aperiam</p>
						<a href="#" class="btn btn-min btn-secondary
						"><span>Learn More</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>-->

	<!-- Special Cuase Paralax -->
<?php
$sp = get_special_cause();
$sp_amount_achieved = get_donation_sum($sp->id);
$crst = determine_donation_currency($sp->id);
$fcurrency='$';
if($crst == 'USD'){
    $fcurrency = '$';
}else{
    $fcurrency = 'GHS';
}
?>
	<div class="special-cause">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12 donet__area_img">
                    <a href="campaign_details.php?donate_id=<?php echo $sp->id ?>"><img style="width:100%" src="<?php echo $sp->top_img;?>" alt="" /></a>
				</div>
				<div class="col-md-6 col-xs-12 donet__area">
					<div class="section-name parallax one">
						<h1>special cause Right Now</h1>
						<h2><a href="campaign_details.php?donate_id=<?php echo $sp->id ?>"><?php echo $sp->title;?></a></h2>
						<h4><?php echo strlen($sp->story) > 200?substr($sp->story, 0, 200)."...":$sp->story;?>

						</h4>
					</div>
					<div class="foundings">
						<div class="progress-bar-wrapper big">
							<div class="progress-bar-outer">
								<div class="clearfix">
									<span class="value one">Raised: <?php echo $fcurrency.number_format($sp_amount_achieved)?></span>
									<span class="value two">- To go: <?php echo $fcurrency; echo number_format($sp->amount_goal-$sp_amount_achieved)?></span>
								</div>
								<div class="progress-bar-inner">
									<div class="progress-bar">
										<span data-percent="<?php echo (($sp_amount_achieved/$sp->amount_goal)*100)?>"> <span class="pretng"><?php echo round((($sp_amount_achieved/$sp->amount_goal)*100),2)?>%</span> </span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="btns-wrapper">
						<a href="#" data-toggle="modal" data-target="#payoptions" data-title="<?php echo $sp->title ?>" data-donation-id="<?php echo $sp->id ?>" class="btn btn-big btn-solid "><i class="fa fa-archive"></i><span>Make Donation</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Causes -->

	<div class="causes-wrapper">
		<div class="container">
			<div class="section-name one">
				<h2>Recent Cause</h2>
				<div class="short-text">
					<h5>Your little support can bring a simile to someone</h5>
				</div>
			</div>
			<div class="causes">
				<div class="causes-list row">
<?php
$cs = get_three_cause();


while ($row = $cs->fetch_object()) {
    $r_amount_achieved = get_donation_sum($row->id);
    $crst = determine_donation_currency($row->id);
    $currency='$';
    if($crst == 'USD'){
        $currency = '$';
    }else{
        $currency = 'GHS';
    }

	?>
								<div class="cause-wrapper col-md-4 col-xs-12 col-sm-6 legal health">
									<div class="cause content-box">
										<div class="img-wrapper">
                                            <a href="campaign_details.php?donate_id=<?php echo $row->id ?>">
											<div class="overlay">
											</div>
											<img class="img-responsive" src="<?php echo $row->top_img;?>" alt="">
                                            </a>
										</div>
										<div class="info-block">
                                            <a href="campaign_details.php?donate_id=<?php echo $row->id ?>"><h4><?php echo $row->title?></h4></a>
											<p><?php echo strlen($row->brief) > 200?substr($row->brief, 0, 200)."...":$row->brief;?></p>
											<div class="foundings">
												<div class="progress-bar-wrapper min">
													<div class="progress-bar-outer">
														<p class="values"><span class="value one">Raised: <?php echo $currency.number_format($r_amount_achieved);?></span>-<span class="value two">To go: <?php echo $currency; echo number_format($row->amount_goal-$r_amount_achieved)?></span></p>
														<div class="progress-bar-inner">
															<div class="progress-bar">
																<span data-percent="<?php echo (($r_amount_achieved/$row->amount_goal)*100)?>"><span class="pretng"><?php echo round((($r_amount_achieved/$row->amount_goal)*100),2)?>%</span> </span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="donet_btn">
												<a data-toggle="modal" data-target="#payoptions" data-title="<?php echo $sp->title ?>" data-donation-id="<?php echo $row->id ?>" href="#" class="btn btn-min btn-solid"><i class="fa fa-archive"></i><span>Donate Now</span></a>
											</div>
										</div>
									</div>
								</div>
	<?php }?>
</div>
			</div>
		</div>
	</div>


    <!-- team -->
    <div class="team-wrapper">
        <div class="container">
            <div class="section-name one">
                <h2>Categories</h2>

                <div class="tags">
                    <a href="campaigns.php?catg=community"><div class="btn btn-solid">Clubs &amp; Community</div></a>
                    <a href="campaigns.php?catg=arts"><div class="btn btn-solid">Creative Projects</div></a>
                    <a href="campaigns.php?catg=greek"><div class="btn btn-solid">Fraternities &amp; Sororities</div></a>
                    <a href="campaigns.php?catg=other"><div class="btn btn-solid">Fun &amp; Special Events</div></a>
                    <a href="campaigns.php?catg=children"><div class="btn btn-solid">Kids &amp; Family</div></a>
<!--                    <a href="campaigns.php?catg=lgbt"><div class="btn btn-solid">LGBT</div></a>-->
                    <a href="campaigns.php?catg=health"><div class="btn btn-solid">Medical &amp; Health</div></a>
                    <a href="campaigns.php?catg=memorial"><div class="btn btn-solid">Memorials &amp; Funerals</div></a>

                    <a href="campaigns.php?catg=military"><div class="btn btn-solid">Military</div></a>
                    <a href="campaigns.php?catg=non-profit"><div class="btn btn-solid">Non-Profit and Charity</div></a>
                    <a href="campaigns.php?catg=animal"><div class="btn btn-solid">Pets &amp; Animals </div></a>
                    <a href="campaigns.php?catg=political"><div class="btn btn-solid">Politics &amp; Public Office</div></a>
                    <a href="campaigns.php?catg=church"><div class="btn btn-solid">Religious Organizations</div></a>
                    <a href="campaigns.php?catg=run-walk-ride"><div class="btn btn-solid">Run/Walk/Rides </div></a>
                    <a href="campaigns.php?catg=education"><div class="btn btn-solid">Schools &amp; Education</div></a>
                    <a href="campaigns.php?catg=sports"><div class="btn btn-solid">Sports &amp; Teams</div></a>
                    <a href="campaigns.php?catg=trips"><div class="btn btn-solid">Trips &amp; Adventures</div></a>

                </div>
            </div>


        </div>
    </div>


	<!-- work togather -->
	<div class="donation-wrapper-home work_togathers ">
		<div class="parallax-mask"></div>
		<div class="container">
			<div class="work_togather">
				<h2>Give a little &amp; change a lot.</h2>
				<h1>Let’s Work Together!!</h1>
			</div>
		</div>
	</div>






	<!-- Volunteers -->
	<!--<div class="volunteers-need-wrapper volunteers-wrapper">
		<div class="parallax-mask"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				<div class="weneed-volunt info-block">
					<h2>We Need Volunteers</h2>
					<p>Nullam turpis mauris, egestas sed rutrum quis, egestas quis diam. Morbi at congue justo, a co.
					Fusce eget ante volutpat, rutrum orci non, scelerisque enim. Fusce eget nibh ornare, fringillolvenenatis eros. Nulla laoreet sagittis est, quis dapibus justo malesuada sed.</p>
					<a href="#" class="btn btn-big"><i class="fa fa-heartbeat"></i>Become a Volunteer</a>
				</div>

				</div>
			</div>
		</div>
	</div>-->


	<!-- gallery -->
	<!--<div class="volunteers-wrapper images-gallery-wrapper">
		<div class="container">
			<div class="section-name one">
				<h2>Causes Gallery</h2>
				<div class="short-text">
					<h5>boridiatat non proident sunt in culpa qui officia</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4 images-outer ">
					<div class="images-inner  ">
						<div class="nivo-activator"></div>
						<div class="images single-images-gl clearfix">
							<a href="assets/img/gallery/img-1.jpg" class="nivo-trigger" data-lightbox-gallery="gallery1"><span class="fa fa-arrows-alt"></span><img src="assets/img/gallery/img-1.jpg" alt=""></a>
						</div>
					</div>
				</div>
				<div class="col-sm-4 images-outer ">
					<div class="images-inner  ">
						<div class="nivo-activator"></div>
						<div class="images single-images-gl clearfix">
							<a href="assets/img/gallery/img-2.jpg" class="nivo-trigger" data-lightbox-gallery="gallery1"><span class="fa fa-arrows-alt"></span><img src="assets/img/gallery/img-2.jpg" alt=""></a>
						</div>
					</div>
				</div>
				<div class="col-sm-4 images-outer ">
					<div class="images-inner  ">
						<div class="nivo-activator"></div>
						<div class="images single-images-gl clearfix">
							<a href="assets/img/gallery/img-3.jpg" class="nivo-trigger" data-lightbox-gallery="gallery1"><span class="fa fa-arrows-alt"></span><img src="assets/img/gallery/img-3.jpg" alt=""></a>
						</div>
					</div>
				</div>
				<div class="col-sm-4 images-outer ">
					<div class="images-inner  ">
						<div class="nivo-activator"></div>
						<div class="images single-images-gl clearfix">
							<a href="assets/img/gallery/img-4.jpg" class="nivo-trigger" data-lightbox-gallery="gallery1"><span class="fa fa-arrows-alt"></span><img src="assets/img/gallery/img-4.jpg" alt=""></a>
						</div>
					</div>
				</div>
				<div class="col-sm-4 images-outer ">
					<div class="images-inner  ">
						<div class="nivo-activator"></div>
						<div class="images single-images-gl clearfix">
							<a href="assets/img/gallery/img-5.jpg" class="nivo-trigger" data-lightbox-gallery="gallery1"><span class="fa fa-arrows-alt"></span><img src="assets/img/gallery/img-5.jpg" alt=""></a>
						</div>
					</div>
				</div>
				<div class="col-sm-4 images-outer ">
					<div class="images-inner  ">
						<div class="nivo-activator"></div>
						<div class="images single-images-gl clearfix">
							<a href="assets/img/gallery/img-1.jpg" class="nivo-trigger" data-lightbox-gallery="gallery1"><span class="fa fa-arrows-alt"></span><img src="assets/img/gallery/img-1.jpg" alt=""></a>
						</div>
					</div>
				</div>



			</div>
		</div>
	</div>-->

	<!-- Blog -->
	<section  class="blog-area blog-post-wrapper hide">
		<div class="container">
			<div class="section-name one">
				<h2>Recent News</h2>
				<div class="short-text">
					<h5>boridiatat non proident sunt in culpa qui officia</h5>
				</div>
			</div>
			<div class="row">
				<!-- Blog Single -->
				<div class="col-md-4 col-sm-6">
					<div class="blog-box">
						<div class="blog-top-desc">
							<div class="blog-date">
								27 july 2017
							</div>
							<h4>Helping kids Grow up Stronger</h4>
							<i class="fa fa-user-o"></i> <strong>Admin</strong>
							<i class="fa fa-commenting-o"></i> <strong>12 Comments</strong>
						</div>
						<img src="assets/img/blog/img-1.jpg" alt="">
						<div class="blog-btm-desc">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque nam, necessitatibus odio dignissimos nostrum unde iure veniam.</p>
							<a href="#" class="btn btn-min btn-solid"> Read More  <i class="fa fa-arrow-right"></i> </a>
						</div>
					</div>
				</div>
				<!-- Blog Single -->
				<!-- Blog Single -->
				<div class="col-md-4 col-sm-6">
					<div class="blog-box">
						<div class="blog-top-desc">
							<div class="blog-date">
								20 july 2017
							</div>
							<h4>Helping kids Grow up Stronger</h4>
							<i class="fa fa-user-o"></i> <strong>Admin</strong>
							<i class="fa fa-commenting-o"></i> <strong>10 Comments</strong>
						</div>
						<img src="assets/img/blog/img-3.jpg" alt="">
						<div class="blog-btm-desc">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque nam, necessitatibus odio dignissimos nostrum unde iure veniam.</p>
							<a href="#" class="btn btn-min btn-solid"> Read More  <i class="fa fa-arrow-right"></i> </a>
						</div>
					</div>
				</div>
				<!-- Blog Single -->
				<!-- Blog Single -->
				<div class="col-md-4 col-sm-4 hidden-sm">
					<div class="blog-box">
						<div class="blog-top-desc">
							<div class="blog-date">
								17 july 2017
							</div>
							<h4>Helping kids Grow up Stronger</h4>
							<i class="fa fa-user-o"></i> <strong>Admin</strong>
							<i class="fa fa-commenting-o"></i> <strong>8 Comments</strong>
						</div>
						<img src="assets/img/blog/img-2.jpg" alt="">
						<div class="blog-btm-desc">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque nam, necessitatibus odio dignissimos nostrum unde iure veniam.</p>
							<a href="#" class="btn btn-min btn-solid"> Read More  <i class="fa fa-arrow-right"></i> </a>
						</div>
					</div>
				</div>
				<!-- Blog Single -->

			</div>
		</div>
	</section>



    <div class="modal fade payoptions" id="payoptions" tabindex="-1" role="dialog" aria-labelledby="payoptions">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content">
                    <div class="row" style="margin-left:0; margin-right:0; padding-bottom:10px">
                        <form>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <h3 style="margin:10px 0 10px 0;" class="text-center">Payment Option</h3>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Amount</label>
                                    <input required="required" class="form-control cam" style="width:100%;" type="text" id="iamount" value="10" placeholder="Amount" />
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Full Name</label>
                                    <input required="" class="form-control" style="width:100%;" type="text" id="fname" value="" placeholder="Full Name"/>
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Email Address</label>
                                    <input required="" class="form-control" style="width:100%;" type="email" id="iemail" value="" placeholder="Email Address"/>
                                </div>

                            </div>
                            <br/>
                            <div><p style="color:red;font-weight: bold" class="p_error text-center"></p></div>
                            <br/>
<?php if($ipinfo=='Ghana'){ ?>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 hide">
                               <!-- <button style="width:100%;" type="submit" class="btn btn-primary btn-lg paystripe hubtel_visa" ><i class="fa fa-credit-card fa-2"></i> Visa/Mastercard</button>  -->
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>
<?php }else{?>

    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 hide">
        <!--<button style="width:100%;" type="submit" class="btn btn-primary btn-lg judopay" ><i class="fa fa-credit-card fa-2"></i> Visa/Mastercard</button> -->
        <!--                                <img src="images/visamaster.png" width="100%">-->
    </div>

<?php }?>

<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 hide">
                                <button style="width:100%;" type="submit" class="btn btn-primary btn-lg paystripe hubtel_visa" ><i class="fa fa-credit-card fa-2"></i> Visa/Mastercard</button> 
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <button style="width:100%;" type="submit" class="btn btn-danger btn-lg paymm"  data-dismiss="modal" aria-hidden="true"><i class="fa fa-money fa-2"></i> Donate </button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade mmmodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content">

                    <h2 class="text-center">Payment Options</h2>
                    <div class="modal-body">
                        <div class="">
                            <form class="form-goup" style="padding: 8px;" id="donate_form">
                                <input type="hidden" name="email" id="email"/>
                                <input type="hidden" name="campid" value="" class="campid"/>
                                <input type="hidden" name="isregistered" value="0"/>
                                <input type="hidden" name="donatorid" value="-1"/>
                                <input type="hidden" name="trans_source" value="WEB"/>

                                <div class="form-group">
                                    <label class="sr-only" for="donate_amount">Amount (in Ghana Cedis)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">GH₵</div>
                                        <input type="number" name="amount" class="form-control" id="donate_amount" placeholder="Amount">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input type="hidden" name="optradio" value="mtn"/>
                                    <!--<div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input checked style="margin-top: 18px;" type="radio" id="card_mtn" value="mtn" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/mtn-mobile-money.png"></label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_airtel" value="airtel" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/airtel-money.png"/></label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_tigo" value="tigo" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/tigo-cash.png"/></label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_voda" value="voda" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="assets/img/vodafone-cash-ghana.png"/></label>
                                        </div>
                                    </div>-->
                                    <!--                    <label class="radio-inline"><input style="margin-top: 18px;" type="radio" id="card_visa" value="visa" name="optradio"><img style="max-height: 50px; max-width: 200px; padding: 5px;" height="50px" src="http://www.catnets.com.au/images/visa_mastercard_logo.png"/></label>-->




                                </div>

                                <!--OTHER MOBILE MONEY-->

                                <div id="mm_form" class="sw_form">
                                    <div class="form-group">
                                        <label class="sr-only" for="fullname">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input name="fullname" type="text" class="form-control" id="fullname" placeholder="Name"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="sr-only" for="mphone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone</div>
                                            <input name="phone" type="tel" class="form-control" id="mphone" placeholder="Phone Number"/>
                                        </div>
                                    </div>
                                </div>
                                <!--VODAFONE CASH-->

                                <div id="voda_form" class="sw_form hide">
                                    <div class="form-group">
                                        <label class="sr-only" for="fullname">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input name="vfullname" type="text" class="form-control" id="vfullname" placeholder="Name"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="sr-only" for="mphone">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Phone</div>
                                            <input name="vphone" type="text" class="form-control" id="vphone" placeholder="Phone Number"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only" for="token">Token</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Token</div>
                                            <input name="token" type="text" class="form-control" id="token" placeholder="Token"/>
                                        </div>
                                    </div>

                                </div>
                                <div style="display: block;text-align: right;">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Donate</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>



    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>





    <!--    HUBTEL SCRIPTS-->

    <form id="hubForm" action="process/hubtel_visa.php" method="POST">
        <input type="hidden" id="hubemail" name="email" />
        <input type="hidden" id="hub_amount" name="amount"/>
        <input type="hidden" id="hub_title" name="title" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="hub_name" name="name"/>
        <input type="hidden" id="hub_camp_id" class="hub_campid" name="campid"/>
    </form>

    <!--JUDO VISA SCRIPT-->
    <form id="judoForm" action="process/judopay/judopay.php" method="POST">
        <input type="hidden" id="judo_email" name="email" />
        <input type="hidden" id="judo_amount" name="amount"/>
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="judo_name" name="name"/>
        <input type="hidden" id="judo_camp_id" class="judo_campid" name="campid"/>
    </form>

	<!-- Foter -->
<?php include_once ('footer.php');?>