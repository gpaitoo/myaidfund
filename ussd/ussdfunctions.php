<?php

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack)-strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function contains($needle, $haystack) {
    return strpos($haystack, $needle) !== false;
}

function getUssdData($offset) {
    global $mysqli;
    $sql = "SELECT * FROM ussd_campaign ORDER BY ussdrank LIMIT $offset,4";
    $results = $mysqli->query($sql);
    return $results;
}

function getUssdDataMore($offset) {
    global $mysqli;
    $sql = "SELECT * FROM ussd_campaign ORDER BY ussdrank LIMIT $offset,4";
    $results = $mysqli->query($sql);
    return $results;
}

function getUssdCampaignByRank($ussdrank) {
    global $mysqli;
    $sql = "SELECT * FROM ussd_campaign WHERE ussdrank=$ussdrank ORDER BY ussdrank";
    $results = $mysqli->query($sql);
    return $results;
}

function getOffset($sessionid) {
    global $mysqli;
    $sql = "SELECT menu_count FROM ussdpos WHERE sessionid='$sessionid' AND menustatus='initial_menu' ORDER BY id DESC";
    $results = $mysqli->query($sql);
    $rst=0;
    while ($row = $results->fetch_array()){
        $rst=$row['menu_count'];
    }
    return $rst;
}

function setOffset($sessionid,$count) {
    global $mysqli;
    $sql = "UPDATE ussdpos SET menu_count=$count WHERE sessionid=$sessionid";
    $mysqli->query($sql);
}

function getUssdDataBySession($session) {
    global $mysqli;
    $sql = "SELECT * FROM ussd_data WHERE sessionid = '$session' ORDER BY id DESC LIMIT 1";
    $results = $mysqli->query($sql);
    return $results;
}

function setUssdData($sessions,$network,$msisdn) {
    global $mysqli;
    $sql = "INSERT INTO ussd_data(sessionid,network,msisdn) VALUES('$sessions','$network','$msisdn')";
    $results = $mysqli->query($sql);
    return $results;
}

function updateUssdData($sessionid, $key,$value) {
    global $mysqli;
    $sql = "UPDATE ussd_data SET $key = $value WHERE sessionid = '$sessionid'";

    $results = $mysqli->query($sql);
}

function format_msisdn($msisdn){
    if(startsWith($msisdn,'233')){
        return "0".substr($msisdn,3);
    }else{
        return $msisdn;
    }
}

function write_logs($log){
    $File = "reqin.log";
    $Handle = fopen($File, 'a');
    $Data = "$log\n";
    fwrite($Handle, $Data);
    fclose($Handle);
}