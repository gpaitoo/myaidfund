<?php
include ("ussdfunctions.php");
$session = $_REQUEST['UserSessionID'];
$message = $_REQUEST['msg'];
$msisdn  = $_REQUEST['msisdn'];
$network = $_REQUEST['network'];
$appdata = $_REQUEST['APPDATA'];

write_logs(json_encode($_REQUEST));
//write_logs($appdata);
$start = -1;

if ($appdata == '{}') {
	$start     = 0;
	$ussd_code = $message;
} else {
	$start     = -1;
	$ussd_code = -1;
}

to_ussd_app($start, $ussd_code, $message, $session, $msisdn, $network);

function to_ussd_app($start, $ussd_code, $message, $session, $msisdn, $network) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
			CURLOPT_URL            => "https://myaidfund.com/ussd/ussdapp.php?start=$start&ussdcode=$ussd_code&content=$message&session=$session&msisdn=$msisdn&network=$network",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			CURLOPT_HTTPHEADER     => array(
				"cache-control: no-cache",
			),
		));

	$response = curl_exec($curl);
	$err      = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:".$err;
	} else {
		echo $response;
	}
}
