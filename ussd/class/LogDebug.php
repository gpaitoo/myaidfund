<?php

/**
 * Created by PhpStorm.
 * User: oduro
 * Date: 6/29/2017
 * Time: 9:28 AM
 */
class LogDebug
{
    function debug($text, $show = FALSE) {
        //$output = show_now()."::".$text."\n";

        if ($show === FALSE) {
            $this->write_to_file($text, 'debug.log');
        } else {
            //echo $output;
        }

    }

    function write_to_file($data, $file) {
        #CHECK THIS OUT
        #http://www.php.net/manual/en/function.stream-set-blocking.php
        $fp = fopen($file, 'a');
        #CONSIDER STRIPPING ALL NEWLINE ETC
        fwrite($fp, "{$data}\n");
    }


}