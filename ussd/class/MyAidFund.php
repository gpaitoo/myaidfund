<?php

/**
 * Created by PhpStorm.
 * User: oduro
 * Date: 6/29/2017
 * Time: 9:37 AM
 */
class MyAidFund
{
    private $action_showMenu = "showMenu";
    private $action_prompt = "prompt";
    private $action_input = "input";

public function initial_menu($session,$network,$msisdn) {
    setUssdData($session,$network,$msisdn);
    $offset = getOffset($session);
    $result = getUssdData($offset);
    $items=array();
    while ($r = $result->fetch_array()) {
        $ar = trim($r['ussdrank'].".".$r['campname']);
        array_push($items,$ar);
    }
    array_push($items,"99.More");
    $a=array(implode(",",$items));
    $title="MyAidFund Donations";
    $menus = $items;//array("1.Yaw Amoah ","2.Kofi Ameyaw","3.Naa Kwaley");
    $offset+=4;
    setOffset($session,$offset);
    return   array("action" => $this->action_showMenu, "menus" => $menus, "title" => $title);
}

public function amount_msg($session,$campid){
    $result = getUssdCampaignByRank($campid);
    $cname='';
    while ($r = $result->fetch_array()) {
        $cname = trim($r['campname']);
    }

    $title="Please Enter Amount to Donate to $cname";
    $menus = "";

    updateUssdData($session,'campid',"(SELECT campid FROM ussd_campaign WHERE ussdrank=$campid)");
    return   array("action" => $this->action_input, "menus" => $menus, "title" => $title);
}

public function make_donation($session,$amnt){
    updateUssdData($session,'amount',$amnt);


    $title="Please Confirm your donation from your mobile money account. Thank You";
    $menus = "";
    return  array("action" => $this->action_prompt, "menus" => $menus, "title" => $title);
}


    public function make_donation_voda($session,$amnt){
        //updateUssdData($session,'amount',$amnt);


        $title="Your payment request has been sent. Thank you for your kind gesture";
        $menus = "";
        return  array("action" => $this->action_prompt, "menus" => $menus, "title" => $title);
    }


    public function request_vodafone_token($session,$amnt){
        updateUssdData($session,'amount',$amnt);


        $title="Please Enter your generated Token (dial *110# to generate Token).";
        write_logs($title);
        $menus = "";
        return  array("action" => $this->action_input, "menus" => $menus, "title" => $title);
    }


public function donate($msisdn,$campid,$amnt,$network,$token=''){
//$curl = curl_init();
//$url= "https://myaidfund.com/process/InitiatePayment.php?fullname=kofi&phone=$msisdn&optradio=$network&campid=$campid&amount=$amnt&isregistered=0&donatorid=1&email=a%40b.com";

$cmd = " php -q /home/apptechhub/public_html/ussd/donate.php '$msisdn' '$amnt' '$campid' '$network' '$token' > /dev/null 2>/dev/null &";
    write_logs($cmd);
    shell_exec($cmd);
    write_logs("-------------------------------");
    write_logs($cmd);

//echo $url;
/*    write_logs($url);
curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_VERBOSE => false,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache"
    ),
));

curl_exec($curl);

curl_close($curl);*/


}

}