<?php
require 'Serial.php';
class paymentApi {

	public function initiate_call($phone, $amount, $transmode, $transitem,$transid) {

		$token    = ($transmode == 'VODAFONE')?Serial::random(5):'';
		$post_arr = array('PayType' => $transmode,
			'PhoneNumber'              => ($transmode == 'TIGO')?'233'.substr($phone, 1):$phone,
			'FullName'                 => 'MTECH SERVICE',
			'Amount'                   => $amount,
			'TransactionId'            => $transid,
			'Item'                     => $transitem,
			'CallbackUrl'              => 'http://107.6.149.82:8083/sdp/ussd/menu/callback.php',
			'Token'                    => $token,

		);

		$post_data = json_encode(array($post_arr));
		$curl      = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL            => "http://139.162.221.182/initiate.php",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => "",
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => "POST",
				CURLOPT_POSTFIELDS     => $post_data,
				CURLOPT_HTTPHEADER     => array(
					"api: AE2B1FCA515949E5D54FB22B8ED95575",
					"authorization: Basic: 8fa8d201e89d1d5fe6002a4af8db882a",
					"cache-control: no-cache",
					"content-type: application/json",
				),
			));

		return json_decode(curl_exec($curl));
	}

}