<?php
class Menus {
	//private $awoof_menu;

	public function initial_menu() {
		$responseMsg = array(
			"menu1" => "
            Welcome, PRESS
            1. VAS SERVICES
            2. UBA SERVICES
            3. ECG
            4. BUY AIRTIME
        ",
		);
		return $responseMsg;
	}

	public function uba_services() {
		$responseMsg = array(
			"uba" => "
Welcome to UBA Payment Services,
PRESS
1. BLUE CREST
2. Regent University
 ",
            );

		return $responseMsg;
	                      }



	public function getBlueCrest() {
        $responseMsg = array(
            "blue_crest" => "Welcome To Blue Crest Services,PRESS
1. Pay Fees
2. Check Balance
",

"PayFees" => "Enter Student Id",
"Confirm" => "Confirm Student Id",
"Name"    => "Enter Full Name",
"blue_crest_payment_mode" => "Choose Your Payment Option
PRESS
1. MTN Mobile Money
2. Tigo Cash
3. Airtel Money
4. Vodafone Cash
",
            "MTN" => "Enter Mobile Money Phone Number ",

			"TIGO" => "Enter Mobile Money Phone Number ",

			"AIRTEL" => "Enter Mobile Money Phone Number ",

			"VODAFONE" => "Enter Mobile Money Phone Number ",
            "fee_mount" => "Enter Amount",
            "confirm_details" =>"Please Confirm Details
 STUDENT ID: {{accno}}
 PHONE NUMBER: {{payphone}}
 PAYMENT ITEM: FEES
 AMOUNT: GHS {{amount}}
 VENDOR: {{paymode}} 

PRESS
1.PROCEED
2.CANCEL
",

            "success" => "Payment Made Successfully",

			"Deposit" => "Choose Mode of Deposit",

			"Withdrawal" => "Under Construction",

			"FeesBalance" => "Enter Student Id",

		);
		return $responseMsg;
	}

	//Regent University Menu Tree

    public function getRegentUniversity() {
        $responseMsg = array(
            "regent" => "Welcome To Blue Crest Services,PRESS
1. Pay Fees
2. Check Balance
",

            "PayFees" => "Enter Student Id",
            "Confirm" => "Confirm Student Id",
            "Name"    => "Enter Full Name",
            "regent_payment_mode" => "Choose Your Payment Option
PRESS
1. MTN Mobile Money
2. Tigo Cash
3. Airtel Money
4. Vodafone Cash
",
            "MTN" => "Enter Mobile Money Phone Number ",

            "TIGO" => "Enter Mobile Money Phone Number ",

            "AIRTEL" => "Enter Mobile Money Phone Number ",

            "VODAFONE" => "Enter Mobile Money Phone Number ",
            "fee_mount" => "Enter Amount",
            "confirm_details" =>"Please Confirm Details
STUDENT ID: {{accno}}
 PHONE NUMBER: {{payphone}}
 PAYMENT ITEM: FEES
 AMOUNT: GHS {{amount}}
 VENDOR: {{paymode}} 
PRESS
1.PROCEED
2.CANCEL
",

            "success" => "Payment Made Successfully",

            "Deposit" => "Choose Mode of Deposit",

            "Withdrawal" => "Under Construction",

            "FeesBalance" => "Enter Student Id",

        );
        return $responseMsg;
    }

    //

	public function ecg_services() {
		$responseMsg = array(
			"ecg" => "Welcome To ECG Services,
PRESS
1. Top Up
2. Check Units
",

			"TopUp" => "Enter Meter Number",
            "confirm_meter_number" => "Confirm Meter Number",
            "ecg_pay_mode" => "Choose Your Payment Option,
PRESS
1. MTN Mobile Money
2. Tigo Cash
3. Airtel Money
4. Vodafone Cash
",

			"MTN" => "Enter Mobile Money Phone Number ",

			"TIGO" => "Enter Mobile Money Phone Number ",

			"AIRTEL" => "Enter Mobile Money Phone Number ",

			"VODAFONE" => "Enter Mobile Money Phone Number ",

			"ecg_amount" => "Enter Amount",
            "ecg_confirm" =>"
 METER NUMBER: {{accno}}
 PHONE NUMBER: {{payphone}}
 PAYMENT ITEM: ECG TOP UP
 AMOUNT: {{amount}}
 VENDOR: {{paymode}} 
 
 PRESS
 
 1.PROCEED
 2.CANCEL
",

            "initiate" => "Payment Initiated",
			"Deposit" => "Choose Mode of Deposit",

			"Withdrawal" => "Under Construction",

			"CheckUnits" => "Enter Meter Number",

		);
		return $responseMsg;
	}



	//Awoof Menu Tree

    public function getAwoof() {
        $responseMsg = array(
            "menu1" => "Welcome To UBA Awoof Promotion,
                        PRESS
                        1. Registration
                        2. Deposit
                        3. Withdrawal
                        ",

            "Registration" => "Enter FullName",

            "Mode" => "Choose Your Payment Option,
			PRESS
			1. MTN Mobile Money
			2. Tigo Cash
			3. Airtel Money
			4. Vodafone Cash
			",

            "MTN" => "Enter Mobile Money Phone Number ",

            "TIGO" => "Enter Mobile Money Phone Number ",

            "AIRTEL" => "Enter Mobile Money Phone Number ",

            "VODAFONE" => "Enter Mobile Money Phone Number ",

            "Amount" => "Enter Deposit Amount",

            "Deposit" => "Choose Mode of Deposit",

            "Withdrawal" => "Under Construction",
        );
        return $responseMsg;
    }

}

