<?php
/**
 * Created by PhpStorm.
 * User: oduro
 * Date: 6/29/2017
 * Time: 9:25 AM
 */
include ('../includes/config.php');
include ('ussdfunctions.php');
require 'class/LogDebug.php';
require 'class/opSession.php';
require 'class/menutree.php';
require 'class/MyAidFund.php';

$operations = new OpSession();
$ussdstart  = $_REQUEST['start'];
$ussdcode   = $_REQUEST['ussdcode'];
$content    = $_REQUEST['content'];
$sessionId  = $_REQUEST['session'];
$msisdn     = $_REQUEST['msisdn'];
$network    = $_REQUEST['network'];
$myaid      = new MyAidFund();

$log = new LogDebug();

//
if ($ussdstart == 0) {

	try {

        /*if($network=='mtn'){
            $rst = array("action" => "prompt", "menus" => "", "title" => "Sorry this service is not available on MTN Mobile Money, You can donate Using Airtel, Tigo and Vodafone Cash");
            reply_ussd($rst);
            die();
        }*/

		if ($ussdcode == '*718*23') {
			$log->debug('default log');
			$log->debug($operations->setUssdSession($sessionId, "initial_menu", $sessionId.'_'.$ussdcode));
			$response = $myaid->initial_menu($sessionId, $network, $msisdn);
			reply_ussd($response);
		} else if ($ussdcode == '*718*23*1') {
			$log->debug('------ 718*23*1');
			$log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
			$myaid->initial_menu($sessionId, $network, $msisdn);
			$result = $myaid->amount_msg($sessionId, 1);
			$log->debug($result);
			reply_ussd($result);
		} else if ($ussdcode == '*718*23*2') {
			$log->debug('------ 718*23*2');
			$log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
			$myaid->initial_menu($sessionId, $network, $msisdn);
			$result = $myaid->amount_msg($sessionId, 2);
			$log->debug($result);
			reply_ussd($result);
		} else if ($ussdcode == '*718*23*3') {
			$log->debug('------ 718*23*3');
			$log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
			$myaid->initial_menu($sessionId, $network, $msisdn);
			$result = $myaid->amount_msg($sessionId, 3);
			$log->debug($result);
			reply_ussd($result);
		} else if ($ussdcode == '*718*23*4') {
			$log->debug('------ 718*23*4');
			$log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
			$myaid->initial_menu($sessionId, $network, $msisdn);
			$result = $myaid->amount_msg($sessionId, 4);
			$log->debug($result);
			reply_ussd($result);
		} else if ($ussdcode == '*718*23*5') {
			$log->debug('------ 718*23*5');
			$log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
			$myaid->initial_menu($sessionId, $network, $msisdn);
			$result = $myaid->amount_msg($sessionId, 5);
			$log->debug($result);
			reply_ussd($result);
		} else if ($ussdcode == '*718*23*6') {
            $log->debug('------ 718*23*6');
            $log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
            $myaid->initial_menu($sessionId, $network, $msisdn);
            $result = $myaid->amount_msg($sessionId, 6);
            $log->debug($result);
            reply_ussd($result);
        }else if ($ussdcode == '*718*23*7') {
            $log->debug('------ 718*23*7');
            $log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
            $myaid->initial_menu($sessionId, $network, $msisdn);
            $result = $myaid->amount_msg($sessionId, 7);
            $log->debug($result);
            reply_ussd($result);
        }else if ($ussdcode == '*718*23*8') {
            $log->debug('------ 718*23*8');
            $log->debug($operations->setUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
            $myaid->initial_menu($sessionId, $network, $msisdn);
            $result = $myaid->amount_msg($sessionId, 8);
            $log->debug($result);
            reply_ussd($result);
        }

	} catch (Exception $e) {

		$log->debug('error exception');
		$result = $firstTrust->default_response();
		echo '1'.$result['error'];

	}
} else {

	$cuch_menu = $operations->getUssdMenuStatus($sessionId);

	switch ($cuch_menu) {
		case 'initial_menu':
			switch ($content) {
			case '1':
			case '2':
			case '3':
			case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '10':
            case '11':
                $log->debug('donar 1');
                $log->debug($operations->updateUssdSession($sessionId, "donation_amount", $sessionId.'_'.$ussdcode));
                $result = $myaid->amount_msg($sessionId, $content);
                reply_ussd($result);
                break;
            case '99':
                $log->debug('default log');
                $log->debug($operations->setUssdSession($sessionId, "initial_menu", $sessionId.'_'.$ussdcode));
                $response = $myaid->initial_menu($sessionId, $network, $msisdn);
                reply_ussd($response);
                break;
			}
			break;
		case 'donation_amount':

            $result  = getUssdDataBySession($sessionId);
            $msisdn  = '';
            $network = '';
            $campid  = '';
            $amnt    = '';
            $net     = '';

            while ($r = $result->fetch_array()) {
                $msisdn  = $r['msisdn'];
                $network = $r['network'];
                $campid  = $r['campid'];
                $amnt    = $r['amount'];
            }

            switch ($network) {
                case 'vodafone':
                    $log->debug('vodafone_donation');
                    $log->debug($operations->updateUssdSession($sessionId, "vodafone_donation", $sessionId.'_'.$ussdcode));
                    $log->debug("vodafone switch");
                    $result = $myaid->request_vodafone_token($sessionId,$content);
                    $log->debug(json_encode($result));
                    reply_ussd($result);
                    //$net = 'vodafone';
                    break;
                default:
                    $log->debug('make donation');
                    $log->debug($operations->updateUssdSession($sessionId, "make_donation", $sessionId.'_'.$ussdcode));
                    $result = $myaid->make_donation($sessionId, $content);
                    reply_ussd($result);

                    $amnt=$content;
                    $net = $network;
                    $msisdn = format_msisdn($msisdn);
                    $myaid->donate($msisdn, $campid, $amnt, $net);
                    break;
            }




			break;
        case 'vodafone_donation':
            $log->debug('make_donation_vodafone');
            $log->debug($operations->updateUssdSession($sessionId, "make_donation_vodafone", $sessionId.'_'.$ussdcode));
            $result = $myaid->make_donation_voda($sessionId, $content);
            reply_ussd($result);

            $result  = getUssdDataBySession($sessionId);
            $msisdn  = '';
            $network = '';
            $campid  = '';
            $amnt    = '';
            $net     = '';
            $token = $content;
            while ($r = $result->fetch_array()) {
                $msisdn  = $r['msisdn'];
                $network = $r['network'];
                $campid  = $r['campid'];
                $amnt    = $r['amount'];
            }
            $net = 'voda';
            $msisdn = format_msisdn($msisdn);
            $myaid->donate($msisdn, $campid, $amnt, $net,$token);
            break;
	}
}

function reply_ussd($response) {
	echo json_encode(array("USSDResp" => $response));
}