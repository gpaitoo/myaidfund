<div class="g-signin2 hide" id="g_signin" data-onsuccess="onSignIn"></div>
<div id="d_login_form" class="hide">
<div class="form-body">
        <ul class="nav nav-tabs final-login">
        <li class="active">
<a data-toggle="tab" href="#sectionA">Sign In</a>
        </li>
            <li>
                <a data-toggle="tab" href="#sectionB">Register</a>
            </li>
        </ul>
    <div class="tab-content">
        <div id="sectionA" class="tab-pane fade in active">
            <div class="innter-form">
                <form class="sa-innate-form k_login" onsubmit="event.preventDefault(); submitlogin()" method="post" >
                    <label class="text-black" style="float:left">Phone/Email</label>
                    <input type="text" id="xxxname" name="xxxname" placeholder="Phone/Email">
                    <label class="text-black" style="float:left">Password</label>
                    <input type="password" id="xxxpass" placeholder="Password" name="xxxpass">
                    <button type="submit">Sign In</button> &nbsp;<a href="">Forgot Password?</a>
                </form>
            </div>
            <div class="social-login">
                <p>- - - - - - - - - - - - - Sign In With - - - - - - - - - - - - - </p>
                <ul>
                    <li><a onclick="event.preventDefault(); fb_login()" href="#" ><i class="fa fa-facebook"></i> Facebook</a></li>
                    <li><a onclick="event.preventDefault(); gl_login()" href="#"><i class="fa fa-google-plus"></i> Google+</a></li>
                    <li class="hide"><a href=""><i class="fa fa-twitter"></i> Twitter</a></li>
                </ul>
                <div id="status">
        </div>
        <div id="response"></div>
            </div>
            <div class="clearfix"></div></div>
        <div id="sectionB" class="tab-pane fade">
            <div class="innter-form">
                <form class="sa-innate-form" method="post" onsubmit="event.preventDefault(); register_user();">
                    <label class="text-black" style="float:left">Name</label>
                    <input type="text" id="dfullname" name="username" placeholder="Full Name">
                    <label class="text-black" style="float:left">Email</label>
                    <input type="text" id="rxxxname" name="xxxname" placeholder="Phone/Email">
                    <label class="text-black" style="float:left">Password</label>
                    <input type="password" id="rxxxpass" name="rxxxpass" placeholder="Password">
                    <button class="left" type="submit">Join now</button>
                    <p>By clicking Join now, you agree to MyAidFunds`s User Agreement, Privacy Policy, and Cookie Policy.</p>
                </form>
            </div>
            <div class="social-login"><p>- - - - - - - - - - - - - Register With - - - - - - - - - - - - - </p>
                <ul>
                    <li><a onclick="fb_login()"><i class="fa fa-facebook"></i> Facebook</a></li>
                    <li><a onclick="gl_login()"><i class="fa fa-google-plus"></i> Google+</a></li>
                    <li class="hide"><a href=""><i class="fa fa-twitter"></i> Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>