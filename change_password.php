<?php
include ('includes/allfunctions.php');
$email = $_REQUEST['email'];
$code = $_REQUEST['code'];
$rst = check_can_change_password($email,$code);
if(!$rst){
    header('location: index.php');
}
$page = 'Activation';
include_once ('header.php');


?>

    <style>
        .container{
            width:95%;
        }
    </style>

    <!-- basic-slider end -->

    <!-- Features -->


    <!-- Special Cuase Paralax -->



    <!-- Causes -->



    <!-- team -->
    <div class="team-wrapper" id="our_mission">
        <div class="container">
            <div class="col-4">
                <h2 style="text-align: center">Change Password</h2>

                <div class="well" style="width: 80%; margin-left: auto; margin-right: auto">

                    <form class="prst" action="process/process_users.php">
                        <input type="hidden" name="code" value="<?echo $code; ?>">
                        <input type="hidden" name="opera" value="update">
                        <div class="">
                            <label style="color: blue;font-size: medium;"> Password </label>
                            <input class="form-control" id="pass1" type="password" size="14" name="xxpass1" placeholder="New Password"/>
                        </div>
                        <div class="">
                            <label style="color: blue;font-size: medium;"> Confirm Password </label>
                            <input class="form-control" id="pass2" type="password" size="14" name="xxpass2" placeholder="Confirm Password"/>
                        </div>

                        <?php if ($is_production):?>
                            <div class="g-recaptcha" data-sitekey="6Ld5qUsUAAAAAFoDW9Oj_ugh0k9XyWTYaKMe4IYG"></div>
                        <?php endif;?>

                        <div class="field">
                            <button type="submit" class="btn btn-min btn-solid" style="margin-left: 0px;"><span>Submit</span></button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>




    <!-- Modal -->




    <!--    STRIPE SCRIPTS-->

    <form id="stripeForm" action="process/stripe_process.php" method="POST">
        <input type="hidden" id="stripeToken" name="stripeToken" />
        <input type="hidden" id="stripeEmail" name="stripeEmail" />
        <input type="hidden" id="stripeAmount" name="amount"/>
        <input type="hidden" id="stripeDescription" name="description" value="MyAidFund Donation"/>
        <input type="hidden" id="amountInCents" name="amountInCents" />
        <input type="hidden" id="isregisterd" name="isregistered" value="0"/>
        <input type="hidden" id="donatorid" name="donatorid" value="-1"/>
        <input type="hidden" id="name" name="name"/>
        <input type="hidden" class="campid" name="campid"/>
        <!--<script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_KkbyHWohs3rkNRbi5aDCLqQU"
                data-amount="<?php /*echo $amount;*/?>"
                data-name="Apptechhub Global, Inc."
                data-description="Widget"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
        </script>-->
    </form>

    <!-- Foter -->
<?php include_once ('footer.php');?>