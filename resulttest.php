<?php

$curl = curl_init();

curl_setopt_array($curl, array(
		CURLOPT_URL            => "https://finance.google.com/finance/converter?a=1&from=USD&to=GHS",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING       => "",
		CURLOPT_MAXREDIRS      => 10,
		CURLOPT_TIMEOUT        => 30,
		CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST  => "GET",
		CURLOPT_HTTPHEADER     => array(
			"cache-control: no-cache",
			"postman-token: ee501792-f688-bb63-9cd5-9d031341a41e",
		),
	));

$response = curl_exec($curl);
$err      = curl_error($curl);

$pos = strpos($response, 'bld')+4;
$rst = substr($response, $pos, 3);
echo $rst;
